package pl.edu.pw.mini.optimization.bbcomp.client;

import java.io.*;
import java.net.*;
import java.util.*;


////////////////////////////////////////////////////////////
// black box library Java proxy
//
public class BBComp
{
	////////////////////////////////////////////////////////////
	// simplistic network abstraction
	//

	private static final int          DEFAULT_PORT       = 7000;

	private Socket m_socket = null;
	private BufferedReader m_input = null;
	private PrintWriter m_output = null;

	public BBComp() throws Exception
	{
		m_socket = new Socket("localhost", DEFAULT_PORT);
		m_input = new BufferedReader(new InputStreamReader(m_socket.getInputStream()));
		m_output = new PrintWriter(m_socket.getOutputStream(), true);
	}

	public BBComp(int port) throws Exception
	{
		m_socket = new Socket("localhost", port);
		m_input = new BufferedReader(new InputStreamReader(m_socket.getInputStream()));
		m_output = new PrintWriter(m_socket.getOutputStream(), true);
	}

	public void close()
	{
		// disconnect and kill proxy
		try
		{
			if (m_socket != null) m_socket.close();
		}
		catch (Exception ex)
		{
		}
		finally
		{
			m_socket = null;
		}
	}

	public void finalize()
	{
		close();
	}

	private String receiveLine() throws Exception
	{
		String line = m_input.readLine();
		//System.out.println("Received: " + line);
		return line;
	}

	private void sendLine(String line) throws Exception
	{
		//System.out.println("Requested: " + line);
		m_output.print(line + "\n");
		m_output.flush();
	}


	////////////////////////////////////////////////////////////
	// black box library interface functions
	//

	boolean configure(boolean history, String logfilepath) throws Exception
	{
		sendLine("configure(" + (history ? "1" : "0") + ",\"" + logfilepath + "\")");
		return (Integer.parseInt(receiveLine()) != 0);
	}

	boolean login(String username, String password) throws Exception
	{
		sendLine("login(\"" + username + "\", \"" + password + "\")");
		return (Integer.parseInt(receiveLine()) != 0);
	}

	int numberOfTracks() throws Exception
	{
		sendLine("numberOfTracks()");
		return Integer.parseInt(receiveLine());
	}

	String trackName(int trackindex) throws Exception
	{
		sendLine("trackName(" + new Integer(trackindex).toString() + ")");
		return receiveLine();
	}

	boolean resetTrack(String trackname) throws Exception
	{
		sendLine("resetTrack(" + trackname + ")");
		return (Integer.parseInt(receiveLine()) != 0);
	}

	boolean setTrack(String trackname) throws Exception
	{
		sendLine("setTrack(\"" + trackname + "\")");
		return (Integer.parseInt(receiveLine()) != 0);
	}

	int numberOfProblems() throws Exception
	{
		sendLine("numberOfProblems()");
		return Integer.parseInt(receiveLine());
	}

	boolean setProblem(int problemID) throws Exception
	{
		sendLine("setProblem(" + new Integer(problemID).toString() + ")");
		return (Integer.parseInt(receiveLine()) != 0);
	}

	int dimension() throws Exception
	{
		sendLine("dimension()");
		return Integer.parseInt(receiveLine());
	}

	int budget() throws Exception
	{
		sendLine("budget()");
		return Integer.parseInt(receiveLine());
	}

	int evaluations() throws Exception
	{
		sendLine("evaluations()");
		return Integer.parseInt(receiveLine());
	}

	public class EvaluateReturn
	{
		public boolean result;
		public double value;
	}

	EvaluateReturn evaluate(Vector<Double> point) throws Exception
	{
		String command = "evaluate([" + point.elementAt(0).toString();
		for (int i=1; i<point.size(); i++)
		{
			command += " ";
			command += point.elementAt(i).toString();
		}
		command += "])";
		sendLine(command);
		String result = receiveLine();
		EvaluateReturn ret = new EvaluateReturn();
		if (result.equals("0"))
		{
			ret.result = false;
			return ret;
		}
		else
		{
			ret.result = true;
			ret.value = Double.parseDouble(result.substring(2));
			return ret;
		}
	}

	public class HistoryReturn
	{
		public boolean result;
		public Vector<Double> point;
		public double value;
	}

	HistoryReturn history(int index, int dimension) throws Exception
	{
		sendLine("history(" + new Integer(index).toString() + "," + new Integer(dimension).toString() + ")");
		String result = receiveLine();
		HistoryReturn ret = new HistoryReturn();
		if (result.equals("0"))
		{
			ret.result = false;
			return ret;
		}
		else
		{
			assert result.substring(0, 3).equals("1,[");
			ret.result = true;
			ret.point = new Vector<Double>(dimension);
			ret.point.setSize(dimension);
			int pos = 3;
			for (int d=0; d<dimension; d++)
			{
				int end;
				for (end=pos; end<result.length(); end++)
				{
					if (result.charAt(end) == ' ') break;
					if (result.charAt(end) == ']') break;
				}
				ret.point.setElementAt(Double.parseDouble(result.substring(pos, end)), d);
				pos = end + 1;
			}
			assert result.charAt(pos) == ',';
			ret.value = Double.parseDouble(result.substring(pos+1));
			return ret;
		}
	}

	String errorMessage() throws Exception
	{
		sendLine("errorMessage()");
		return receiveLine();
	}
}

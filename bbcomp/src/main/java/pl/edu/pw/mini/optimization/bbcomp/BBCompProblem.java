package pl.edu.pw.mini.optimization.bbcomp;

import lombok.SneakyThrows;
import pl.edu.pw.mini.optimization.bbcomp.client.BBComp;
import pl.edu.pw.mini.optimization.bbcomp.client.BBCompClient;
import pl.edu.pw.mini.optimization.bbcomp.exception.BBCompBudgetExceededException;
import pl.edu.pw.mini.optimization.bbcomp.exception.BBCompException;

import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Collectors;

public class BBCompProblem {

    private static final double UPPER_BOUNDARY = 1;
    private static final double LOWER_BOUNDARY = 0;
    private final BBCompClient client;
    private final String name;
    private final int _dimension;
    private final double[][] _bounds;
    private final int index;

    @SneakyThrows
    BBCompProblem(BBCompClient client) {
        this.client = client;
        this._dimension = client.getDimension();
        this.name = client.getProblemId().toString();
        this.index = client.getProblemId();
        this._bounds = new double[2][];
        this._bounds[0] = new double[_dimension];
        this._bounds[1] = new double[_dimension];
        for (int i = 0; i < _dimension; i++) {
            this._bounds[0][i] = LOWER_BOUNDARY;
            this._bounds[1][i] = UPPER_BOUNDARY;
        }
    }

    public double[] evaluateFunction(double[] x) {
        Vector<Double> pt = Arrays.stream(x)
                .boxed()
                .collect(Collectors.toCollection(Vector::new));
        try {
            BBComp.EvaluateReturn result = client.evaluate(pt);
            if (result.result) {
                //System.out.println(String.format("%s -> %s", Arrays.toString(x), result.value));
                return new double[] {result.value};
            } else {
                throw new BBCompBudgetExceededException();
            }
        } catch (BBCompException e) {
            throw new RuntimeException("Unexpected exception happened during execution.", e);
        }
    }

    public int getDimension() {
        return _dimension;
    }

    public double[] getSmallestValuesOfInterest() {
        return _bounds[0];
    }

    public double getSmallestValueOfInterest(int index) {
        return _bounds[1][index];
    }

    public double[] getLargestValuesOfInterest() {
        return _bounds[1];
    }

    public double getLargestValueOfInterest(int index) {
        return _bounds[1][index];
    }

    public String getId() {
        return name;
    }

    public String getName() {
        return name;
    }

    @SneakyThrows
    public long getEvaluations() {
        return client.getEvaluations();
    }

    public long getIndex() {
        return index;
    }

    public boolean isFinalTargetHit() {
        return false;
    }
}

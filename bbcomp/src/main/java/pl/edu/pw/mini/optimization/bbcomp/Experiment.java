package pl.edu.pw.mini.optimization.bbcomp;

import lombok.SneakyThrows;
import pl.edu.pw.mini.optimization.bbcomp.client.BBCompClient;
import pl.edu.pw.mini.optimization.bbcomp.exception.BBCompException;
import pl.edu.pw.mini.optimization.bbcomp.utils.NativeUtils;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Experiment {

    private static final Path LOG_DIR = Paths.get("./exdata").toAbsolutePath();

    @SneakyThrows
    public static void main(String[] args) {
        try {
            runWithProxy("lib/proxy", args);
        } catch (IOException e) {
            runWithProxy("bbcomp/lib/proxy", args);
        }
    }

    private static void runWithProxy(String path, String[] args) throws IOException {
        NativeUtils.runNativeAppWith(path, p -> {
            try {
                // Allow proxy to start
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Experiment experiment = new Experiment(BBCompArgs.parse(args));
            experiment.run();
        });
    }

    private final BBCompArgs args;
    private final BBCompClient client;

    private Experiment(BBCompArgs args) {
        this.args = args;
        this.client = new BBCompClient(LOG_DIR.toString(), args.getUsername(), args.getPassword());
    }

    @SneakyThrows
    private void run() {
        client.setup();

        printAvailableTracks(client);
    }

    private static void printAvailableTracks(BBCompClient client) throws BBCompException {
        ArrayList<String> tracks = client.listTracks();
        System.out.println( String.format("There is %d tracks", tracks.size()));
        for (int i = 0; i < tracks.size(); i++) {
            System.out.println(String.format("  %d: %s", i, tracks.get(i)));
        }
    }
}

package pl.edu.pw.mini.optimization.bbcomp.exception;

public class BBCompException extends Exception {
    public BBCompException(String msg, Throwable ex) {
        super(msg, ex);
    }
}

package pl.edu.pw.mini.optimization.bbcomp;

import lombok.Getter;

class BBCompArgs {
    @Getter
    private String algorithmName;
    @Getter
    private String username;
    @Getter
    private String password;
    @Getter
    private String trackName;
    @Getter
    private int problemFrom;
    @Getter
    private int problemTo;

    private BBCompArgs(String algorithmName, String username, String password, String trackName, int problemFrom, int problemTo) {
        this.algorithmName = algorithmName;
        this.username = username;
        this.password = password;
        this.trackName = trackName;
        this.problemFrom = problemFrom;
        this.problemTo = problemTo;
    }

    public static BBCompArgs parse(String[] args) throws IllegalArgumentException {
        for (int i = 0; i < args.length; ++i)
            System.err.println(args[i]);
        if (args.length == 0) {
            return new BBCompArgs("all", "demoaccount", "demopassword", "trial", 0, 100);
        } else if (args.length == 1) {
            return new BBCompArgs(args[0], "demoaccount", "demopassword", "trial", 0, 1000);
        } else if (args.length == 3) {
            return new BBCompArgs(args[0], args[1], args[2], "trial", 0, 1000);
        } else if (args.length == 6) {
            if (!"trial".equals(args[3])) {
                System.out.printf("Are you sure to run track %s?\n", args[3]);
                if ("y".equals(System.console().readLine())) {
                    return new BBCompArgs(args[0], args[1], args[2], args[3], Integer.parseInt(args[4]), Integer.parseInt(args[5]));
                }
            }
            return new BBCompArgs(args[0], args[1], args[2], "trial", Integer.parseInt(args[4]), Integer.parseInt(args[5]));
        } else {
            throw new IllegalArgumentException("The app must be run with either 0, 1 (algorithm name) or 3 parameters (algorithm name, username, password).");
        }
    }
}

package pl.edu.pw.mini.optimization.bbcomp;

import lombok.SneakyThrows;
import pl.edu.pw.mini.optimization.bbcomp.client.BBCompClient;
import pl.edu.pw.mini.optimization.bbcomp.exception.BBCompBudgetExceededException;
import pl.edu.pw.mini.optimization.bbcomp.utils.NativeUtils;
import pl.edu.pw.mini.optimization.core.GPSOConfigurator;
import pl.edu.pw.mini.optimization.core.GeneralizedParticleSwarm;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.NonoverlappingRegionsExploringLocationInitializer;
import pl.edu.pw.mini.optimization.core.initialization.PreviousOptimaAwareLocationInitializer;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;
import pl.edu.pw.mini.optimization.utils.Loggers;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An example of benchmarking random search on a COCO suite.
 * <p>
 * Set the parameter BUDGET_MULTIPLIER to suit your needs.
 */
public class BBCompComputations {
    final static GPSOConfigurator configurator = PropertiesAccessor.getGPSOConfigurationInstance();

    /**
     * The maximal number of independent restarts allowed for an algorithm that
     * restarts itself.
     */
    private static final int INDEPENDENT_RESTARTS = 10000;

    private static final Path LOG_DIR = Paths.get("exdata").toAbsolutePath();


//
//    /**
//     * The random seed. Change if needed.
//     */
//    public static final long RANDOM_SEED = 0xdeadbeef;
    /**
     * The problem to be optimized (needed in order to simplify the interface
     * between the optimization algorithm and the COCO platform).
     */
    private BBCompProblem PROBLEM;

    /**
     * The main method initializes the random number generator and calls the
     * example experiment on the bi-objective suite.
     */

    private final BBCompArgs args;

    private final BBCompClient client;

    public BBCompComputations(BBCompArgs args) {
        this.args = args;
        this.client = new BBCompClient(LOG_DIR.toString(), args.getUsername(), args.getPassword());
    }

    @SneakyThrows
    public static void main(String[] args) {

        System.out.println("Running the example experiment... (might take time, be patient)");
        System.out.println("Algorithm " + Behaviour.toString(configurator.getAlgorithmsInitialPollList()));
        System.out.flush();

        try {
            runWithProxy("lib/proxy", args);
        } catch (IOException e) {
            runWithProxy("bbcomp/lib/proxy", args);
        }

        System.out.println("Done!");
        System.out.flush();
    }

    private static void runWithProxy(String path, String[] args) throws IOException {
        NativeUtils.runNativeAppWith(path, p -> delayedExperiment(args, p));
    }

    @SneakyThrows
    private static void delayedExperiment(String[] args, Process p) {
        try {
            // Allow proxy to start
            Thread.sleep(100);

        new BBCompComputations(BBCompArgs.parse(args))
                .exampleExperiment();
        } finally {
            p.destroy();
        }
    }

    /**
     * A simple example of benchmarking random search on a given suite with
     * default instances that can serve also as a timing experiment.
     *
     */
    private void exampleExperiment() {
        try {
            String trackName = args.getTrackName();
            client.setup();
            client.setTrack(trackName);
            int numProblems = client.getNumberOfProblems();
            System.out.println(String.format("number of problems in the track: %d", numProblems));
            /* Iterate over all problems in the suite */
            for (int problemId = args.getProblemFrom(); problemId < args.getProblemTo(); problemId++) {
                try {
                System.out.println("----------------");
                client.setProblem(problemId);
                System.out.println(String.format("selected problem: %d", problemId));
                PROBLEM = new BBCompProblem(client);
                int dimension = PROBLEM.getDimension();
                System.out.println("new problem " + PROBLEM.getName());

                SamplesGatherer f = new SamplesGatherer((new Function() {

                    @Override
                    public double getValue(double[] x) {
                        return (PROBLEM.evaluateFunction(x))[0];
                    }

                    @Override
                    public Bounds getBounds() {
                        return new Bounds(
                                PROBLEM.getSmallestValuesOfInterest(),
                                PROBLEM.getLargestValuesOfInterest()
                        );
                    }

                    @Override
                    public int getDimension() {
                        return PROBLEM.getDimension();
                    }

                    @Override
                    public String getName() {
                        return PROBLEM.getName();
                    }
                }), configurator.getModelEstimationProbability());

                PreviousOptimaAwareLocationInitializer locationInitializer =
                        new NonoverlappingRegionsExploringLocationInitializer(f);

                /* Run the algorithm at least once */
                List<RTree> tabooIndexes = new ArrayList<>();
                List<Behaviour> behaviours = Behaviour.getBehaviours(configurator.getAlgorithmsInitialPollList());
                RESTART: for (int run = 1; run <= 1 + INDEPENDENT_RESTARTS; run++) {
                    Loggers.SWARM_LOGGER.registerRestart();
                    if (configurator.resetAdaptationWeightAtRestart()) {
                        behaviours = Behaviour.getBehaviours(configurator.getAlgorithmsInitialPollList());
                    }
                    final int POP_SIZE = configurator.getPopulationSizeAdjuster().getSize(
                            dimension,
                            (int) Math.round(Math.log10(configurator.getBudgetMultiplier())),
                            behaviours.size());

                    Loggers.registerRuns(
                            Behaviour.getId(configurator.getAlgorithmsInitialPollList()) + "_"
                                    + PropertiesAccessor.getInstance().getVersion(),
                            PROBLEM.getId(),
                            PROBLEM.getDimension()
                    );
                    locationInitializer.resetState();
                    GeneralizedParticleSwarm gpso = new GeneralizedParticleSwarm(
                            f,
                            POP_SIZE,
                            configurator.getSwarmRestartManager(PROBLEM.getDimension()),
                            locationInitializer,
                            behaviours);

                    long evaluationsDone = PROBLEM.getEvaluations();
                    long evaluationsRemaining = (long) (dimension * configurator.getBudgetMultiplier()) - evaluationsDone;

                    double[] optimum = null;
                    double bestValue = Double.POSITIVE_INFINITY;
                    /* Break the loop if the target was hit or there are no more remaining evaluations */
                        while (!PROBLEM.isFinalTargetHit() && (evaluationsRemaining > 0)) {
                            optimum = gpso.optimize(
                                    10);
                            bestValue = gpso.getOptimumValue();
                            evaluationsDone = PROBLEM.getEvaluations();
                            evaluationsRemaining = (long) (dimension * configurator.getBudgetMultiplier()) - evaluationsDone;
                            if (gpso.shouldBeRestarted()) {
                                if (configurator.isTabuInitializerToBeUsed()) tabooIndexes.add(gpso.getSamplesIndex());
                                gpso.clearMemory();
                                behaviours.forEach(bhv -> bhv.resetResultsAccumulation());
                                SingleSample optimumEstimationSample = new SingleSample(optimum, bestValue);
                                Loggers.SUBRUNS_LOGGER.registerEstimatedOptimum(optimumEstimationSample);
                                locationInitializer.registerNewOptimumEstimation(optimumEstimationSample);
                                continue RESTART;
                            }
                        }
                        if (optimum != null) {
                            SingleSample optimumEstimationSample = new SingleSample(optimum, bestValue);
                            Loggers.OPTIMA_LOGGER.registerOptimum(optimumEstimationSample);
                            Loggers.SUBRUNS_LOGGER.registerEstimatedOptimum(optimumEstimationSample);
                        }
                        tabooIndexes.forEach(rTree -> rTree.clearIndex());
                        gpso.clearMemory();

                        /* Break the loop if the algorithm performed no evaluations or an unexpected thing happened */
                        if (PROBLEM.getEvaluations() == evaluationsDone) {
                            System.out.println("WARNING: Budget has not been exhausted (" + evaluationsDone + "/"
                                    + dimension * configurator.getBudgetMultiplier() + " evaluations done)!\n");
                            break;
                        } else if (PROBLEM.getEvaluations() < evaluationsDone)
                            System.out.println("ERROR: Something unexpected happened - function evaluations were decreased!");
                    }
                }
                catch (BBCompBudgetExceededException ex) {
                    System.err.println(ex.getMessage());
                    System.err.println("Going to solve next problem");
                    continue;
                }
            }


            Loggers.complete();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

package pl.edu.pw.mini.optimization.bbcomp.utils;

/*
 * Class NativeUtils is published under the The MIT License:
 *
 * Copyright (c) 2012 Adam Heinrich <adam@adamh.cz>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.io.*;
import java.nio.file.*;
import java.util.Scanner;
import java.util.function.Consumer;

public class NativeUtils {

    private static String OS = System.getProperty("os.name").toLowerCase();
    private static final String NATIVE_FOLDER_PATH_PREFIX = "nativeutils";

    private static File temporaryDir;

    private NativeUtils() {
    }

    public static void loadLibrary(String name) throws RuntimeException {
        String fullPath = toSystemDependentFile(name, "lib", "so", "", "dll");
        try {
            String absPath = Paths.get(fullPath).toAbsolutePath().toString();
            System.load(absPath);
        } catch (java.lang.UnsatisfiedLinkError e1) {
            try {
                withNativeFileInJar("/" + fullPath, System::load);
            } catch (Exception e2) {
                e2.printStackTrace();
                throw new java.lang.UnsatisfiedLinkError("No " + name + " in java.library.path or in JAR file");
            }
        }
    }

    public static void runNativeAppWith(String name, Consumer<Process> code) throws IOException {
        String fullPath = toSystemDependentFile(name, "", "", "", "exe");

        File f = new File(fullPath);
        if (f.exists()) {
            runWith(fullPath, code);
        } else {
            try {
                withNativeFileInJar("/" + name, p -> {
                    try {
                        runWith(p, code);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw e;
                }
            }
        }
    }

    public static void withNativeFileInJar(
            String path,
            Consumer<String> consumer) throws IOException {
        if (!path.startsWith("/")) {
            throw new IllegalArgumentException("The path has to be absolute (start with '/').");
        }

        if (temporaryDir == null) {
            temporaryDir = createTempDirectory(NATIVE_FOLDER_PATH_PREFIX);
            temporaryDir.deleteOnExit();
        }

        String filename = Paths.get(path).getFileName().toString();
        File temp = new File(temporaryDir, filename);

        try (InputStream is = NativeUtils.class.getResourceAsStream(path)) {
            Files.copy(is, temp.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            temp.delete();
            throw e;
        } catch (NullPointerException e) {
            temp.delete();
            throw new FileNotFoundException("File " + path + " was not found inside JAR.");
        }

        try {
            consumer.accept(temp.getAbsolutePath());
        } finally {
            if (isPosixCompliant()) {
                temp.delete();
            } else {
                temp.deleteOnExit();
            }
        }

    }

    public static String toSystemDependentFile(
            String path,
            String linPrefix,
            String linExtension,
            String winPrefix,
            String winExtension) {
        String basePath = Paths.get(path).getParent().toString();
        String filename = Paths.get(path).getFileName().toString();

        if (isWindows()) {
            filename = winPrefix + filename;
            if (winExtension != null && !winExtension.equals("")) {
                filename += ".";
                filename += winExtension;
            }
        } else {
            filename = linPrefix + filename;
            if (linExtension != null && !linExtension.equals("")) {
                filename += ".";
                filename += linExtension;
            }
        }

        return (basePath + "/" + filename).replaceAll("//", "/");
    }

    private static void runWith(String path, Consumer<Process> code) throws IOException {
        Process proc = Runtime.getRuntime().exec(path);
        inheritIO(proc.getInputStream(), System.out);
        inheritIO(proc.getErrorStream(), System.err);

        code.accept(proc);

        proc.destroy();
    }

    private static boolean isPosixCompliant() {
        try {
            return FileSystems.getDefault()
                    .supportedFileAttributeViews()
                    .contains("posix");
        } catch (FileSystemNotFoundException
                | ProviderNotFoundException
                | SecurityException e) {
            return false;
        }
    }

    private static File createTempDirectory(String prefix) throws IOException {
        String tempDir = System.getProperty("java.io.tmpdir");
        File generatedDir = new File(tempDir, prefix + System.nanoTime());

        if (!generatedDir.mkdir())
            throw new IOException("Failed to create temp directory " + generatedDir.getName());

        return generatedDir;
    }

    private static boolean isWindows() {
        return OS.contains("win");
    }

    private static boolean isNonWindows() {
        return !isWindows();
    }

    private static void inheritIO(final InputStream src, final PrintStream dest) {
        new Thread(() -> {
            Scanner sc = new Scanner(src);
            while (sc.hasNextLine()) {
                dest.println(sc.nextLine());
            }
        }).start();
    }
}

package pl.edu.pw.mini.optimization.bbcomp.client;

import lombok.Getter;
import pl.edu.pw.mini.optimization.bbcomp.exception.BBCompException;

import java.util.ArrayList;
import java.util.Vector;

public class BBCompClient {
    private static final int LOGIN_DELAY_MS = 10 * 1000;
    private static final int LOCK_DELAY_MS = 60 * 1000;
    private static final int MAX_RETRIES = 10;

    private BBComp bbcomp;

    private final String logPath;
    private final String username;
    private final String password;

    @Getter
    private String track = null;
    @Getter
    private Integer problemId = null;

    public BBCompClient(String logPath, String username, String password) {
        this.logPath = logPath;
        this.username = username;
        this.password = password;
    }

    public void setup() throws BBCompException {
        retryInternal(false, "setup", () -> {
            setupInternal();
            return 0;
        });
    }

    public ArrayList<String> listTracks() throws BBCompException {
        return retry("listTracks", () -> {
            int no = bbcomp.numberOfTracks();
            if (no == 0) {
                fail("numberOfTracks() failed");
            }

            ArrayList<String> result = new ArrayList<>(no);
            for (int i = 0; i < no; i++) {
                String trackName = bbcomp.trackName(i);
                if ("".equals(trackName)) {
                    fail("trackName() failed");
                }
                result.add(trackName);
            }

            return result;
        });
    }

    public void setTrack(final String trackname) throws BBCompException {
        retry("setTrack", () -> {
            if (!bbcomp.setTrack(trackname)) {
                fail("setTrack failed");
            }
            return 0;
        });
        track = trackname;
    }

    public int getNumberOfProblems() throws BBCompException {
        return retryInt("getNumberOfProblems", () -> {
            int res = bbcomp.numberOfProblems();
            if (res == 0) {
                fail("numberOfProblems failed");
            }
            return res;
        });
    }

    public void setProblem(int problemId) throws BBCompException {
        retry("setProblem", () -> {
            if (!bbcomp.setProblem(problemId)) {
                if (bbcomp.errorMessage().startsWith("failed to acquire lock")) {
                    System.out.println("Failed to acquire lock, waiting and retrying");
                    try {
                        Thread.sleep(LOCK_DELAY_MS);
                    } catch (InterruptedException ignored) {
                    }
                }
                fail("setProblem failed");
            }
            return 0;
        });
        this.problemId = problemId;
    }

    public BBComp.EvaluateReturn evaluate(final Vector<Double> point) throws BBCompException {
        getBudget();        // For debugging purposes only TODO: Remove
        getEvaluations();   // For debugging purposes only TODO: Remove
        BBComp.EvaluateReturn result = retry("evaluate", () -> bbcomp.evaluate(point));
        if (!result.result) {
            getErrorMessage();
        }
        return result;
    }

    private String getErrorMessage() throws BBCompException {
        return retry("getErrorMessage", () -> bbcomp.errorMessage());
    }

    public int getDimension() throws BBCompException {
        return retryInt("getDimension", () -> {
            int res = bbcomp.dimension();
            if (res == 0) {
                fail("dimension failed");
            }
            return res;
        });
    }

    public int getBudget() throws BBCompException {
        return retryInt("getBudget", () -> {
            int res = bbcomp.budget();
            if (res < 0) {
                fail("budget failed");
            }
            return res;
        });
    }

    public int getEvaluations() throws BBCompException {
        return retryInt("getEvaluations", () -> {
            int res = bbcomp.evaluations();
            if (res < 0) {
                fail("evaluations failed");
            }
            return res;
        });
    }

    private void setupInternal() throws Exception {
        bbcomp = new BBComp();
        if (!bbcomp.configure(false, logPath)) {
            fail("configure() failed");
        }
        if (!bbcomp.login(username, password)) {
            if ("already logged in".equals(bbcomp.errorMessage())) {
                try {
                    Thread.sleep(LOGIN_DELAY_MS);
                } catch (InterruptedException ignored) {
                }
            }
            fail("login() failed");
        }
    }

    private void tryRestoreState() throws BBCompException {
        retryInternal(false, "restoreState", () -> {
            setupInternal();
            if (track != null) {
                if (!bbcomp.setTrack(track)) {
                    fail("setTrack failed");
                }
            }
            if (problemId != null) {
                if (!bbcomp.setProblem(problemId)) {
                    fail("setProblem failed");
                }
            }
            return 0;
        });
    }

    private void fail(String msg) throws Exception {
        msg = msg + ": " + bbcomp.errorMessage();
        System.out.println(msg);
        throw new Exception(msg);
    }

    private <TResult> TResult retryInternal(boolean restore, String actionName, BBCompAction<TResult> action) throws BBCompException {
        int retries = 0;
        Exception lastException = null;
        while (retries < MAX_RETRIES) {
            try {
                return action.perform();
            } catch (Exception ex) {
                lastException = ex;
                System.out.println(ex.toString() + " : " + ex.getMessage());

                if (restore) {
                    tryRestoreState();
                }
            }
            retries++;
        }
        System.out.println("Action " + actionName + " failed " + MAX_RETRIES + " times - aborting.");
        throw new BBCompException("Action " + actionName + " failed " + MAX_RETRIES + " times - check inner exception for more details.", lastException);
    }

    private <TResult> TResult retry(String actionName, BBCompAction<TResult> action) throws BBCompException {
        return retryInternal(true, actionName, action);
    }

    private int retryInt(String actionName, IntBBCompAction action) throws BBCompException {
        return retryInternal(true, actionName, action::perform);
    }

    @FunctionalInterface
    interface BBCompAction<T> {
        T perform() throws Exception;
    }

    @FunctionalInterface
    interface IntBBCompAction {
        int perform() throws Exception;
    }

    @FunctionalInterface
    interface VoidBBCompAction {
        void perform() throws Exception;
    }
}
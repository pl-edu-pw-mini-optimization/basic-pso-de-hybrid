package pl.edu.pw.mini.optimization.core.behaviour;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.RastriginFunction;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.RandomBoundLocationInitializer;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;

public class UCTBasedSearchTest {
    @Test
    public void testSamplingFrequency() {
        int zeroZeroBin = 0;
        int zeroOneBin = 0;
        int oneZeroBin = 0;
        int oneOneBin = 0;

        for (int largRuns = 0; largRuns < 10; ++largRuns) {
            SamplesGatherer function = new SamplesGatherer(new RastriginFunction(2), 0.0);
            for (int i = 0; i < 20; i++) {
                //sampling in four squares - each with local optimum (0.0, 1.0 etc.)
                function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() - 0.5});
                function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() + 0.5});
                function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() - 0.5});
                function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() + 0.5});
            }
            double[] solution = new double[function.getDimension()];
            Behaviour behaviour = new UCTSamplingBehaviour(1, 0.7);
            List<Particle> particles = new ArrayList<>();
            Particle particle = new Particle(function, particles, new RandomBoundLocationInitializer(function), solution);
            particle.initializeVelocity(particles);
            for (int i = 0; i < 1000; ++i) {
                behaviour.registerIteration(1);
                particle.sampleNext(behaviour);
                if (particle.getX()[0] < 0.5 && particle.getX()[1] < 0.5) {
                    ++zeroZeroBin;
                } else if (particle.getX()[0] >= 0.5 && particle.getX()[1] < 0.5) {
                    ++oneZeroBin;
                } else if (particle.getX()[0] < 0.5 && particle.getX()[1] >= 0.5) {
                    ++zeroOneBin;
                } else if (particle.getX()[0] >= 0.5 && particle.getX()[1] >= 0.5) {
                    ++oneOneBin;
                }
            }
        }

        //best bucket should dominate worse buckets
        Assert.assertTrue(zeroOneBin < zeroZeroBin);
        Assert.assertTrue(oneZeroBin < zeroZeroBin);
        //better buckets should dominate the worst bucket
        Assert.assertTrue(oneOneBin < zeroOneBin);
        Assert.assertTrue(oneOneBin < oneZeroBin);
        //in 100 samples at least one should fall into worst bucket
        //right now I could not make it work...
        Assert.assertTrue(0 < oneOneBin);
    }
}

package pl.edu.pw.mini.optimization.core.behaviour;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BehaviourTest {

    @Test
    public void testToString() {
        int[] setup = new int[]{0, 1};
        assertEquals("DE:1", Behaviour.toString(setup));

        setup = new int[]{2, 1};
        assertEquals("PSO-L-2007:2,DE:1", Behaviour.toString(setup));

        setup = new int[]{2, 1, 1, 0, 1};
        assertEquals("PSO-L-2007:2,DE:1,RS:1,LM:1", Behaviour.toString(setup));
    }

    @Test
    public void getId() {
        int[] setup = new int[]{0, 1};
        assertEquals("D_1", Behaviour.getId(setup));

        setup = new int[]{2, 1};
        assertEquals("PD_2_1", Behaviour.getId(setup));

        setup = new int[]{4, 3, 2, 0, 1};
        assertEquals("PDRL_4_3_2_1", Behaviour.getId(setup));
    }
}
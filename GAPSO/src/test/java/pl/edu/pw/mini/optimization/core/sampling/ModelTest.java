package pl.edu.pw.mini.optimization.core.sampling;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pw.mini.optimization.core.functions.LinearFunction;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.functions.SphereFunction;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.List;

public class ModelTest {
    @Test
    public void gettingParabolaPeakTest() {
        int dim = 2;
        SamplesGatherer function = new SamplesGatherer(new SphereFunction(dim), 0.0);
        for (int i = 0; i < 20; i++) {
            //sampling in four squares - each with local optimum (0.0, 1.0 etc.)
            function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() - 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() + 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() - 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() + 0.5});
        }

        List<Sample> sampleList = function.getNearestSamples(2 * dim + 1,new double[dim]);
        SquareModel sm = new SquareModel(dim, sampleList);
        double[] peak = sm.getBoundedParabolaPeak(function.getBounds());
        Assert.assertArrayEquals(new double[] {0.0, 0.0}, peak, 1e-8);

        sampleList = function.getNearestSamples(function.getSamplesCount(),new double[dim]);
        sm = new SquareModel(dim, sampleList);
        peak = sm.getBoundedParabolaPeak(function.getBounds());
        Assert.assertArrayEquals(new double[] {0.0, 0.0}, peak, 1e-8);
    }

    @Test
    public void gettingBestBoundLinearTest() {
        int dim = 2;
        SamplesGatherer function = new SamplesGatherer(new LinearFunction(dim), 0.0);
        for (int i = 0; i < 20; i++) {
            //sampling in four squares - each with local optimum (0.0, 1.0 etc.)
            function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() - 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() - 0.5, Generator.RANDOM.nextDouble() + 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() - 0.5});
            function.getValue(new double[]{Generator.RANDOM.nextDouble() + 0.5, Generator.RANDOM.nextDouble() + 0.5});
        }

        List<Sample> sampleList = function.getNearestSamples(2 * dim + 1,new double[dim]);
        SquareModel sm = new SquareModel(dim, sampleList);
        double[] peak = sm.getBoundedParabolaPeak(function.getBounds());
        Assert.assertArrayEquals(new double[] {-1.0, 3.0}, peak, 1e-8);

        sampleList = function.getNearestSamples(function.getSamplesCount(),new double[dim]);
        sm = new SquareModel(dim, sampleList);
        peak = sm.getBoundedParabolaPeak(function.getBounds());
        Assert.assertArrayEquals(new double[] {-1.0, 3.0}, peak, 1e-8);
    }
}

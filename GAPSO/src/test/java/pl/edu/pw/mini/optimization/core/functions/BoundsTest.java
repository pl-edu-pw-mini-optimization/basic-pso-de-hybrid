package pl.edu.pw.mini.optimization.core.functions;

import org.junit.Assert;
import org.junit.Test;

public class BoundsTest {
    @Test
    public void testBoundsShrinking() {
        Bounds boundsToBeShrunk = new Bounds(-1.0, 3.0, 2);
        Bounds shrinkingBounds = new Bounds(2.0, 4.0, 2);
        boundsToBeShrunk.shrinkBoundsWithinBounds(shrinkingBounds.toArray());

        Assert.assertArrayEquals(new double[] {2.0, 2.0}, boundsToBeShrunk.getLower(), 1e-8);
        Assert.assertArrayEquals(new double[] {3.0, 3.0}, boundsToBeShrunk.getUpper(), 1e-8);
    }
}

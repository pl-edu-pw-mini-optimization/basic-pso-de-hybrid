package pl.edu.pw.mini.optimization.core.sampling;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class SampleTest {

    @Test
    public void getKOrderedSamples() {
        //DATA
        SingleSample sample_1_0__0_0 = new SingleSample(new double[] {1.0, 0.0}, 0.0);
        SingleSample sample_2_0__0_0 = new SingleSample(new double[] {2.0, 0.0}, 0.0);
        SingleSample sample_1_4__0_0 = new SingleSample(new double[] {1.4, 0.0}, 0.0);
        SingleSample sample_1_6__0_0 = new SingleSample(new double[] {1.6, 0.0}, 0.0);

        //SETUP
        Cluster cluster = new Cluster(sample_1_0__0_0, 2);
        cluster.add(sample_2_0__0_0);
        cluster.add(sample_1_4__0_0);
        cluster.add(sample_1_6__0_0);

        //TEST
        List<DistancedSample> samples = cluster.getKOrderedSamples(new double[] {1.35, 0}, 2);
        Assert.assertArrayEquals(
                new double[] {1.4, 0.0},
                samples.get(0).getX(),
                0.0);
        Assert.assertArrayEquals(
                new double[] {1.6, 0.0},
                samples.get(1).getX(),
                0.0);
    }
}
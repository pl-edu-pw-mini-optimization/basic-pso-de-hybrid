package pl.edu.pw.mini.optimization.core.behaviour;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.RandomBoundLocationInitializer;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;

public class TabooSearchTest {
    public class FlatBoringFunction extends Function {
        private final Bounds bounds;
        private int dim;

        public FlatBoringFunction(int dim) {
            this.dim = dim;
            this.bounds = new Bounds(0.0, 2.0, dim);
        }

        @Override
        public double getValue(double[] x) {
            return 5.0;
        }

        @Override
        public Bounds getBounds() {
            return bounds;
        }

        @Override
        public int getDimension() {
            return dim;
        }

        @Override
        public String getName() {
            return "Everywhere 5.0";
        }
    }

    @Test
    public void testDistributionAfterTaboo() {
        int dim = 2;
        int initialSamples = 1000;

        SamplesGatherer sg = new SamplesGatherer(new FlatBoringFunction(dim), 0.0);
        for (int i = 0; i < initialSamples; ++i) {
            sg.getValue(
                    new double[] {Generator.RANDOM.nextDouble(),
                            Generator.RANDOM.nextDouble()}
            );
        }

        Behaviour b = new TabooSearchBehaviour(1.0, 100);
        List<Particle> particles = new ArrayList<>();
        double[] solution = new double[sg.getDimension()];
        Particle p = new Particle(sg, particles, new RandomBoundLocationInitializer(sg),solution);
        p.initializeVelocity(particles);
        int totalSamples = initialSamples / 100;
        int countTabooed = 0;
        for (int i = 0; i < totalSamples; ++i) {
            b.registerIteration(1);
            p.sampleNext(b);
            double[] x = p.getX();
            if (x[0] > 1 || x[1] > 1) {
                countTabooed++;
            }
        }

        Assert.assertTrue(totalSamples / Math.pow(2,dim) >= (totalSamples - countTabooed));
    }
}

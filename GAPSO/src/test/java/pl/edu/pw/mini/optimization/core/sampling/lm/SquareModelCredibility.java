package pl.edu.pw.mini.optimization.core.sampling.lm;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.core.functions.RastriginFunction;
import pl.edu.pw.mini.optimization.core.functions.SphereFunction;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;

public class SquareModelCredibility {
    @Test
    public void testSquareFunction() {
        int dim = 2;
        double minBound = -1.0;
        double maxBound = 1.0;
        Function function = new SphereFunction(dim);
        SquareModel sm = prepareModelFromSamples(dim, minBound, maxBound, function);

        double[] testX = Generator.generateRandomWithinSimpleBounds(minBound, maxBound, dim);
        CredibleValue cv = sm.evaluateInModel(testX);
        Assert.assertEquals(function.getValue(testX), cv.getValue(), 1e-4);
        Assert.assertEquals(1.0, sm.getRSquared(), 1e-4);
    }

    private SquareModel prepareModelFromSamples(int dim, double minBound, double maxBound, Function function) {
        List<Sample> sampleList = new ArrayList<>();
        for (int i = 0; i < 2 * (2 * dim + 1); ++i) {
            double[] x = Generator.generateRandomWithinSimpleBounds(minBound, maxBound, dim);
            double y = function.getValue(x);
            sampleList.add(new SingleSample(x,y));
        }
        return new SquareModel(dim, sampleList);
    }

    @Test
    public void testRastriginFunction() {
        int dim = 2;
        double minBound = 0.95;
        double maxBound = 1.05;
        Function function = new RastriginFunction(dim);
        SquareModel sm = prepareModelFromSamples(dim, minBound, maxBound, function);

        double differences = 0.0;
        double deltas = 0.0;
        int count = 1000;
        for (int i = 0; i < count; ++i) {
            double[] testX = Generator.generateRandomWithinSimpleBounds(minBound, maxBound, dim);
            CredibleValue cv = sm.evaluateInModel(testX);
            double realValue = function.getValue(testX);
            double modelValue = cv.getValue();
            deltas += cv.getRegressionSE();
            differences += Math.abs(realValue - modelValue);
        }
        Assert.assertTrue("Average difference should be less then " + 4 * deltas / count + "(was " + differences / count + ")", differences / count < 4 * deltas / count);
        Assert.assertTrue("Model RSquared should be better than 0.95 (was " +  sm.getRSquared() + ")", sm.getRSquared() > 0.95);
    }

}


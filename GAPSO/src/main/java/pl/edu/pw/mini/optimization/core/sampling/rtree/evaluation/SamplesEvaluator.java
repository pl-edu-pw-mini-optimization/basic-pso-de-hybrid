package pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation;

import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SamplesEvaluator {
    private List<RTree> rTrees = new ArrayList<>();

    public SamplesEvaluator(RTree rTree) {
        rTrees.add(rTree);
    }

    public SamplesEvaluator(List<RTree> rTrees) {
        this.rTrees.addAll(rTrees);
    }

    public Evaluation getEvaluation(double[] x) {
        Evaluation evaluation = new Evaluation();
        double largestAverageDistance = Double.NEGATIVE_INFINITY;
        int dimPlusOne = x.length + 1;
        for (RTree rTree: rTrees) {
            boolean contained = rTree.someLeafOverlaps(x);
            double evaluatedAverageDistance = rTree.getKNearestSamples(x, dimPlusOne)
                    .stream()
                    .mapToDouble(s -> s.getDistance(x))
                    .average()
                    .getAsDouble();
            evaluation.addMeasurement(contained, evaluatedAverageDistance);
        }
        return evaluation;
    }
}

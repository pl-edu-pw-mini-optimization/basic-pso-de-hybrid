package pl.edu.pw.mini.optimization.core.functions;

import java.util.Arrays;

public class Bounds {
    private double[] lower;
    private double[] upper;

    /**
     * Creating bounds object from single array for backward compatibility
     * @param bounds
     */
    @Deprecated
    public Bounds(double[][] bounds) {
        this(bounds[0], bounds[1]);
    }

    /**
     * Creating bounds object from pair of arrays
     * @param lower
     * @param upper
     */
    public Bounds(double[] lower, double[] upper) {
        if (upper.length != lower.length)
            throw new IllegalArgumentException("Lower and upper bounds have different lengths");
        int dimension = upper.length;
        this.upper = Arrays.copyOf(upper, dimension);
        this.lower = Arrays.copyOf(lower, dimension);
    }

    /**
     * Creating bounds object from values and dimension (identical bounds in each dimension)
     * @param lowerValue
     * @param upperValue
     * @param dimension
     */
    public Bounds(double lowerValue, double upperValue, int dimension) {
        this.lower = new double[dimension];
        this.upper = new double[dimension];
        Arrays.fill(this.lower, lowerValue);
        Arrays.fill(this.upper, upperValue);
    }

    /**
     * Introduced during refactoring in order mot to blow the whole code up in one go
     * @return
     */
    @Deprecated
    public double[][] toArray() {
        double[][] boundsArray = new double[2][];
        boundsArray[0] = this.lower;
        boundsArray[1] = this.upper;
        return boundsArray;
    }

    public double[] getLower() {
        return lower;
    }

    public double[] getUpper() {
        return upper;
    }

    public void shrinkBoundsWithinBounds(double[][] newBounds) {
        for (int d = 0; d < lower.length; ++d) {
            lower[d] = Math.min(Math.max(lower[d], newBounds[0][d]), upper[d]);
            upper[d] = Math.max(Math.min(upper[d], newBounds[1][d]), lower[d]);
        }
    }
}

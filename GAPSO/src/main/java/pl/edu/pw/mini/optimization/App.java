package pl.edu.pw.mini.optimization;

import pl.edu.pw.mini.optimization.core.GeneralizedParticleSwarm;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.functions.*;
import pl.edu.pw.mini.optimization.core.initialization.RandomBoundLocationInitializer;
import pl.edu.pw.mini.optimization.core.restart.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    private static final int DIM = 2;
    private static final int RUNS = 1;
    private static final int POP_SIZE = 40;
    private static final int ITERS = 1000;

    public static void main(String[] args) {
        //Some standard test functions
        List<SamplesGatherer> functions = new ArrayList<>();
        functions.add(new SamplesGatherer(new SphereFunction(DIM),0.0));
        functions.add(new SamplesGatherer(new RosenbrockFunction(DIM),0.0));
        functions.add(new SamplesGatherer(new RastriginFunction(DIM),0.0));

        //Various proportions of PSO to DE
        List<int[]> setups = new ArrayList<>();
        //setups.add(new int[]{0,0,0,0,1});
        //setups.add(new int[]{1,1,0,0,1});
        setups.add(new int[]{11, 11, 0, 0, 0, 0});
        setups.add(new int[]{10, 10, 1, 0, 0, 1});
        setups.add(new int[]{10, 10, 1, 0, 1, 0});
        setups.add(new int[]{10, 10, 0, 0, 1, 1});
        setups.add(new int[]{0, 0, 20, 0, 0, 1});
        //setups.add(new int[]{16,4,0,0,1});
        //setups.add(new int[]{4,16,0,0,1});
        //setups.add(new int[]{4,1});
        //setups.add(new int[]{4,1,0,1});
        //setups.add(new int[]{0,0,0,1});
        //setups.add(new int[]{4,1,1,1});
        //setups.add(new int[]{1,0,0,1});
        //setups.add(new int[]{0,1,0,1});
        //setups.add(new int[]{1,1,0,1});

        //Number of repetitions of each optimization

        for (int[] setup : setups) {
            List<Behaviour> behaviours = Behaviour.getBehaviours(setup);
            System.out.println(Behaviour.toString(setup));
            for (SamplesGatherer f : functions) {
                List<Double> results = new ArrayList<>();
                double result = 0.0;
                System.out.print(
                        f.getName() + ":"
                );
                for (int run = 0; run < RUNS; ++run) {
                    ArrayList<SwarmRestartManager> restartManagers = new ArrayList<>();
                    restartManagers.add(new NoGlobalImprovementRestartManager(30));
                    restartManagers.add(new ParticlesBestLocationsCollapsed(1e-10, f.getDimension()));
                    CompositeRestartManager restartManager = new AndCompositeRestartManager(restartManagers);

                    GeneralizedParticleSwarm gpso = new GeneralizedParticleSwarm(
                            f,
                            POP_SIZE,
                            restartManager,
                           new RandomBoundLocationInitializer(f),
                            behaviours);
                    double[] solution = gpso.optimize(
                            ITERS);
                    double currentResult = f.getValue(solution);
                    results.add(currentResult);
                    result += currentResult / RUNS; //aggregating results across runs
                }
                System.out.println(result);
//                System.out.println(
//                        Arrays.toString(result)
//                );
                f.getIndex().clearIndex();
            }
        }
    }

}

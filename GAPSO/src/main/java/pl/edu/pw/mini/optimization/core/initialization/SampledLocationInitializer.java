package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.LinearModel;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;

import java.util.List;

public abstract class SampledLocationInitializer extends BaseLocationInitializer {
    protected final SamplesGatherer sampledFunction;

    SampledLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
        this.sampledFunction = sampledFunction;
    }

    @Override
    public void resetState() {
        //DO NOTHING ON PURPOSE
    }
}

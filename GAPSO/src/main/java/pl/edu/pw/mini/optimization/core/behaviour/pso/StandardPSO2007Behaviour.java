package pl.edu.pw.mini.optimization.core.behaviour.pso;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.List;

public abstract class StandardPSO2007Behaviour extends PSOBehaviour {

    public StandardPSO2007Behaviour(double weight, double omega, double c1, double c2) {
        super(weight, omega, c1, c2);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        double[] newV = new double[particle.getV().length];
        List<Particle> particles = particle.getParticles();
        double[] localXBest = getBestLocation(particle, particles);
        Loggers.MOVES_LOGGER.registerBaseSample(particle.getXBest(), "PersonalBest");
        Loggers.MOVES_LOGGER.registerBaseSample(particle.getX(), "CurrentX");
        Loggers.MOVES_LOGGER.registerBaseSample(localXBest, "LocalBest");
        double[] oldX = new double[particle.getX().length];
        for (int dim = 0; dim < oldX.length; ++dim) {
            oldX[dim] = particle.getX()[dim] - particle.getV()[dim];
        }
        Loggers.MOVES_LOGGER.registerBaseSample(oldX, "PreviousX");
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = _omega * particle.getV()[i] +
                    _c1 * Generator.RANDOM.nextDouble() * (particle.getXBest()[i] - particle.getX()[i]) +
                    _c2 * Generator.RANDOM.nextDouble() * (localXBest[i] - particle.getX()[i]);
        }
        return newV;
    }

    protected abstract double[] getBestLocation(Particle particle, List<Particle> particles);

}

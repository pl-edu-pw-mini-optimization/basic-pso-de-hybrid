package pl.edu.pw.mini.optimization.core.initialization.bounds;

import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.PropertiesAccessor.getGPSOConfigurationInstance;

public class Region {
    private double[][] regionForExploration;
    private Sample[][] splitters;
    private double[] spread;
    private boolean[][] isBoundary;
    private double ratio;
    private int testCount;

    public Region(double[][] bounds) {
        regionForExploration = new double[2][];
        regionForExploration[0] = Arrays.copyOf(bounds[0], bounds[0].length);
        regionForExploration[1] = Arrays.copyOf(bounds[1], bounds[1].length);
        spread = new double[bounds[0].length];
        for (int dim = 0; dim < spread.length; ++dim) {
            spread[dim] = regionForExploration[1][dim] - regionForExploration[0][dim];
        }
        isBoundary = new boolean[2][];
        isBoundary[0] = new boolean[bounds[0].length];
        isBoundary[1] = new boolean[bounds[1].length];
        Arrays.fill(isBoundary[0], true);
        Arrays.fill(isBoundary[1], true);

        splitters = new Sample[2][];
        splitters[0] = new Sample[bounds[0].length];
        splitters[1] = new Sample[bounds[1].length];
        Arrays.fill(splitters[0], null);
        Arrays.fill(splitters[1], null);

        ratio = getGPSOConfigurationInstance().getInitialExplorationAreaRatio();
        testCount = 0;
    }

    private Region(Region parentRegion, int bestCutDimension, boolean isUpperRegion, Sample splitter) {
        this(parentRegion.regionForExploration);
        splitters[0] = Arrays.copyOf(parentRegion.splitters[0], parentRegion.splitters[0].length);
        splitters[1] = Arrays.copyOf(parentRegion.splitters[1], parentRegion.splitters[1].length);
        isBoundary[0] = Arrays.copyOf(parentRegion.isBoundary[0], parentRegion.isBoundary[0].length);
        isBoundary[1] = Arrays.copyOf(parentRegion.isBoundary[1], parentRegion.isBoundary[1].length);
        regionForExploration[isUpperRegion ? 0 : 1][bestCutDimension] = splitter.getX()[bestCutDimension];
        isBoundary[isUpperRegion ? 0 : 1][bestCutDimension] = false;
        splitters[isUpperRegion ? 0 : 1][bestCutDimension] = splitter;
    }

    public void moveBoundary(Sample oldSplitter, Sample newSplitter) {
        for (int dim = 0; dim < splitters[0].length; ++dim) {
            if (oldSplitter.equals(splitters[0][dim])) {
                splitters[0][dim] = newSplitter;
                regionForExploration[0][dim] = newSplitter.getX()[dim];
            }
            if (oldSplitter.equals(splitters[1][dim])) {
                splitters[1][dim] = newSplitter;
                regionForExploration[1][dim] = newSplitter.getX()[dim];
            }
        }
    }

    public List<Region> splitRegion(Sample splitter) {
        double[] splittingPoint = splitter.getX();
        int bestSplitDimension = -1;
        double minWidth = 0;
        for (int dim = 0; dim < splittingPoint.length; ++dim) {
            double testWidth = Math.min(splittingPoint[dim] - regionForExploration[0][dim],
                    regionForExploration[1][dim] - splittingPoint[dim]);
            if (testWidth > minWidth) {
                minWidth = testWidth;
                bestSplitDimension = dim;
            }
        }

        Region lowerRegion = new Region(this,
                bestSplitDimension, false, splitter);
        Region upperRegion = new Region(this,
                bestSplitDimension, true, splitter);

        ArrayList<Region> regions = new ArrayList<>();
        regions.add(lowerRegion);
        regions.add(upperRegion);
        return regions;
    }

    public double[][] getCurrentAreaOfInterest() {
        double[][] areaOfInterest = new double[2][];
        areaOfInterest[0] = Arrays.copyOf(regionForExploration[0], regionForExploration[0].length);
        areaOfInterest[1] = Arrays.copyOf(regionForExploration[1], regionForExploration[1].length);
        for (int dim = 0; dim < spread.length; ++dim) {
            if (!isBoundary[0][dim] && !isBoundary[1][dim]) {
                areaOfInterest[0][dim] += (1 - ratio) * 0.5 * spread[dim];
                areaOfInterest[1][dim] -= (1 - ratio) * 0.5 * spread[dim];
            } else if (isBoundary[0][dim]) {
                areaOfInterest[1][dim] -= (1 - ratio) * spread[dim];
            } else if (isBoundary[1][dim]) {
                areaOfInterest[0][dim] += (1 - ratio) * spread[dim];
            }
        }
        return areaOfInterest;
    }

    public void selectToExplore() {
        testCount++;
        if (ratio > 1e-1) {
            ratio /= 2.0;
        } else {
            ratio = getGPSOConfigurationInstance().getInitialExplorationAreaRatio();
        }
    }

    public boolean isPointStrictlyInside(double[] x) {
        for (int dim = 0; dim < x.length; ++dim){
            if (x[dim] >= regionForExploration[1][dim]
                    || x[dim] <= regionForExploration[0][dim]) {
                return false;
            }
        }
        return true;
    }

    public boolean isPointInside(double[] x) {
        for (int dim = 0; dim < x.length; ++dim){
            if (x[dim] > regionForExploration[1][dim]
                    || x[dim] < regionForExploration[0][dim]) {
                return false;
            }
        }
        return true;
    }
}

package pl.edu.pw.mini.optimization.core.behaviour;

public class UCTSamplingUnboundedModelBehaviour extends UCTSamplingModelBehaviour {
    public UCTSamplingUnboundedModelBehaviour(double weigth, double cFactor) {
        super(weigth, cFactor);
    }

    @Override
    protected double[][] getBoundsForResult(double[][] bounds, double[][] functionBounds) {
        return functionBounds;
    }
}

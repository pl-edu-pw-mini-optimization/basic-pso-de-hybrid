package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.utils.NamedObject;

public class NullNextMoveSamplesLogger implements NextMoveSamplesLogger {
    @Override
    public void registerNewMove() {
        //empty on purpose
    }

    @Override
    public void registerBaseSample(double[] location, String sampleRole) {
        //empty on purpose
    }

    @Override
    public void registerEffectSample(double[] location, NamedObject moveType) {
        //empty on purpose
    }

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        //empty on purpose
    }

    @Override
    public void registerIteration() {
        //empty on purpose
    }

    @Override
    public void finalize() {
        //empty on purpose
    }
}

package pl.edu.pw.mini.optimization.core.functions;


import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.optim.PointValuePair;

import java.util.ArrayList;
import java.util.List;

public class FunctionWrapper implements MultivariateFunction {
    private final pl.edu.pw.mini.optimization.core.functions.Function _function;
    private final List<PointValuePair> _cache;

    public FunctionWrapper(pl.edu.pw.mini.optimization.core.functions.Function function) {
        _function = function;
        _cache = new ArrayList<>();
    }

    @Override
    public double value(double[] doubles) {
        for (PointValuePair sample : _cache) {
            boolean match = true;
            for (int i = 0; i < sample.getPoint().length; ++i) {
                if (sample.getPoint()[i] != doubles[i]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return sample.getValue();
            }
        }
        return _function.getValue(doubles);
    }

    public void addToCache(double[] sample, double value) {
        _cache.add(new PointValuePair(sample, value));
    }


}

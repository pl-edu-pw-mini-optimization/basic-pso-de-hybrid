package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;

import java.util.ArrayList;
import java.util.List;

public abstract class ParticlesRestartManager extends SwarmRestartManager {
    protected List<Particle> particles;

    ParticlesRestartManager() {
        particles = new ArrayList<>();
    }

    @Override
    public void registerNewIteration() {
        //DO NOTHING ON PURPOSE
    }

    @Override
    public void registerImprovement() {
        //DO NOTHING ON PURPOSE
    }

    @Override
    public void registerParticle(Particle particle) {
        particles.add(particle);
    }

}

package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Generator;

public class UCTSamplingBehaviour extends Behaviour {
    protected final double cFactor;

    public UCTSamplingBehaviour(double weigth, double cFactor) {
        super(weigth);
        this.cFactor = cFactor;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        particle.resetBest();

        double[][] bounds = particle.getFunction().getPromisingRectangleBounds(cFactor);
        double[] newV = new double[particle.getV().length];
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = -particle.getX()[i]
                    + Generator.RANDOM.nextDouble() * (
                    bounds[1][i] -
                            bounds[0][i])
                    + bounds[0][i];
        }
        return newV;
    }
}

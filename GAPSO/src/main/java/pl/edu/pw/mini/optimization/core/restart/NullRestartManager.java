package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;

public class NullRestartManager extends SwarmRestartManager {
    @Override
    public void registerNewIteration() {
        //DO COMPLETELY NOTHING
    }

    @Override
    public void registerImprovement() {
        //DO COMPLETELY NOTHING
    }

    @Override
    public void registerParticle(Particle particle) {
        //DO COMPLETELY NOTHING
    }

    @Override
    public boolean shouldBeRestarted() {
        return false;
    }

    @Override
    public String toString() {
        return "NoRestart";
    }
}

package pl.edu.pw.mini.optimization.core.sampling;

import java.util.*;
import java.util.stream.Collectors;

public class Cluster extends Sample {
    private Sample seed;
    private TreeSet<Sample> children;
    private long count;
    private double meanValue;
    private double radius;
    private int capacity;

    public Cluster(Sample seed, int capacity) {
        this.seed = seed;
        this.children = new TreeSet<>();
        this.children.add(seed);
        this.count = seed.getWeight();
        this.meanValue = seed.getValue();
        this.radius = seed.getRadius();
        this.capacity = capacity;
    }

    public boolean add(Sample sample) {
        if (children.size() >= capacity) {
            double[] searchPoint = sample.getX();
            Sample closestSeed = getClosestSample(searchPoint);
            if (closestSeed instanceof Cluster) {
                boolean addedSuccesfuly = ((Cluster) closestSeed).add(sample);
                if (addedSuccesfuly) {
                    updateCountMeanRadiusValues(sample);
                    return true;
                } else {
                    return false;
                }
            } else {
                children.remove(closestSeed);
                Cluster cluster = new Cluster(closestSeed, capacity);
                children.add(cluster);
                boolean addedSuccesfuly = cluster.add(sample);
                if (addedSuccesfuly) {
                    updateCountMeanRadiusValues(sample);
                    return true;
                } else {
                    return false;
                }
            }
        } else if (!children.contains(sample)) {
            children.add(sample);
            updateCountMeanRadiusValues(sample);
            return true;
        }
        return false;
    }

    private Sample getClosestSample(double[] searchPoint) {
        double minDistance = Double.POSITIVE_INFINITY;
        Sample closestSeed = null;
        for (Sample seed : children.descendingSet()) {
            double testDistance = seed.getDistance(searchPoint);
            if (testDistance < minDistance) {
                closestSeed = seed;
                minDistance = testDistance;
            }
        }
        return closestSeed;
    }

    private void updateCountMeanRadiusValues(Sample sample) {
        meanValue = (meanValue * count) / (count + sample.getWeight()) + (sample.getValue() * sample.getWeight()) / (count + sample.getWeight());
        count += sample.getWeight();
        radius = Math.max(radius, sample.getDistance(seed.getX()));
    }

    @Override
    public double[] getX() {
        return seed.getX();
    }

    @Override
    public double getValue() {
        return meanValue;
    }

    @Override
    public double getRadius() {
        return radius;
    }

    @Override
    public long getWeight() {
        return count;
    }

    @Override
    public List<DistancedSample> getKOrderedSamples(double[] point, int k) {
        List<DistancedSample> distancedSamples = new ArrayList<>();
        List<DistancedSample> orderedChildren =
                children.stream()
                        .map(sample -> new DistancedSample(sample, point))
                        .sorted(Comparator.comparingDouble(DistancedSample::getDistance))
                        .collect(Collectors.toList());

        for (DistancedSample orderedChild : orderedChildren) {
            //gather at least k samples if possible
            if (distancedSamples.size() < k) {
                distancedSamples =
                        DistancedSample.mergeOrderedLists(
                                distancedSamples,
                                orderedChild.getKOrderedSamples(point, k),
                                k);
                continue;
            }
            DistancedSample lastSample = distancedSamples.get(distancedSamples.size() - 1);
            if (lastSample.getDistance() > orderedChild.getDistance(point) - orderedChild.getRadius()) {
                distancedSamples =
                        DistancedSample.mergeOrderedLists(
                                distancedSamples,
                                orderedChild.getKOrderedSamples(point, k),
                                k);
            }
        }
        return distancedSamples;
    }

    public List<Sample> getChildren() {
        return new ArrayList<>(children);
    }
}

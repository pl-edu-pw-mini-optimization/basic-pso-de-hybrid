package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.ArrayList;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.PropertiesAccessor.getGPSOConfigurationInstance;

public class DefaultCompositeInitializer extends SampledLocationInitializer {
    List<BaseLocationInitializer> locationInitializers;

    public DefaultCompositeInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
        locationInitializers = new ArrayList<>();

        String[] initializersSequence = PropertiesAccessor.getGPSOConfigurationInstance().getInitializersSequence();
        for (String initializerName : initializersSequence) {
            if (initializerName.toLowerCase().equals("square"))
                locationInitializers.add(new QuadraticSingleFireSolverLocationInitializer(sampledFunction));
            else if (initializerName.toLowerCase().equals("linear"))
                locationInitializers.add(new LinearSolverLocationInitializer(sampledFunction));
            else if (initializerName.toLowerCase().equals("boundary"))
                locationInitializers.add(new BoundaryVertexLocationInitializer(sampledFunction));
            else if (initializerName.toLowerCase().equals("ea"))
                locationInitializers.add(new EvolutionaryAlgorithmInitializer(sampledFunction));
            else if (initializerName.toLowerCase().equals("de"))
                locationInitializers.add(new DEBest1BinLocationInitializer(sampledFunction));
            else
                throw new IllegalArgumentException(initializerName + " does not exist in initializers dictionary");
        }
        locationInitializers.add(new RandomBoundLocationInitializer(sampledFunction));
    }

    @Override
    public void resetState() {
        if (locationInitializers != null) {
            for (BaseLocationInitializer initializer : locationInitializers) {
                initializer.resetState();
            }
        }
    }

    @Override
    public double[] getNext() {
        if (locationInitializers != null) {
            for (BaseLocationInitializer initializer : locationInitializers) {
                if (initializer.isActive())
                    return initializer.getNext();
            }
        }
        return getRandomWithinFunctionBounds();
    }

    @Override
    public boolean isActive() {
        if (locationInitializers != null) {
            return locationInitializers
                    .stream()
                    .anyMatch(i -> i.isActive());
        } else{
            return false;
        }
    }

    @Override
    public void shrinkBoundsWithinBounds(double[][] newBounds) {
        super.shrinkBoundsWithinBounds(newBounds);
        if (locationInitializers != null) {
            for (BaseLocationInitializer initializer : locationInitializers) {
                initializer.shrinkBoundsWithinBounds(newBounds);
            }
        }
    }

    @Override
    protected void resetOriginalFunctionBounds(Function function) {
        super.resetOriginalFunctionBounds(function);
        if (locationInitializers != null) {
            for (BaseLocationInitializer initializer : locationInitializers) {
                initializer.resetOriginalFunctionBounds(function);
            }
        }
    }

}

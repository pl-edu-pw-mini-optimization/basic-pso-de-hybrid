package pl.edu.pw.mini.optimization.core;

import pl.edu.pw.mini.optimization.core.behaviour.GridSearchPseudoBehaviour;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.utils.Loggers;

public class GridSearch extends BaseOptimizer {
    private final int density;
    private final double[] currentPoint;
    private boolean breakComputations;

    public GridSearch(SamplesGatherer qualityFunction, int density) {
        super(qualityFunction);
        this.density = density;
        this.currentPoint = new double[qualityFunction.getDimension()];
        for (int dim = 0; dim < qualityFunction.getDimension(); ++dim) {
            currentPoint[dim] = (qualityFunction.getBounds().getUpper()[dim]
                    + qualityFunction.getBounds().getLower()[dim]) / 2.0;
        }
        this.breakComputations = false;
    }

    @Override
    public double[] optimize(int suggestedIterations) {
        for (int currentDim1 = 0; currentDim1 < qualityFunction.getDimension(); ++currentDim1) {
            double spreadDim1 = (qualityFunction.getBounds().getUpper()[currentDim1] - qualityFunction.getBounds().getLower()[currentDim1])
                    / (density);
            for (int currentDim2 = currentDim1 + 1; currentDim2 < qualityFunction.getDimension(); ++currentDim2) {
                double spreadDim2 = (qualityFunction.getBounds().getUpper()[currentDim2] - qualityFunction.getBounds().getLower()[currentDim2])
                        / (density);
                System.arraycopy(solution, 0, currentPoint, 0, solution.length);
                for (int stepDim1 = 0; stepDim1 <= density; ++stepDim1) {
                    currentPoint[currentDim1] = qualityFunction.getBounds().getLower()[currentDim1] + stepDim1 * spreadDim1;
                    for (int stepDim2 = 0; stepDim2 <= density; ++stepDim2) {
                        currentPoint[currentDim2] = qualityFunction.getBounds().getLower()[currentDim2] + stepDim2 * spreadDim2;
                        double value = qualityFunction.getValue(currentPoint);
                        Loggers.SAMPLES_LOGGER.registerSample(new SingleSample(currentPoint,value),new GridSearchPseudoBehaviour());
                        if (value < bestValue) {
                            updateOptimumEstimationWithCandidateSolution(value, currentPoint);
                        }
                    }
                }
            }
        }
        this.breakComputations = true;
        return solution;
    }

    @Override
    public boolean shouldBeRestarted() {
        return false;
    }

    @Override
    public boolean breakComputations() {
        return this.breakComputations;
    }

}

package pl.edu.pw.mini.optimization.core.sampling;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DistancedSample extends Sample {
    private final double distance;
    protected Sample sample;

    public DistancedSample(Sample sample, double[] refPoint) {
        this.sample = sample;
        this.distance = sample.getDistance(refPoint);
    }

    public DistancedSample(Sample sample, double[] refPoint, int dim) {
        this.sample = sample;
        this.distance = sample.getDistanceInDimension(refPoint, dim);
    }

    @Override
    public double[] getX() {
        return sample.getX();
    }

    @Override
    public double getValue() {
        return sample.getValue();
    }

    @Override
    public double getRadius() {
        return sample.getRadius();
    }

    @Override
    public long getWeight() {
        return sample.getWeight();
    }

    @Override
    public List<DistancedSample> getKOrderedSamples(double[] point, int k) {
        return sample.getKOrderedSamples(point, k);
    }

    public double getDistance() {
        return distance;
    }


    /**
     * Returns a merged sorted by distance list out of sorted l1 and sorted l2 with a max length of k
     *
     * @param l1 first ordered list
     * @param l2 second ordered list
     * @param k  maximum list length
     * @return sorted list
     */
    public static List<DistancedSample> mergeOrderedLists(List<DistancedSample> l1, List<DistancedSample> l2, int k) {
        List<DistancedSample> mergedList = new ArrayList<>();
        Iterator<DistancedSample> i1 = l1.iterator();
        Iterator<DistancedSample> i2 = l2.iterator();
        //TODO supposedly this can be done better with spliterators
        if (i1.hasNext() && i2.hasNext()) {
            DistancedSample l1HeadSample = i1.next();
            DistancedSample l2HeadSample = i2.next();
            while (true) {
                if (l1HeadSample.getDistance() < l2HeadSample.getDistance()) {
                    mergedList.add(l1HeadSample);
                    if (mergedList.size() > k - 1)
                        return mergedList;
                    if (i1.hasNext()) {
                        l1HeadSample = i1.next();
                    } else {
                        mergedList.add(l2HeadSample);
                        if (mergedList.size() > k - 1)
                            return mergedList;
                        break;
                    }
                } else {
                    mergedList.add(l2HeadSample);
                    if (mergedList.size() > k - 1)
                        return mergedList;
                    if (i2.hasNext()) {
                        l2HeadSample = i2.next();
                    } else {
                        mergedList.add(l1HeadSample);
                        if (mergedList.size() > k - 1)
                            return mergedList;
                        break;
                    }
                }
            }
        }
        while (i1.hasNext()) {
            mergedList.add(i1.next());
            if (mergedList.size() > k - 1)
                return mergedList;
        }
        while (i2.hasNext()) {
            mergedList.add(i2.next());
            if (mergedList.size() > k - 1)
                return mergedList;
        }
        return mergedList;
    }
}

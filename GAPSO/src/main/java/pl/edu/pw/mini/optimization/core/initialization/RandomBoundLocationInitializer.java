package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.Function;

public class RandomBoundLocationInitializer extends BaseLocationInitializer {
    public RandomBoundLocationInitializer(Function function) {
        super(function);
    }

    @Override
    public double[] getNext() {
        return getRandomWithinFunctionBounds();
    }

    @Override
    public void resetState() {
        //DO NOTHING ON PURPOSE
    }

    @Override
    public boolean isActive() {
        return true;
    }
}

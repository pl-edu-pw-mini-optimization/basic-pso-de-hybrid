package pl.edu.pw.mini.optimization.core.behaviour.pso;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.List;

public abstract class StandardPSO2011Behaviour extends PSOBehaviour {
    public StandardPSO2011Behaviour(double weight, double omega, double c1, double c2) {
        super(weight, omega, c1, c2);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        double[] newV = new double[particle.getV().length];
        List<Particle> particles = particle.getParticles();
        double[] localXBest = getBestLocation(particle, particles);

        double[] globalAttractionResult = new double[particle.getFunction().getDimension()];
        double[] localAttractionResult = new double[particle.getFunction().getDimension()];
        double[] weightCenter = new double[particle.getFunction().getDimension()];
        double[] attractionVector = new double[particle.getFunction().getDimension()];
        double attractionVectorLength = 0.0;
        double radius = 0.0;
        for (int i = 0; i < newV.length; ++i) {
            globalAttractionResult[i] = _c2 * Generator.RANDOM.nextDouble() * (localXBest[i] - particle.getX()[i])
                    + particle.getX()[i];
            localAttractionResult[i] = _c1 * Generator.RANDOM.nextDouble() * (particle.getXBest()[i] - particle.getX()[i])
                    + particle.getX()[i];
            weightCenter[i] = (globalAttractionResult[i] + localAttractionResult[i] + particle.getX()[i]) / 3.0;
            attractionVector[i] = Generator.RANDOM.nextGaussian();
            attractionVectorLength += attractionVector[i] * attractionVector[i];
            radius += (weightCenter[i] - particle.getX()[i]) * (weightCenter[i] - particle.getX()[i]);
        }
        radius = Math.sqrt(radius) * Generator.RANDOM.nextDouble();
        attractionVectorLength = Math.sqrt(attractionVectorLength);
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = _omega * particle.getV()[i] +
                    attractionVector[i] / attractionVectorLength * radius + weightCenter[i] - particle.getX()[i];
                    ;
        }
        return newV;
    }

    protected abstract double[] getBestLocation(Particle particle, List<Particle> particles);
}

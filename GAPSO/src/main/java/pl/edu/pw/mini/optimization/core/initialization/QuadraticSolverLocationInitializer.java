package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;

import java.util.List;

public class QuadraticSolverLocationInitializer extends SampledLocationInitializer {
    QuadraticSolverLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
    }


    protected double[] getParabolaPeakWithRandomFallback() {
        List<Sample> data = sampledFunction.getNearestSamples(sampledFunction.getSamplesCount(),
                new double[dimension]);
        if (data.size() >= 2 * sampledFunction.getDimension() + 1) {
            try {
                SquareModel sm = new SquareModel(dimension, data);
                return sm.getBoundedParabolaPeak(bounds);
            } catch (SingularMatrixException ex) {
                return getRandomWithinFunctionBounds();
            }
        }
        return getRandomWithinFunctionBounds();
    }

    @Override
    public double[] getNext() {
        return getParabolaPeakWithRandomFallback();
    }

    @Override
    public boolean isActive() {
        return sampledFunction.getSamplesCount() >= 2 * dimension + 1;
    }
}

package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.List;

public class QuadraticSingleFireSolverLocationInitializer extends SampledLocationInitializer {
    private int fired;

    QuadraticSingleFireSolverLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
        fired = 0;
    }


    protected double[] getParabolaPeakWithRandomFallback() {
        List<Sample> data = sampledFunction.getNearestSamples(sampledFunction.getSamplesCount(),
                new double[dimension]);
        if (data.size() >= sampledFunction.getMinSquareSampleSize()) {
            try {
                SquareModel sm = new SquareModel(dimension, data);
                double[] peak = sm.getBoundedParabolaPeak(bounds);
                fired =
                        PropertiesAccessor
                                .getGPSOConfigurationInstance()
                                .getSquareInitializerDelay(function.getDimension());
                return peak;
            } catch (SingularMatrixException ex) {
                return getRandomWithinFunctionBounds();
            }
        }
        return getRandomWithinFunctionBounds();
    }

    @Override
    public double[] getNext() {
        return getParabolaPeakWithRandomFallback();
    }

    @Override
    public boolean isActive() {
        return (--fired < 0) && (sampledFunction.getSamplesCount() >= sampledFunction.getMinSquareSampleSize());
    }
} 
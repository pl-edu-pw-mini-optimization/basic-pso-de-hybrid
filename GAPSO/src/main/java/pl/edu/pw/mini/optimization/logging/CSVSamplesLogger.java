package pl.edu.pw.mini.optimization.logging;

import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.NamedObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import static pl.edu.pw.mini.optimization.utils.Sanitizer.sanitizeFilename;

public class CSVSamplesLogger extends BaseCSVLogger implements SamplesLogger {
    HashMap<Integer,List<Pair<Sample, NamedObject>>> samplesBuffer;

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        super.registerRun(configuration, functionName, functionDimension);
        samplesBuffer = new HashMap<>();
        samplesBuffer.put(iteration, new ArrayList<>());
    }

    @Override
    public void registerIteration() {
        super.registerIteration();
        samplesBuffer.put(iteration, new ArrayList<>());
    }

    @Override
    public void registerSample(Sample sample, NamedObject namedObject) {
        samplesBuffer.get(iteration).add(
                new Pair<>(sample,namedObject)
        );
    }

    @Override
    protected void logEntry() {
        if (this.active) {
            for (Integer key : samplesBuffer.keySet()) {
                for (Pair<Sample,NamedObject> sample : samplesBuffer.get(key)) {
                    lastLog = new Date();
                    StringBuilder sb = new StringBuilder();
                    sb.append(functionName);
                    sb.append(',');
                    sb.append(String.format("%3d",run));
                    sb.append(',');
                    sb.append(String.format("%3d", key));
                    sb.append(',');
                    for (int d = 0; d < sample.getKey().getX().length; ++d) {
                        sb.append(String.format(Locale.ENGLISH, "%11.8f", sample.getKey().getX()[d]));
                        sb.append(',');
                    }
                    sb.append(String.format(Locale.ENGLISH, "%16.8f", sample.getKey().getValue()));
                    sb.append(',');
                    sb.append(sample.getSecond().toString());
                    sb.append(System.lineSeparator());
                    logLineToLogFile(sb);
                }
                samplesBuffer.get(key).clear();
            }
            samplesBuffer.clear();
        }

    }

    @Override
    protected String logType() {
        return "samples";
    }

    @Override
    protected void generateHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("Function");
        sb.append(',');
        sb.append("Run");
        sb.append(',');
        sb.append("Iteration");
        sb.append(',');
        for (int dim = 0; dim < this.dimension; ++dim) {
            sb.append("X");
            sb.append(dim+1);
            sb.append(',');
        }
        sb.append("Y");
        sb.append(',');
        sb.append("Behavior");
        sb.append(System.lineSeparator());
        logLineToLogFile(sb);

    }
}

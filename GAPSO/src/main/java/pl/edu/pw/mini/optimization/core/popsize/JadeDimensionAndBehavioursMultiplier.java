package pl.edu.pw.mini.optimization.core.popsize;

public class JadeDimensionAndBehavioursMultiplier extends  JadeDimensionSizeMultiplier {

    public JadeDimensionAndBehavioursMultiplier() {
        super();
    }

    public JadeDimensionAndBehavioursMultiplier(int multiplier) {
        super(multiplier);
    }

    @Override
    public String toString() {
        return super.toString() + "Bhv";
    }

    @Override
    public int getSize(int dimension, int budgetSizeRank, int behavioursCount) {
        return super.getSize(dimension, budgetSizeRank, behavioursCount) * behavioursCount;
    }
}

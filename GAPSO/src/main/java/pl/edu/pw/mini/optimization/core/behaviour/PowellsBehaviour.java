package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.PowellOptimizer;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.FunctionWrapper;

/**
 * Standard Local PSO behaviour
 */
public class PowellsBehaviour extends Behaviour {

    public PowellsBehaviour(double weight) {
        super(weight);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        FunctionWrapper function = new FunctionWrapper(particle.getFunction());
        PowellOptimizer po = new PowellOptimizer(1e-8, 1);
        PointValuePair pvp = po.optimize(
                new MaxEval(10000),
                new ObjectiveFunction(function),
                GoalType.MINIMIZE,
                new InitialGuess(particle.getXBest())
        );
        double[] newV = new double[particle.getX().length];
        for (int i = 0; i < newV.length; ++i) {
            newV[i] -= particle.getX()[i];
            newV[i] += pvp.getPoint()[i];
        }
        return newV;
    }
}

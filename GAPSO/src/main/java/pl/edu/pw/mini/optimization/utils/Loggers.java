package pl.edu.pw.mini.optimization.utils;

import pl.edu.pw.mini.optimization.logging.*;

import java.util.ArrayList;
import java.util.List;

public class Loggers {
    public static final SwarmStateLogger SWARM_LOGGER =
            "CSV".equals(PropertiesAccessor.getInstance().getSwarmStateLoggerType()) ? new CSVSwarmStateLogger() : new NullSwarmStateLogger();
    public static final SamplesLogger SAMPLES_LOGGER =
            "CSV".equals(PropertiesAccessor.getInstance().getSamplesLoggerType()) ? new CSVSamplesLogger() : new NullSamplesLogger();
    public static final NextMoveSamplesLogger MOVES_LOGGER =
            "CSV".equals(PropertiesAccessor.getInstance().getMoveLoggerType()) ? new CSVNextMoveSamplesLogger() : new NullNextMoveSamplesLogger();
    public static final SubRunsLogger SUBRUNS_LOGGER =
            "CSV".equals(PropertiesAccessor.getInstance().getSubrunsEffectsLoggerType()) ? new CSVSubRunsEffectsCSVLogger() : new NullSubRunsEffectsLogger();
    public static final RegisteredOptimaLogger OPTIMA_LOGGER =
            "CSV".equals(PropertiesAccessor.getInstance().getOptimaLoggerType()) ? new CSVRegisteredOptimaLogger() : new NullRegisteredOptimaLogger();
    private static final List<BaseLogger> loggers;

    static {
        loggers = new ArrayList<>();
        loggers.add(SWARM_LOGGER);
        loggers.add(SAMPLES_LOGGER);
        loggers.add(MOVES_LOGGER);
        loggers.add(OPTIMA_LOGGER);
    }


    public static void registerRuns(String configuration, String functionName, int functionDimension) {
        loggers.forEach(logger -> logger.registerRun(configuration, functionName, functionDimension));
    }

    public static void complete() {
        loggers.forEach(logger -> logger.finalize());
    }

    public static void registerIteration() {
        loggers.forEach(logger -> logger.registerIteration());
    }
}

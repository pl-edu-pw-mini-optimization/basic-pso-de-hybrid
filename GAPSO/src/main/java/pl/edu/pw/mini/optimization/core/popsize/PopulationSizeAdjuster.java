package pl.edu.pw.mini.optimization.core.popsize;

public abstract class PopulationSizeAdjuster {
    abstract public String toString();
    abstract public int getSize(int dimension, int budgetSizeRank, int behavioursCount);
}

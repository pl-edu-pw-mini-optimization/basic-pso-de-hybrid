package pl.edu.pw.mini.optimization.core.functions;

public class RosenbrockFunction extends Function {

    private final int _dimension;
    private final Bounds _bounds;

    public RosenbrockFunction(int dimension) {
        this._dimension = dimension;
        this._bounds = new Bounds(-5.0, 5.0, dimension);
    }

    @Override
    public double getValue(double[] x) {
        double result = 0.0;
        for (int i = 0; i < x.length - 1; ++i) {
            result += (1 - x[i]) * (1 - x[i]);
            result += 100 * (x[i + 1] - x[i] * x[i]) * (x[i + 1] - x[i] * x[i]);
        }
        return result;
    }

    @Override
    public Bounds getBounds() {
        return _bounds;
    }

    @Override
    public int getDimension() {
        return _dimension;
    }

    @Override
    public String getName() {
        return "Rosenbrock";
    }
}

package pl.edu.pw.mini.optimization.utils;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import pl.edu.pw.mini.optimization.core.GPSOConfigurator;
import pl.edu.pw.mini.optimization.core.PropertiesGPSOConfigurator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Properties;

public class PropertiesAccessor {
    private static PropertiesAccessor propertiesAccessor;
    private static PropertiesGPSOConfigurator gpsoConfigurator;
    private String subrunsEffectsLoggerType;
    private String samplesLoggerType;
    private String moveLoggerType;
    private String swarmStateLoggerType;
    private String version;
    private String artifactId;
    private String optimaLoggerType;

    private PropertiesAccessor() {
        final Properties gitProperties = new Properties();
        try {
            gitProperties.load(this.getClass().getClassLoader().getResourceAsStream("git.properties"));
        } catch (IOException e1) {
            handleGitPropertiesException(gitProperties);
        } catch (NullPointerException e2) {
            handleGitPropertiesException(gitProperties);
        }
        final Properties properties = new Properties();
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
            version = properties.getProperty("application.version") + "-" +
                    gitProperties.getProperty("git.commit.id.abbrev") + "-" +
                    gitProperties.getProperty("git.dirty");
            artifactId = properties.getProperty("application.artifactId");
            samplesLoggerType = getPropetyWithFallBack(properties, "logger.samples");
            moveLoggerType = getPropetyWithFallBack(properties, "logger.next.move.samples");
            swarmStateLoggerType = getPropetyWithFallBack(properties, "logger.swarm");
            subrunsEffectsLoggerType = getPropetyWithFallBack(properties, "logger.subrun");;
            optimaLoggerType = getPropetyWithFallBack(properties, "logger.optima");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getPropetyWithFallBack(Properties defaultApplicationProperties, String propertyName) {
        try {
            InputStream inputStream = Files.newInputStream(Paths.get("logger.properties"), StandardOpenOption.READ);
            Properties customLoggerProperties = new Properties();
            customLoggerProperties.load(inputStream);
            inputStream.close();
            return customLoggerProperties.getProperty(propertyName);
        } catch (IOException ex1) {
            try {
                Properties defaultLoggerProperties = new Properties();
                defaultLoggerProperties.load(this.getClass().getClassLoader().getResourceAsStream("logger.default.properties"));
                return defaultLoggerProperties.getProperty(propertyName);
            } catch (IOException ex2) {
                return defaultApplicationProperties.getProperty(propertyName);
            }
        }
    }

    private void handleGitPropertiesException(Properties gitProperties) {
        System.err.println("[WARN] No 'git.properties' file: please compile project with 'mvn compile' before running");
        gitProperties.setProperty("git.commit.id.abbrev", "unknown");
        gitProperties.setProperty("git.dirty", "unknown");
    }

    public static PropertiesAccessor getInstance() {
        if (propertiesAccessor == null) {
            propertiesAccessor = new PropertiesAccessor();
        }
        return propertiesAccessor;
    }

    public static GPSOConfigurator getGPSOConfigurationInstance() {
        if (gpsoConfigurator == null) {
            gpsoConfigurator = new PropertiesGPSOConfigurator();
        }
        return gpsoConfigurator;
    }

    public String getVersion() {
        return version;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getSwarmStateLoggerType() {
        return swarmStateLoggerType;
    }

    public String getSamplesLoggerType() {
        return samplesLoggerType;
    }

    public String getSubrunsEffectsLoggerType() {
        return subrunsEffectsLoggerType;
    }

    public String getOptimaLoggerType() { return optimaLoggerType;
    }

    public String getMoveLoggerType() {
        return moveLoggerType;
    }
}

package pl.edu.pw.mini.optimization.logging;

public interface SwarmStateLogger extends BaseLogger {
    void registerStagnation(int stagnationPeriod);
    void registerVariation(double swarmDiameter);
    void registerSamplesCount(int samplesCount);
    void registerRestart();
}

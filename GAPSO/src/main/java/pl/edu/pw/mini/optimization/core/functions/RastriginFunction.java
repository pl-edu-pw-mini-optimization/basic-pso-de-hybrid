package pl.edu.pw.mini.optimization.core.functions;

public class RastriginFunction extends Function {

    private final int _dimension;
    private final Bounds _bounds;

    public RastriginFunction(int dimension) {
        this._dimension = dimension;
        this._bounds = new Bounds(-5.0, 5.0, dimension);
    }

    @Override
    public double getValue(double[] x) {
        double result = 0.0;
        for (double x1 : x) {
            result += x1 * x1;
            result -= Math.cos(2 * Math.PI * x1);
            result += 1;
        }
        return result;
    }

    @Override
    public Bounds getBounds() {
        return _bounds;
    }

    @Override
    public int getDimension() {
        return _dimension;
    }

    @Override
    public String getName() {
        return "Rastrigin";
    }
}

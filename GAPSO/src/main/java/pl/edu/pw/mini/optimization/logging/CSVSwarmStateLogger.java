package pl.edu.pw.mini.optimization.logging;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static pl.edu.pw.mini.optimization.utils.Sanitizer.sanitizeFilename;

public class CSVSwarmStateLogger extends BaseCSVLogger implements SwarmStateLogger {
    private int samplesCount;
    private int currentStagnationPeriod;
    private double currentSwarmLocationVariation;

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        super.registerRun(configuration, functionName, functionDimension);
        this.samplesCount = 0;
        this.currentStagnationPeriod = 0;
        this.currentSwarmLocationVariation = 0.0;
    }

    @Override
    public void registerStagnation(int stagnationPeriod) {
        this.currentStagnationPeriod = stagnationPeriod;
    }

    @Override
    public void registerVariation(double swarmVariation) {
        this.currentSwarmLocationVariation = swarmVariation;
    }

    @Override
    public void registerSamplesCount(int samplesCount) {
        this.samplesCount = samplesCount;
    }

    @Override
    public void registerRestart() {
            logEntry();
            this.active = false;
    }

    @Override
    protected void logEntry() {
        if (this.active) {
            lastLog = new Date();
            StringBuilder sb = new StringBuilder();
            sb.append(functionName);
            sb.append(',');
            sb.append(iteration);
            sb.append(',');
            sb.append(samplesCount);
            sb.append(',');
            sb.append(currentStagnationPeriod);
            sb.append(',');
            sb.append(currentSwarmLocationVariation);
            sb.append(System.lineSeparator());
            try {
                Files.write(Paths.get(configuration + '-' + experimentStart + ".csv"), sb.toString().getBytes(),
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                //
                System.err.println("Unable to log to file");
                System.err.println(sb.toString());
            }
        }
    }

    @Override
    protected String logType() {
        return "swarm-state";
    }

    @Override
    protected void generateHeader() {

    }
}

package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.PolynomialModel;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.List;

//TODO: algorithm should dive in to get values from multiple smaller samples
//The idea:
//Traverse the whole tree getting Clusters with more than K samples inside,
//but less then CAPACITY?
public class PolynomialModelBehaviour extends RandomSearchBehaviour {

    private final int degree;

    public PolynomialModelBehaviour(double weight) {
        super(weight);
        this.degree = 4;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        int dims = particle.getFunction().getDimension();
        double[] newVelocity = new double[dims];
        // For each dimension get nearest samples
        for(int d = 0; d < dims; d++) {
            List<Sample> data = particle.getFunction().getNearestInDimensionSamples((this.degree * dims + 1), d, particle.getX());

            // Test for nearest sample quality
            for(int d2 = 0; d2 < dims; d2++) {
                double sd = 0;
                double average = particle.getX()[d2];
                for (int i = 0; i < data.size(); i++) {
                    //instances[j] = data.get(j).getX()[i];
                    sd += (data.get(i).getX()[d2] - average) * (data.get(i).getX()[d2] - average);
                }
                sd = Math.sqrt(sd/data.size());
            }
            newVelocity[d] = getVelocityInDimensionToBoundedModelMinimum(particle, d, data);
        }

        return newVelocity;
    }

    protected double getVelocityInDimensionToBoundedModelMinimum(Particle particle, int dim, List<Sample> data) {
        //System.out.println(data.size());
        int dims = particle.getFunction().getDimension();
        if (data.size() < this.degree * dims + 1) {
            return super.computeVelocity(particle)[dim];
        } else {
            for (Sample dataSample : data) {
                Loggers.MOVES_LOGGER.registerBaseSample(dataSample.getX(), "DataPoint-" + dim);
            }
            Bounds functionBounds = particle.getFunction().getBounds();
            PolynomialModel pm = new PolynomialModel(particle, dim, this.degree, data);

            double newVelocityInDimension = 0;
            try {
                newVelocityInDimension = pm.getBoundedPolynomianMinimum(functionBounds);
                //newVelocity = sm.getBoundedParabolaPeak(functionBounds);
            } catch (SingularMatrixException ex) {
                return super.computeVelocity(particle)[dim];
            }
            newVelocityInDimension -= particle.getX()[dim];
            return newVelocityInDimension;
        }
    }
}

package pl.edu.pw.mini.optimization.core;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.BaseLocationInitializer;
import pl.edu.pw.mini.optimization.core.restart.*;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.utils.Loggers;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeneralizedParticleSwarm extends BaseOptimizer {
    private final ArrayList<Particle> particles;
    private final SwarmRestartManager restartManager;
    private boolean reloaded;
    private List<Behaviour> initialSelectedBehaviours;
    private List<Behaviour> behaviours;

    /**
     * Constructor of the generalized swarm
     *  @param function       function to be optimized
     * @param populationSize swarm/population size
     */
    public GeneralizedParticleSwarm(SamplesGatherer function, int populationSize, SwarmRestartManager restartManager,
                                    BaseLocationInitializer locationInitializer, List<Behaviour> behaviours) {
        super(function);
        particles = new ArrayList<Particle>();
        this.restartManager = restartManager;
        this.reloaded = false;
        this.behaviours = behaviours;

        for (int i = 0; i < populationSize; ++i) {
            Particle particle = new Particle(function, particles, locationInitializer, solution);
            this.restartManager.registerParticle(particle);
            if (particle.getYBest() < bestValue) {
                bestValue = particle.getYBest();
                for (int dim = 0; dim < solution.length; ++dim) {
                    solution[dim] = particle.getXBest()[dim];
                }
            }
        }
        for (int i = 0; i < populationSize; ++i) {
            particles.get(i).initializeVelocity(particles);
        }
    }

    @Override
    public double[] optimize(int suggestedIterations) {
        for (int i = 0; i < suggestedIterations; ++i) {
            restartManager.registerNewIteration();
            Loggers.registerIteration();
            int idx = 0;
            List<Behaviour> selectedBehaviours = selectBehavioursForParticles(behaviours);
            if (!qualityFunction.isAveraging() && !reloaded) {
                reloaded = true;
                for (Particle particle : particles) {
                    particle.recomputeBest();
                }
            }
            for (Particle particle : particles) {
                behaviours.forEach(b -> b.registerValue(particle.getYBest()));
                //this is the crucial part - behaviours are mixed between iterations
                //we can do adaptation on the behaviours vector
                //testing selection before each iteration instead of before batch
                particle.sampleNext(selectedBehaviours.get(idx++));
                if (particle.getYBest() < bestValue) {
                    updateOptimumEstimationWithCandidateSolution(particle.getYBest(), particle.getXBest());
                    restartManager.registerImprovement();
                }
            }
            Loggers.SWARM_LOGGER.registerSamplesCount(qualityFunction.getSamplesCount());
        }
        return solution;
    }

    @Override
    public boolean shouldBeRestarted() {
        return restartManager.shouldBeRestarted();
    }

    @Override
    public boolean breakComputations() {
        return false;
    }

    private List<Behaviour> selectBehavioursForParticles(List<Behaviour> behaviours) {
        behaviours
                .stream()
                .forEach(item -> item.registerIteration(
                        PropertiesAccessor.getGPSOConfigurationInstance().getAdaptationHistoryLength()));
        List<Pair<Behaviour,Double>> bhvList = behaviours
                .stream()
                .map(item -> new Pair<Behaviour,Double>(item,item.getWeight()))
                .collect(Collectors.toList());

        EnumeratedDistribution<Behaviour> behavioursDistribution = null;
        try {
            behavioursDistribution = new EnumeratedDistribution<>(Generator.RANDOM, bhvList);
        } catch (org.apache.commons.math3.exception.MathArithmeticException ex) {
            bhvList = behaviours
                    .stream()
                    .map(item -> new Pair<Behaviour,Double>(item,1.0))
                    .collect(Collectors.toList());
            behavioursDistribution = new EnumeratedDistribution<>(Generator.RANDOM, bhvList);
        }
        Behaviour[] polledBehaviours =
                behavioursDistribution
                        .sample(
                                particles.size() - behaviours.size(),
                                new Behaviour[0])
                ;
        List<Behaviour> selectedBehaviours = new ArrayList<Behaviour>();
        for (Behaviour b : polledBehaviours) {
            selectedBehaviours.add(b);
        }
        for (Behaviour b : behaviours) {
            selectedBehaviours.add(b);
        }
        selectedBehaviours = selectedBehaviours
                .stream()
                .sorted((o1, o2) -> Generator.RANDOM.nextInt(2) - 1)
                .collect(Collectors.toList());
        if (PropertiesAccessor.getGPSOConfigurationInstance().isEnableBehaviorsMixing()) {
            return selectedBehaviours;
        } else if (initialSelectedBehaviours == null) {
            initialSelectedBehaviours = selectedBehaviours;
        }
        return initialSelectedBehaviours;
    }

}

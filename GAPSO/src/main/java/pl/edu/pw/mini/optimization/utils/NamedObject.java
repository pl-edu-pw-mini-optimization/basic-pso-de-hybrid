package pl.edu.pw.mini.optimization.utils;

public interface NamedObject {
    public String toString();
}

package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.bounds.Region;
import pl.edu.pw.mini.optimization.core.initialization.knowledge.OptimumInfo;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.lm.LinearModel;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.*;
import java.util.stream.DoubleStream;

import static pl.edu.pw.mini.optimization.utils.PropertiesAccessor.getGPSOConfigurationInstance;

public class NonoverlappingRegionsExploringLocationInitializer extends ExploringNewRegionsLocationInitalizer {
    private EnumeratedDistribution<Integer> choicesDistrbution;
    private final Map<Integer,Double> pollingResults;
    private int lastChoice;
    private final List<Pair<Integer,Double>> choicesList;

    public NonoverlappingRegionsExploringLocationInitializer(SamplesGatherer function) {
        super(function);


        choicesList = new ArrayList<>();
        lastChoice = -1;

        double randomPointExploration = getGPSOConfigurationInstance().getExplorationStrategyWeightRandomPointExploration();
        double bestPointExploration = getGPSOConfigurationInstance().getExplorationStrategyWeightBestPointExploration();
        double bestPointExploatation = getGPSOConfigurationInstance().getExplorationStrategyWeightBestPointExploitation();
        double differencePointExploration = getGPSOConfigurationInstance().getExplorationStrategyWeightDifferencePointExploration();
        double completeReset = getGPSOConfigurationInstance().getExplorationStrategyWeightCompleteBoundsReset();
        double rouletteExploration =  getGPSOConfigurationInstance().getExplorationStrategyWeightRoulette();
        double valueBasedHeuristic = getGPSOConfigurationInstance().getExplorationStrategyWeightValueBasedHeuristic();
        double linearGridHeuristic = getGPSOConfigurationInstance().getExplorationStrategyWeightModelBasedHeuristic();
        double eaSwapping = getGPSOConfigurationInstance().getExplorationStrategyWeightEASwapping();
        if (randomPointExploration >= 0)
            choicesList.add(new Pair<>(0,randomPointExploration));
        if (bestPointExploration >= 0)
            choicesList.add(new Pair<>(1,bestPointExploration));
        if (bestPointExploatation >= 0)
            choicesList.add(new Pair<>(2,bestPointExploatation));
        if (differencePointExploration >= 0)
            choicesList.add(new Pair<>(3,differencePointExploration));
        if (completeReset >= 0)
            choicesList.add(new Pair<>(4,completeReset));
        if (rouletteExploration >= 0)
            choicesList.add(new Pair<>(5,rouletteExploration));
        if (valueBasedHeuristic >= 0)
            choicesList.add(new Pair<>(6,valueBasedHeuristic));
        if (linearGridHeuristic >= 0)
            choicesList.add(new Pair<>(7,linearGridHeuristic));
        if (eaSwapping >= 0)
            choicesList.add(new Pair<>(8,eaSwapping));

        if (choicesList.size() <= 0) {
            throw new IllegalArgumentException("No re-initializer strategies available");
        }

        choicesDistrbution = new EnumeratedDistribution<>(Generator.RANDOM, choicesList);

        pollingResults = new TreeMap<>();
        for (int i = 0; i < choicesList.size(); ++i) {
            pollingResults.put(choicesList.get(i).getKey(), 0.0);
        }

    }

    @Override
    public void resetState() {
        if (exploitingInitializer != null) {
            if (exploitingInitializer.isActive()) {
                exploitingInitializer.resetState();
            } else {
                //TODO: rebuild this code to properly utilize optimum info
                OptimumInfo optimumInfo = processLastOptimumEstimation();
                updateInitializationStrategiesStatistics(optimumInfo);
                createNewInitializer();
                Region newBounds = null;

                int choice = getInitializationStrategyChoice();

                int bestOptimumIdx = getBestOptimumIdx();

                if (choice == 0) { //random point neighbourhood
                    newBounds = regionsForExploration
                            .get(
                                    Generator.RANDOM.nextInt(regionsForExploration.size()));
                } else if (choice == 1) { //best point neighbourhood
                    List<Region> possibleRegions = selectPossibleRegions(bestOptimumIdx);
                    newBounds = possibleRegions.get(Generator.RANDOM.nextInt(possibleRegions.size()));
                } else if (choice == 2) { //best point exploitation
                    Sample bestOptimumEstimation = previousOptimumEstimations.get(bestOptimumIdx);
                    newBounds = new Region(
                            computeNewBoundsCloseToOptimum(
                                    bestOptimumEstimation,
                                    maxBestNeighbourhood));
                } else if (choice == 3) { // DE vector exploration
                    if (previousOptimumEstimations.size() > 1) {
                        newBounds = getDERegion();
                        if (newBounds == null) {
                            newBounds = regionsForExploration.get(Generator.RANDOM.nextInt(regionsForExploration.size()));
                        }
                    } else {
                        newBounds = regionsForExploration.get(Generator.RANDOM.nextInt(regionsForExploration.size()));
                    }

                } else if (choice == 4) { // total reset of bounds
                    newBounds = new Region(function.getBounds().toArray());
                } else if (choice == 5) { // roulette rule region selection
                    EnumeratedDistribution<Integer> optimaRoulette = getOptimaRoulette();
                    int selectedOptimum = optimaRoulette.sample();
                    List<Region> possibleRegions = selectPossibleRegions(selectedOptimum);
                    newBounds = possibleRegions.get(Generator.RANDOM.nextInt(possibleRegions.size()));
                } else if (choice == 6) { // value based heuristic
                    if (previousOptimumEstimations.size() <= 1) {
                        EnumeratedDistribution<Integer> optimaRoulette = getOptimaRoulette();
                        int selectedOptimum = optimaRoulette.sample();
                        List<Region> possibleRegions = selectPossibleRegions(selectedOptimum);
                        newBounds = possibleRegions.get(Generator.RANDOM.nextInt(possibleRegions.size()));
                    } else {
                        TwoDistinctIds twoDistinctIds = new TwoDistinctIds().invoke();
                        int diffVectorIdx1 = twoDistinctIds.getDiffVectorIdx1();
                        int diffVectorIdx2 = twoDistinctIds.getDiffVectorIdx2();
                        Sample optimum1 = previousOptimumEstimations.get(diffVectorIdx1);
                        Sample optimum2 = previousOptimumEstimations.get(diffVectorIdx2);
                        if (Math.abs(optimum1.getValue() - optimum2.getValue()) < FUNCTION_VALUE_TOLERANCE) {
                            newBounds = getBetweenOptimaRegion(optimum1, optimum2);
                        } else {
                            newBounds = getAnotherPossibleOptimumBounds(optimum1, optimum2);
                        }
                    }
                } else if (choice == 7) { // linear grid based heuristic
                    if (previousOptimumEstimations.size() < 4) {
                        newBounds = regionsForExploration
                                .get(Generator.RANDOM.nextInt(regionsForExploration.size()));
                    } else if (previousOptimumEstimations.size() < 2 * function.getDimension()) {
                        int samplesCount = previousOptimumEstimations.size() / 2;
                        List<Integer> optimaIdxs = getKIndependentIdxFromRoulette(samplesCount, false);
                        List<Sample> data = getSamples(optimaIdxs);
                        double[][] modelSearchBounds = getHypotheticalOptimaSpread(data);
                        BoundaryVertexLocationInitializer init = new BoundaryVertexLocationInitializer(function);
                        init.shrinkBoundsWithinBounds(modelSearchBounds);
                        double[] xSample = init.getBoundaryVertex();
                        newBounds = genearateRegionFromMidpointAndUnorganizedBounds(xSample,
                                modelSearchBounds[0], modelSearchBounds[1], function.getDimension());
                    } else if (previousOptimumEstimations.size() < 3 * function.getDimension()) {
                        int samplesCount = function.getDimension() + 1;
                        List<Integer> optimaIdxs = getKIndependentIdxFromRoulette(samplesCount, false);
                        List<Sample> data = getSamples(optimaIdxs);
                        double[][] modelSearchBounds = getHypotheticalOptimaSpread(data);
                        newBounds = getRegionFromLinearModel(data, new Bounds(modelSearchBounds));
                    } else {
                        int samplesCount = 2 * function.getDimension() + 1;
                        List<Integer> optimaIdxs = getKIndependentIdxFromRoulette(samplesCount, false);
                        List<Sample> data = getSamples(optimaIdxs);
                        double[][] modelSearchBounds = getHypotheticalOptimaSpread(data);
                        newBounds = getRegionFromSquareModel(data, new Bounds(modelSearchBounds));
                    }
                } else if (choice == 8) { // EA swapping crossover heuristic
                    if (previousOptimumEstimations.size() < 2) {
                        newBounds = regionsForExploration
                                .get(Generator.RANDOM.nextInt(regionsForExploration.size()));
                    } else {
                        for (int samplesCount = 2; samplesCount <= Math.min(
                                function.getDimension(),
                                previousOptimumEstimations.size()); ++samplesCount) {
                            List<Integer> optimaIdxs = getKIndependentIdxFromRoulette(samplesCount, true);
                            List<Sample> data = getSamples(optimaIdxs);
                            double[][] modelSearchBounds = getHypotheticalOptimaSpread(previousOptimumEstimations);
                            newBounds = null;
                            int leftTries = 2 * samplesCount * samplesCount;
                            while (newBounds == null && leftTries-- > 0) {
                                double[] xSample = new double[function.getDimension()];
                                for (int dim = 0; dim < function.getDimension(); ++dim) {
                                    xSample[dim] = data.get(Generator.RANDOM.nextInt(data.size())).getX()[dim];
                                }
                                newBounds = genearateRegionFromMidpointAndUnorganizedBounds(xSample,
                                        modelSearchBounds[0], modelSearchBounds[1], function.getDimension());
                                final Region testBounds = newBounds;
                                if (previousOptimumEstimations.stream().anyMatch(optim -> testBounds.isPointInside(optim.getX()))) {
                                    newBounds = null;
                                }
                            }
                            if (newBounds != null)
                                break;
                        }
                        if (newBounds == null) {
                            newBounds = regionsForExploration
                                    .get(Generator.RANDOM.nextInt(regionsForExploration.size()));
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Wrong initializer choice: " + choice);
                }
                exploitingInitializer.resetOriginalFunctionBounds(function);
                exploitingInitializer.shrinkBoundsWithinBounds(newBounds.getCurrentAreaOfInterest());
                newBounds.selectToExplore();
                exploitingInitializer.computeDirectionParameters();
                exploitingInitializer.resetState();
            }
        }
        Loggers.OPTIMA_LOGGER.registerInitializationMethod(lastChoice);
    }

    private Region getRegionFromSquareModel(List<Sample> data, Bounds modelSearchBounds) {
        Region newBounds;
        try {
            SquareModel sm = new SquareModel(function.getDimension(), data);
            double[] newX = sm.getBoundedParabolaPeak(modelSearchBounds);
            Sample originPoint = new SingleSample(newX, Double.POSITIVE_INFINITY);
            newBounds = new Region(
                    computeNewBoundsCloseToOptimum(
                            originPoint,
                            maxBestNeighbourhood));
        } catch (SingularMatrixException ex) {
            newBounds = getRegionFromLinearModel(data, modelSearchBounds);
        } catch (MathIllegalArgumentException ex) {
            newBounds = getRegionFromLinearModel(data, modelSearchBounds);
        }
        return newBounds;
    }

    private List<Integer> getKIndependentIdxFromRoulette(int samplesCount, boolean withElite) {
        EnumeratedDistribution<Integer> optimaRoulette = getOptimaRoulette();
        List<Integer> optimaIdxs = new ArrayList<>();
        if (previousOptimumEstimations.size() == samplesCount) {
            for (int i = 0; i < samplesCount; ++i) {
                optimaIdxs.add(i);
            }
        } else {
            if (withElite) {
                int bestIdx = getBestOptimumIdx();
                if (!optimaIdxs.contains(bestIdx)) {
                    optimaIdxs.add(bestIdx);
                }
            }
            while (optimaIdxs.size() < samplesCount) {
                int newSampleIdx = optimaRoulette.sample();
                if (!optimaIdxs.contains(newSampleIdx)) {
                    optimaIdxs.add(newSampleIdx);
                }
            }
        }
        return optimaIdxs;
    }

    private Region getRegionFromLinearModel(List<Sample> data, Bounds modelSearchBounds) {
        Region newBounds;
        try {
            LinearModel lm = new LinearModel(function.getDimension(), data);
            double[] newX = lm.getBestBound(modelSearchBounds);
            newBounds = genearateRegionFromMidpointAndUnorganizedBounds(newX,
                    modelSearchBounds.getLower(), modelSearchBounds.getUpper(), function.getDimension());
        } catch (SingularMatrixException ex2) {
            newBounds = regionsForExploration
                    .get(Generator.RANDOM.nextInt(regionsForExploration.size()));
        }
        return newBounds;
    }

    private double[][] getSamplesBoundingBox(List<Sample> data) {
        double[][] modelSearchBounds = new double[2][];
        modelSearchBounds[0] = new double[function.getDimension()];
        modelSearchBounds[1] = new double[function.getDimension()];
        for (int dim = 0; dim < function.getDimension(); ++dim) {
            modelSearchBounds[0][dim] = Double.POSITIVE_INFINITY;
            modelSearchBounds[1][dim] = Double.NEGATIVE_INFINITY;
            for (Sample sample : data) {
                modelSearchBounds[0][dim] = Math.min(modelSearchBounds[0][dim], sample.getX()[dim]);
                modelSearchBounds[1][dim] = Math.max(modelSearchBounds[1][dim], sample.getX()[dim]);
            }
        }
        return modelSearchBounds;
    }

    private double[][] getHypotheticalOptimaSpread(List<Sample> data) {
        //TODO: this should not be maximum spread
        //this should be minimum spread - "independent" directions
        //minimized - take DIM smallest nonlinear distances
        Sample bestSample = data.get(0);
        for (Sample sample : data) {
            if (sample.getValue() < bestSample.getValue()) {
                bestSample = sample;
            }
        }
        double[][] modelSearchBounds = new double[2][];
        modelSearchBounds[0] = new double[function.getDimension()];
        modelSearchBounds[1] = new double[function.getDimension()];
        for (int dim = 0; dim < bestSample.getX().length; ++dim) {
            double bestDistance = Double.POSITIVE_INFINITY;
            Sample betterSample = null;
            Sample worseSample = null;
            for (Sample sample1 : data) {
                for (Sample sample2 : data) {
                    double testDistance = Math.abs(sample1.getX()[dim] - sample2.getX()[dim]);
                    if (testDistance < bestDistance && testDistance > OPTIMUM_DISTANCE_TOLERANCE) {
                        bestDistance = testDistance;
                        if (sample1.getValue() < sample2.getValue()) {
                            betterSample = sample1;
                            worseSample = sample2;
                        } else {
                            betterSample = sample2;
                            worseSample = sample1;
                        }
                    }
                }
            }
            modelSearchBounds[0][dim] = bestSample.getX()[dim];
            modelSearchBounds[1][dim] = bestSample.getX()[dim];
            if (Double.isInfinite(bestDistance)) {
                modelSearchBounds[0][dim] -= OPTIMUM_DISTANCE_TOLERANCE;
                modelSearchBounds[1][dim] += OPTIMUM_DISTANCE_TOLERANCE;
            } else {
                if (bestSample.getX()[dim] > worseSample.getX()[dim] && worseSample.getX()[dim] > betterSample.getX()[dim]) { //a ridge
                    modelSearchBounds[1][dim] += bestDistance;
                } else if (bestSample.getX()[dim] < worseSample.getX()[dim] && worseSample.getX()[dim] < betterSample.getX()[dim]) { //a ridge
                    modelSearchBounds[0][dim] -= bestDistance;
                } else if (bestSample.getX()[dim] < worseSample.getX()[dim] && worseSample.getX()[dim] > betterSample.getX()[dim]) { //a gradient
                    modelSearchBounds[0][dim] -= bestDistance;
                } else if (bestSample.getX()[dim] > worseSample.getX()[dim] && worseSample.getX()[dim] < betterSample.getX()[dim]) { //a gradient
                    modelSearchBounds[1][dim] += bestDistance;
                } else { //I am not sure what to do
                    if (Generator.RANDOM.nextBoolean()) {
                        modelSearchBounds[0][dim] -= bestDistance;
                    } else {
                        modelSearchBounds[1][dim] += bestDistance;
                    }
                }

            }
        }
        return modelSearchBounds;
    }

    private List<Sample> getSamples(List<Integer> optimaIdxs) {
        List<Sample> data = new ArrayList<>();
        for (Integer idx : optimaIdxs) {
            data.add(previousOptimumEstimations.get(idx));
        }
        return data;
    }

    private EnumeratedDistribution<Integer> getOptimaRoulette() {
        double maxOptimumValue = previousOptimumEstimations
                .stream()
                .mapToDouble(prevOpt -> prevOpt.getValue() + 1.0)
                .max()
                .getAsDouble();
        List<Pair<Integer, Double>> optimaDistro = new ArrayList<>();
        for (int i = 0; i < previousOptimumEstimations.size(); ++i) {
            optimaDistro.add(new Pair<>(i, maxOptimumValue - previousOptimumEstimations.get(i).getValue()));
        }
        return new EnumeratedDistribution<>(optimaDistro);
    }

    private Region getAnotherPossibleOptimumBounds(Sample optimum1, Sample optimum2) {
        Region newBounds;
        double[] sensiblePoint = new double[optimum1.getX().length];
        Sample worseOptimum = null;
        Sample betterOptium = null;
        if (optimum1.getValue() < optimum2.getValue()) {
            betterOptium = optimum1;
            worseOptimum = optimum2;
        } else {
            betterOptium = optimum2;
            worseOptimum = optimum1;
        }
        for (int dim = 0; dim < betterOptium.getX().length; ++dim) {
            sensiblePoint[dim] = 2 * betterOptium.getX()[dim] - worseOptimum.getX()[dim];
        }
        double[] firstBoundsVector = betterOptium.getX();
        double[] secondBoundsVector = worseOptimum.getX();
        int dimension = betterOptium.getX().length;
        newBounds = genearateRegionFromMidpointAndUnorganizedBounds(sensiblePoint, firstBoundsVector, secondBoundsVector, dimension);
        return newBounds;
    }

    private Region genearateRegionFromMidpointAndUnorganizedBounds(double[] midPoint, double[] firstBoundsVector, double[] secondBoundsVector, int dimension) {
        Region newBounds;
        double[] distance = new double[dimension];
        double[][] anotherPossibleOptimumBounds = new double[2][];
        anotherPossibleOptimumBounds[0] = new double[dimension];
        anotherPossibleOptimumBounds[1] = new double[dimension];
        for (int dim = 0; dim < dimension; ++dim) {
            distance[dim] = Math.max(OPTIMUM_DISTANCE_TOLERANCE, Math.abs(firstBoundsVector[dim] - secondBoundsVector[dim]));
            anotherPossibleOptimumBounds[0][dim] = midPoint[dim] - distance[dim] / 2;
            anotherPossibleOptimumBounds[1][dim] = midPoint[dim] + distance[dim] / 2;
        }
        newBounds = new Region(anotherPossibleOptimumBounds);
        return newBounds;
    }

    private Region getBetweenOptimaRegion(Sample optimum1, Sample optimum2) {
        double[][] betweenOptimaBounds = new double[2][];
        betweenOptimaBounds[0] = new double[optimum1.getX().length];
        betweenOptimaBounds[1] = new double[optimum1.getX().length];
        for (int dim = 0; dim < optimum1.getX().length; ++dim) {
            betweenOptimaBounds[0][dim] = Math.min(optimum1.getX()[dim],optimum2.getX()[dim]);
            betweenOptimaBounds[1][dim] = Math.max(optimum1.getX()[dim],optimum2.getX()[dim]);
            if (betweenOptimaBounds[1][dim] - betweenOptimaBounds[0][dim] < OPTIMUM_DISTANCE_TOLERANCE) {
                betweenOptimaBounds[0][dim] -= OPTIMUM_DISTANCE_TOLERANCE / 2;
                betweenOptimaBounds[1][dim] += OPTIMUM_DISTANCE_TOLERANCE / 2;
            } else if (betweenOptimaBounds[1][dim] - betweenOptimaBounds[0][dim] > 2 * OPTIMUM_DISTANCE_TOLERANCE) {
                betweenOptimaBounds[0][dim] += OPTIMUM_DISTANCE_TOLERANCE / 2;
                betweenOptimaBounds[1][dim] -= OPTIMUM_DISTANCE_TOLERANCE / 2;
            }
        }
        return new Region(betweenOptimaBounds);
    }

    private int getInitializationStrategyChoice() {
        if (getGPSOConfigurationInstance().adaptInitializationStrategy()) {
            List<Pair<Integer, Double>> updatedChoicesList = new ArrayList<>();
            for (Pair<Integer, Double> choice : choicesList) {
                updatedChoicesList.add(new Pair<Integer, Double>(choice.getKey(),
                        Math.max(1e-16, choice.getValue() + pollingResults.get(choice.getKey()))));
            }
            choicesDistrbution = new EnumeratedDistribution<>(Generator.RANDOM, updatedChoicesList);
        }

        int choice = choicesDistrbution.sample();
        lastChoice = choice;
        return choice;
    }

    private void updateInitializationStrategiesStatistics(OptimumInfo optimumInfo) {
        if (optimumInfo.isNewOptimum()) {
            boolean bestResultImproved = optimumInfo.isBestValueImproved();
            if (bestResultImproved) {
                if (pollingResults.containsKey(lastChoice)) {
                    pollingResults.put(lastChoice, pollingResults.get(lastChoice) + 1.0);
                }
            } else {
                if (pollingResults.containsKey(lastChoice)) {
                    for (int ch = 0; ch < choicesList.size(); ++ch) {
                        int consideredChoice = choicesList.get(ch).getKey();
                        if (consideredChoice != lastChoice) {
                            pollingResults.put(consideredChoice,
                                    pollingResults.get(consideredChoice) +  1.0 / (choicesList.size() - 1));
                        }
                    }
                }
            }
        } else {
            if (pollingResults.containsKey(lastChoice)) {
                pollingResults.put(lastChoice, pollingResults.get(lastChoice) - 2.0);
            }
        }
    }

    private Region getDERegion() {
        double[][] modelSearchBounds = getHypotheticalOptimaSpread(previousOptimumEstimations);
        int tries = 2 * function.getDimension() * function.getDimension();
        while (tries-- > 0) {
            TwoDistinctIds twoDistinctIds = new TwoDistinctIds().invoke();
            int diffVectorIdx1 = twoDistinctIds.getDiffVectorIdx1();
            int diffVectorIdx2 = twoDistinctIds.getDiffVectorIdx2();
            int randomOptimumIdx = twoDistinctIds.getRandomOptimumIdx();
            double[] diffVector1 = previousOptimumEstimations.get(diffVectorIdx1).getX();
            double[] diffVector2 = previousOptimumEstimations.get(diffVectorIdx2).getX();
            double[] originVector = previousOptimumEstimations.get(randomOptimumIdx).getX();
            double[] x = new double[originVector.length];
            for (int dim = 0; dim < originVector.length; ++dim) {
                x[dim] = originVector[dim] + (diffVector2[dim] - diffVector1[dim]);
            }
            final Region region = genearateRegionFromMidpointAndUnorganizedBounds(x,
                    modelSearchBounds[0], modelSearchBounds[1], function.getDimension());

            for (int dim = 0; dim < originVector.length; ++dim) {
                if (region.getCurrentAreaOfInterest()[0][dim] > function.getBounds().getUpper()[dim])
                    continue;
                if (region.getCurrentAreaOfInterest()[1][dim] < function.getBounds().getLower()[dim])
                    continue;
            }

            if (!previousOptimumEstimations.stream().anyMatch(opti -> region.isPointInside(opti.getX()))) {
                return region;
            }
        }
        return null;
    }

    private List<Region> selectPossibleRegions(int bestOptimumIdx) {
        Sample originPoint = previousOptimumEstimations.get(bestOptimumIdx);
        List<Region> possibleRegions = new ArrayList<>();
        for (Region region : regionsForExploration) {
            if (region.isPointInside(originPoint.getX())) {
                possibleRegions.add(region);
            }
        }
        return possibleRegions;
    }

    private class TwoDistinctIds {
        private int diffVectorIdx1;
        private int diffVectorIdx2;
        private int randomOptimumIdx;

        public int getDiffVectorIdx1() {
            return diffVectorIdx1;
        }

        public int getDiffVectorIdx2() {
            return diffVectorIdx2;
        }

        public int getRandomOptimumIdx() {
            return randomOptimumIdx;
        }

        public TwoDistinctIds invoke() {
            diffVectorIdx1 = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
            diffVectorIdx2 = diffVectorIdx1;
            randomOptimumIdx = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
            while (diffVectorIdx2 == diffVectorIdx1) {
                diffVectorIdx2 = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
            }
            return this;
        }
    }
}

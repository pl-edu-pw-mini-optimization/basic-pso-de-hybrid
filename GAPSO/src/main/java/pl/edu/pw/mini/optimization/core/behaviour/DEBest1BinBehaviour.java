package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.ArrayList;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.Generator.generateNewDESample;

/**
 * Standard Local PSO behaviour
 */
public class DEBest1BinBehaviour extends Behaviour {

    protected double _crossProb;
    protected double _scalingFactor;

    public DEBest1BinBehaviour(double weight, double crossProb, double scalingFactor) {
        super(weight);
        _crossProb = crossProb;
        _scalingFactor = scalingFactor;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        List<Particle> particles = particle.getParticles();
        List<Integer> chosen = new ArrayList<>();
        chosen.add(particles.indexOf(particle));
        while (chosen.size() < 3) {
            int tryAdd = Generator.RANDOM.nextInt(particles.size());
            if (!chosen.contains(tryAdd)) {
                chosen.add(tryAdd);
            }
        }
        Integer[] chosenArray = chosen.toArray(new Integer[0]);
        double[] newV = new double[particle.getV().length];
        double[] localXBest = getBestLocation(particle, particles);
        double scale = getScale(particle);
        double crossProb = getCrossProb(particle);

        Loggers.MOVES_LOGGER.registerBaseSample(localXBest, "Best");
        Loggers.MOVES_LOGGER.registerBaseSample(particles.get(chosenArray[0]).getXBest(), "Current");
        Loggers.MOVES_LOGGER.registerBaseSample(particles.get(chosenArray[1]).getXBest(),"Diff1");
        Loggers.MOVES_LOGGER.registerBaseSample(particles.get(chosenArray[2]).getXBest(), "Diff2");
        double[] newX = generateNewDESample(
                localXBest,
                particles.get(chosenArray[0]).getXBest(),
                particles.get(chosenArray[1]).getXBest(),
                particles.get(chosenArray[2]).getXBest(),
                scale, crossProb,
                particle.getFunction().getBounds());
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = -particle.getX()[i] + newX[i];
        }
        return newV;
    }

    protected double getCrossProb(Particle p) {
        return _crossProb;
    }

    protected double getScale(Particle p) {
        return Generator.RANDOM.nextDouble() * _scalingFactor;
    }

    protected double[] getBestLocation(Particle particle, List<Particle> particles) {
        return particle.getGlobalBestSoFar();
    }
}

package pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation;

import java.util.ArrayList;

public class Evaluation {
    private final ArrayList<Double> distances;
    private int countContaining;

    public Evaluation() {
        this.countContaining = 0;
        this.distances = new ArrayList<>();
    }

    public void addMeasurement(boolean isContained, double distance) {
        if (isContained) countContaining++;
        distances.add(distance);
    }

    public double getContainmentRatio() {
        return ((double)this.countContaining) / distances.size();
    }

    public double getContainmentMeanDistance() {
        return distances
                .stream()
                .mapToDouble(item -> item)
                .average()
                .getAsDouble();
    }
}

package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.distribution.CauchyDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JADEbBest1BinBehaviour extends DEBest1BinBehaviour {
    protected List<List<Pair<Particle, Double>>> cpRegister = new ArrayList<>();
    protected List<List<Pair<Particle, Double>>> sfRegister = new ArrayList<>();

    public JADEbBest1BinBehaviour(double weight, double crossProb, double scalingFactor) {
        super(weight, crossProb, scalingFactor);
    }

    @Override
    public void registerIteration(int historySize) {
        super.registerIteration(historySize);
        cpRegister.add(new ArrayList<>());
        sfRegister.add(new ArrayList<>());
        if (cpRegister.size() > 2)
            cpRegister.remove(0);
        if (sfRegister.size() > 2)
            sfRegister.remove(0);
        List<Double> selectedCps = selectSuccesfulFactorValues(cpRegister);
        if (selectedCps.size() > 0) {
            double count = cpRegister.get(cpRegister.size() - 2).size();
            double succ = selectedCps.size();
            _crossProb = (1 - succ / count) * _crossProb + succ / count * selectedCps
                    .stream()
                    .mapToDouble(item -> item)
                    .average()
                    .getAsDouble();
        }
        List<Double> selectedSfs = selectSuccesfulFactorValues(sfRegister);
        if (selectedSfs.size() > 0) {
            double count = sfRegister.get(sfRegister.size() - 2).size();
            double succ = selectedCps.size();
            _scalingFactor = (1 - succ / count) * _scalingFactor + succ / count *
                    selectedSfs
                            .stream()
                            .mapToDouble(item -> item * item)
                            .sum() /
                            selectedSfs
                                    .stream()
                                    .mapToDouble(item -> item)
                                    .sum();
        }
    }

    private List<Double> selectSuccesfulFactorValues(List<List<Pair<Particle,Double>>> factorRegister) {
        List<Double> selectedFactors = new ArrayList<>();
        if (factorRegister.size() >= 2 && changesRegister.size() >= 2) {
            List<Pair<Particle, Double>> deltas = changesRegister.get(changesRegister.size() - 2);
            List<Pair<Particle, Double>> factors = factorRegister.get(factorRegister.size() - 2);
            List<Particle> positiveDeltasParticles = deltas
                    .stream()
                    .filter(item -> item.getValue() > 0)
                    .map(item -> item.getKey())
                    .collect(Collectors.toList());
            if (positiveDeltasParticles.size() > 0) {
                for (Pair<Particle, Double> row : factors) {
                    if (positiveDeltasParticles.contains(row.getKey()))
                        selectedFactors.add(row.getValue());
                }
            }
        }
        return selectedFactors;
    }

    @Override
    protected double getCrossProb(Particle p) {
        NormalDistribution nd = new NormalDistribution(Generator.RANDOM, _crossProb, 0.1);
        double cpSampled = nd.sample();
        cpRegister.get(cpRegister.size() - 1).add(new Pair<Particle, Double>(p, cpSampled));
        return cpSampled;
    }

    @Override
    protected double getScale(Particle p) {
        CauchyDistribution cd = new CauchyDistribution(Generator.RANDOM, _scalingFactor, 0.1);
        double sfSampled = cd.sample();
        sfRegister.get(sfRegister.size() - 1).add(new Pair<Particle, Double>(p, sfSampled));
        return sfSampled;
    }


}

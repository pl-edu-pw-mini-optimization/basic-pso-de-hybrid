package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.BoundsGenerator;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.FunctionBoundaryBoundsGenerator;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.OtherPointBoundsGenerator;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

public class OverlappingRegionsExploringLocationsInitializer extends ExploringNewRegionsLocationInitalizer {
    public OverlappingRegionsExploringLocationsInitializer(SamplesGatherer function) {
        super(function);
    }

    @Override
    public void resetState() {
        if (exploitingInitializer != null) {
            if (exploitingInitializer.isActive()) {
                exploitingInitializer.resetState();
            } else {
                processLastOptimumEstimation();
                createNewInitializer();
                double[][] newBounds = null;
                int choice = Generator.RANDOM.nextInt(4);
                if (previousOptimumEstimations.size() < 2 || choice < 1) {
                    Sample previousOptimumEstimation = previousOptimumEstimations.get(Generator.RANDOM.nextInt(previousOptimumEstimations.size()));
                    newBounds = computeNewBoundsFromSingleOptimum(previousOptimumEstimation);
                } else if (choice == 1) {
                    int firstOptimum = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
                    int secondOptimum = getAnotherOptimum(firstOptimum);
                    Sample previousOptimumEstimation1 = previousOptimumEstimations.get(firstOptimum);
                    Sample previousOptimumEstimation2 = previousOptimumEstimations.get(secondOptimum);
                    newBounds = computeNewBoundsFromTwoOptima(previousOptimumEstimation1, previousOptimumEstimation2);
                } else if (choice == 2) {
                    int bestOptimumIdx = getBestOptimumIdx();
                    int firstOptimum = bestOptimumIdx;
                    int secondOptimum = getAnotherOptimum(firstOptimum);
                    Sample previousOptimumEstimation1 = previousOptimumEstimations.get(firstOptimum);
                    Sample previousOptimumEstimation2 = previousOptimumEstimations.get(secondOptimum);
                    newBounds = computeNewBoundsFromTwoOptima(previousOptimumEstimation1, previousOptimumEstimation2);
                } else {
                    int bestOptimumIdx = getBestOptimumIdx();
                    Sample bestOptimumEstimation = previousOptimumEstimations.get(bestOptimumIdx);
                    newBounds = computeNewBoundsCloseToOptimum(bestOptimumEstimation, 0.1 + Generator.RANDOM.nextDouble() * 0.15);
                }
                exploitingInitializer.shrinkBoundsWithinBounds(newBounds);
                exploitingInitializer.computeDirectionParameters();
            }
        }
    }

    private double[][] computeNewBoundsFromTwoOptima(Sample previousOptimumEstimation1, Sample previousOptimumEstimation2) {
        BoundsGenerator generator = new OtherPointBoundsGenerator(previousOptimumEstimation1, 0.5);
        return generator.generateSearchBounds(previousOptimumEstimation2);
    }

    private double[][] computeNewBoundsFromSingleOptimum(Sample previousOptimumEstimation) {
        int side = Generator.RANDOM.nextInt(2);
        int dim = Generator.RANDOM.nextInt(dimension);

        BoundsGenerator generator = new FunctionBoundaryBoundsGenerator(bounds.toArray(), side, dim, 0.5);
        return generator.generateSearchBounds(previousOptimumEstimation);
    }

    private int getAnotherOptimum(int firstOptimum) {
        int secondOptimum = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
        while (secondOptimum == firstOptimum) {
            secondOptimum = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
        }
        return secondOptimum;
    }

}

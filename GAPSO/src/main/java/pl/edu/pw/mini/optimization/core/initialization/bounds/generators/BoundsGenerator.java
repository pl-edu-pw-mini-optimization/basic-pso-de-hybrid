package pl.edu.pw.mini.optimization.core.initialization.bounds.generators;

import pl.edu.pw.mini.optimization.core.sampling.Sample;

public abstract class BoundsGenerator {
    public boolean isDominatedByForReferencePoint(BoundsGenerator other, Sample referencePoint) {
        double[][] otherBounds = other.getBounds(referencePoint);
        double[][] bounds = this.getBounds(referencePoint);
        for (int d = 0; d < bounds[0].length; ++d) {
            if (otherBounds[0][d] < bounds[0][d] || otherBounds[1][d] > bounds[1][d]) {
                return false;
            }
        }
        return true;
    }

    public abstract double[][] getBounds(Sample origin);
    public abstract double[][] generateSearchBounds(Sample origin);
}

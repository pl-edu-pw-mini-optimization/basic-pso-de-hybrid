package pl.edu.pw.mini.optimization;

import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;

public interface OptimizationAlgorithm {
    /**
     * Method run in order to find an approximation of optimal solution
     *
     * @param suggestedIterations number of suggested iterations of the optimization algorithm
     * @return location of optimum approximation
     */
    double[] optimize(int suggestedIterations);

    /**
     *
     * @return estimated optimum value
     */
    double getOptimumValue();
    /**
     *
     * @return true if algorithm recognizes itself as being stuck
     */
    boolean shouldBeRestarted();

    /**
     *
     * @return sanples memory gathered during optimization process
     */
    RTree getSamplesIndex();

    /**
     * Clears algorithm and samples memory
     */
    void clearMemory();

    /**
     *
     * @return true if the optimization should be discontinued (useful for deterministic algorithms)
     */
    boolean breakComputations();
}

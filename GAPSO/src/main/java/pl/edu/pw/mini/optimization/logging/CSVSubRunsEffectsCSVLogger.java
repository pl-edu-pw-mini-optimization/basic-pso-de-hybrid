package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

import static pl.edu.pw.mini.optimization.utils.Sanitizer.sanitizeFilename;

public class CSVSubRunsEffectsCSVLogger extends BaseCSVLogger implements SubRunsLogger {

    private Bounds bounds;
    private Sample optimumEstimation;

    @Override
    public void registerBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    @Override
    public void registerEstimatedOptimum(Sample optimumEstimation) {
        this.optimumEstimation = optimumEstimation;
        logEntry();
    }

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        super.registerRun(configuration, functionName, functionDimension);
        this.bounds = null;
        this.optimumEstimation = null;
    }

    @Override
    protected void logEntry() {
        if (active && optimumEstimation != null && bounds != null) {
            boolean isWithinBounds = true;
            for (int d = 0; d < optimumEstimation.getX().length; ++d) {
                if (optimumEstimation.getX()[d] > bounds.getUpper()[d] || optimumEstimation.getX()[d] < bounds.getLower()[d]) {
                    isWithinBounds = false;
                    break;
                }
            }
            double diameter = 0.0;
            for (int d = 0; d < optimumEstimation.getX().length; ++d) {
                diameter += (bounds.getUpper()[d] - bounds.getLower()[d]) * (bounds.getUpper()[d] - bounds.getLower()[d]);
            }
            diameter = ((double)Math.round(Math.sqrt(diameter) * 10000)) / 10000;

            lastLog = new Date();
            StringBuilder sb = new StringBuilder();
            sb.append(functionName);
            sb.append(',');
            sb.append(iteration);
            sb.append(',');
            sb.append(isWithinBounds);
            sb.append(',');
            sb.append(diameter);
            sb.append(',');
            for (int d = 0; d < optimumEstimation.getX().length; ++d) {
                sb.append(bounds.getLower()[d]);
                sb.append(',');
                sb.append(bounds.getUpper()[d]);
                sb.append(',');
            }
            for (int d = 0; d < optimumEstimation.getX().length; ++d) {
                sb.append(optimumEstimation.getX()[d]);
                sb.append(',');
            }
            sb.append(optimumEstimation.getValue());
            sb.append(System.lineSeparator());
            try {
                Files.write(Paths.get("RUN-" + configuration + '-' + experimentStart + ".csv"), sb.toString().getBytes(),
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                //
                System.err.println("Unable to log to file");
                System.err.println(sb.toString());
            }
            this.bounds = null;
            this.optimumEstimation = null;
            this.active = false;
        }
    }

    @Override
    protected String logType() {
        return "subrun";
    }

    @Override
    protected void generateHeader() {

    }
}

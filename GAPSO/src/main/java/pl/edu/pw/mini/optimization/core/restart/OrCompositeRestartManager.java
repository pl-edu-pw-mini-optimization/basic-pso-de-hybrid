package pl.edu.pw.mini.optimization.core.restart;

import java.util.List;

public class OrCompositeRestartManager extends  CompositeRestartManager {
    public OrCompositeRestartManager(List<SwarmRestartManager> restartManagers) {
        super(restartManagers);
    }

    @Override
    public boolean shouldBeRestarted() {
        return restartManagers.stream().anyMatch(rm -> rm.shouldBeRestarted());
    }

    @Override
    protected String getSeparToken() {
        return "Or";
    }
}

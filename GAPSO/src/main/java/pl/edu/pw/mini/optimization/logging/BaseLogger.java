package pl.edu.pw.mini.optimization.logging;

public interface BaseLogger {
    void registerRun(String configuration, String functionName, int functionDimension);
    void registerIteration();
    void finalize();
}

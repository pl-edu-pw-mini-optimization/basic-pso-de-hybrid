package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.NamedObject;

public interface NextMoveSamplesLogger extends BaseLogger {
    void registerNewMove();
    void registerBaseSample(double[] location, String sampleRole);
    void registerEffectSample(double[] location, NamedObject moveType);
}

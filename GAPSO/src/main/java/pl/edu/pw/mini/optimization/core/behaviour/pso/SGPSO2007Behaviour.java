package pl.edu.pw.mini.optimization.core.behaviour.pso;

import pl.edu.pw.mini.optimization.core.Particle;

import java.util.List;

public class SGPSO2007Behaviour extends StandardPSO2007Behaviour {

    public SGPSO2007Behaviour(double weight, double omega, double c1, double c2) {
        super(weight, omega, c1, c2);
    }

    @Override
    protected double[] getBestLocation(Particle particle, List<Particle> particles) {
        return this.getBestGlobalLocation(particle, particles);
    }
}

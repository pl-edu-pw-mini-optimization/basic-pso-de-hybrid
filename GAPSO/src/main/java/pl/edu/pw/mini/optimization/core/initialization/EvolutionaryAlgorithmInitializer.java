package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.Generator.generateNewEASample;

public class EvolutionaryAlgorithmInitializer extends SampledLocationInitializer {
    private double _mutationStdDevFactor = 1.0;
    private double _crossProb = 0.7;

    EvolutionaryAlgorithmInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
    }

    @Override
    public double[] getNext() {
        List<Sample> data = sampledFunction.getNearestSamples(sampledFunction.getSamplesCount(),
                new double[dimension]);

        return generateNewEASample(data, _mutationStdDevFactor, _crossProb, bounds);
    }

    @Override
    public boolean isActive() {
        return sampledFunction.getSamplesCount() > 1;
    }

}

package pl.edu.pw.mini.optimization.logging;

public class NullSwarmStateLogger implements SwarmStateLogger {
    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {

    }


    @Override
    public void registerStagnation(int stagnationPeriod) {

    }

    @Override
    public void registerVariation(double swarmDiameter) {

    }

    @Override
    public void registerSamplesCount(int samplesCount) {

    }

    @Override
    public void registerIteration() {

    }

    @Override
    public void finalize() {

    }

    @Override
    public void registerRestart() {

    }
}

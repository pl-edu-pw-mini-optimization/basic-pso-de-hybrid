package pl.edu.pw.mini.optimization.core.popsize;

public class BudgetRankMultiplier extends PopulationSizeAdjuster {

    private final int multiplier;
    private final static int DEFAULT_MULTIPLIER = 10;

    public BudgetRankMultiplier() {
        this(DEFAULT_MULTIPLIER);
    }

    public BudgetRankMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return multiplier + "LOG_BUDGET";
    }

    @Override
    public int getSize(int dimension, int budgetSizeRank, int behavioursCount) {
        return multiplier * budgetSizeRank;
    }
}

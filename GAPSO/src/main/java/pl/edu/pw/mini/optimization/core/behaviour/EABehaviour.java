package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pl.edu.pw.mini.optimization.utils.Generator.generateNewEASample;

/**
 * Standard Local PSO behaviour
 */
public class EABehaviour extends Behaviour {

    private double _mutationStdDevFactor;
    private double _crossProb;

    public EABehaviour(double weight, double crossProb, double mutationStdDevFactor) {
        super(weight);
        _mutationStdDevFactor = mutationStdDevFactor;
        _crossProb = crossProb;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        List<Particle> particles = particle.getParticles();
        List<Sample> data = particles
                .stream()
                .map(item -> new SingleSample(item.getXBest(), item.getYBest()))
                .collect(Collectors.toList());

        Sample particleSample = data.get(particles.indexOf(particle));
        double[] x = generateNewEASample(particleSample, data, _mutationStdDevFactor, _crossProb, particle.getFunction().getBounds());

        double[] newV = new double[particle.getV().length];
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = -particle.getX()[i] + x[i];
        }
        return newV;
    }
}

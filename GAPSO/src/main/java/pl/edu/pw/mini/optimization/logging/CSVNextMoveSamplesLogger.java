package pl.edu.pw.mini.optimization.logging;

import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.NamedObject;

import java.util.*;

public class CSVNextMoveSamplesLogger extends BaseCSVLogger implements NextMoveSamplesLogger {
    private class Moves {
        private double[] resultSample;
        private List<Pair<double[],String>> baseSamples = new ArrayList<>();
        private String moveType;
        private int moveId;
        private int iteration;

        public double[] getResultSample() {
            return resultSample;
        }

        public void setResultSample(double[] resultSample) {
            this.resultSample = resultSample;
        }

        public List<Pair<double[],String>> getBaseSamples() {
            return baseSamples;
        }

        public String getMoveType() {
            return moveType;
        }

        public void setMoveType(String moveType) {
            this.moveType = moveType;
        }

        public void addBaseSample(Pair<double[],String> baseSampleLocation) {
            baseSamples.add(baseSampleLocation);
        }

        public void clearBaseSamples() {
            baseSamples.clear();
        }

        public int getMoveId() {
            return moveId;
        }

        public void setMoveId(int moveId) {
            this.moveId = moveId;
        }

        public int getIteration() {
            return iteration;
        }

        public void setIteration(int iteration) {
            this.iteration = iteration;
        }
    }

    private Moves currentMoves;
    private List<Moves> loggedMoves;
    private int countSamples = 0;


    @Override
    protected void logEntry() {
        if (this.active) {
            for (Moves moves : loggedMoves) {
                for (Pair<double[],String> x: moves.getBaseSamples()) {
                    logSample(moves, x.getFirst(), x.getSecond());
                }
                logSample(moves, moves.getResultSample(), moves.getMoveType());
                moves.clearBaseSamples();
            }
            loggedMoves.clear();
        }


    }

    private void logSample(Moves moves, double[] x, String sampleType) {
        lastLog = new Date();
        StringBuilder sb = new StringBuilder();
        sb.append(functionName);
        sb.append(',');
        sb.append(String.format("%3d",run));
        sb.append(',');
        sb.append(String.format("%3d", moves.getIteration()));
        sb.append(',');
        sb.append(String.format("%3d", moves.getMoveId()));
        sb.append(',');
        for (int d = 0; d < x.length; ++d) {
            sb.append(String.format(Locale.ENGLISH, "%11.8f", x[d]));
            sb.append(',');
        }
        sb.append(sampleType);
        sb.append(System.lineSeparator());
        logLineToLogFile(sb);
    }

    @Override
    protected String logType()  {
        return "moves";
    }

    @Override
    protected void generateHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("Function");
        sb.append(',');
        sb.append("Run");
        sb.append(',');
        sb.append("Iteration");
        sb.append(',');
        sb.append("SampleCount");
        sb.append(',');
        for (int dim = 0; dim < this.dimension; ++dim) {
            sb.append("X");
            sb.append(dim+1);
            sb.append(',');
        }
        sb.append("SampleType");
        sb.append(System.lineSeparator());
        logLineToLogFile(sb);
    }

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        super.registerRun(configuration, functionName, functionDimension);
        loggedMoves = new ArrayList<>();
    }

    @Override
    public void registerNewMove() {
        if (currentMoves != null) {
            loggedMoves.add(currentMoves);
        }
        currentMoves = new Moves();
        currentMoves.setMoveId(countSamples++);
        currentMoves.setIteration(this.iteration);
    }

    @Override
    public void registerBaseSample(double[] location, String sampleRole) {
        double[] locationCopy = Arrays.copyOf(location, location.length);
        currentMoves.addBaseSample(new Pair<>(locationCopy, sampleRole));
    }

    @Override
    public void registerEffectSample(double[] location, NamedObject moveType) {
        double[] locationCopy = Arrays.copyOf(location, location.length);
        currentMoves.setMoveType(moveType.toString());
        currentMoves.setResultSample(locationCopy);
    }
}

package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

public abstract class PreviousOptimaAwareLocationInitializer extends SampledLocationInitializer {
    PreviousOptimaAwareLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
    }

    public abstract void registerNewOptimumEstimation(Sample sample);

}

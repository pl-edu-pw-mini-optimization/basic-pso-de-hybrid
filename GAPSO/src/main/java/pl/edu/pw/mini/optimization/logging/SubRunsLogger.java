package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

public interface SubRunsLogger extends BaseLogger {
    void registerBounds(Bounds bounds);
    void registerEstimatedOptimum(Sample optimumEstimation);
}

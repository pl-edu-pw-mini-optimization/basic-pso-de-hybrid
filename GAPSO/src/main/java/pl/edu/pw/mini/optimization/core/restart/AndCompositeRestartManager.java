package pl.edu.pw.mini.optimization.core.restart;

import java.util.List;

public class AndCompositeRestartManager extends CompositeRestartManager {
    public AndCompositeRestartManager(List<SwarmRestartManager> restartManagers) {
        super(restartManagers);
    }

    @Override
    public boolean shouldBeRestarted() {
        return restartManagers.stream().allMatch(rm -> rm.shouldBeRestarted());
    }

    @Override
    protected String getSeparToken() {
        return "And";
    }
}

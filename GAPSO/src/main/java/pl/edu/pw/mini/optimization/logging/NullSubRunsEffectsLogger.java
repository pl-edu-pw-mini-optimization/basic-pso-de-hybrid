package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

public class NullSubRunsEffectsLogger implements SubRunsLogger {
    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {

    }

    @Override
    public void registerBounds(Bounds bounds) {

    }

    @Override
    public void registerEstimatedOptimum(Sample optimumEstimation) {

    }

    @Override
    public void registerIteration() {

    }

    @Override
    public void finalize() {

    }
}

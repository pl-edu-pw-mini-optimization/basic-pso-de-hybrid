package pl.edu.pw.mini.optimization.core.restart;

import org.apache.commons.math3.stat.StatUtils;
import pl.edu.pw.mini.optimization.utils.Loggers;

public class ParticlesBestLocationsCollapsed extends ParticlesRestartManager {
    private final double epsDiameter;
    private final int dimension;

    public ParticlesBestLocationsCollapsed(double epsDiameter, int dimension) {
        this.epsDiameter = epsDiameter;
        this.dimension = dimension;
    }

    @Override
    public boolean shouldBeRestarted() {
        if (particles.size() < 2) {
            return false;
        }
        double totalAverageVariance = 0.0;
        for (int d = 0; d < dimension; ++d) {
            final int currentDim = d;
            totalAverageVariance += StatUtils.populationVariance(
                    particles
                            .stream()
                            .mapToDouble(item -> item.getXBest()[currentDim])
                            .toArray()
            ) / dimension;
        }
        Loggers.SWARM_LOGGER.registerVariation(totalAverageVariance);
        return (totalAverageVariance < epsDiameter);
    }

    @Override
    public String toString() {
        return "Collapsed";
    }
}

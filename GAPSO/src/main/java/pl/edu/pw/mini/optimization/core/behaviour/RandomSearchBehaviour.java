package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Generator;

/**
 * Standard Local PSO behaviour
 */
public class RandomSearchBehaviour extends Behaviour {

    public RandomSearchBehaviour(double weight) {
        super(weight);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        particle.resetBest();
        double[] newV = Generator.generateRandomWithinBounds(
                particle.getFunction().getBounds(),
                particle.getFunction().getDimension());
        for (int i = 0; i < newV.length; ++i) {
            newV[i] -= particle.getX()[i];
        }
        return newV;
    }
}

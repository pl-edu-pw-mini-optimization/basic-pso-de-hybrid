package pl.edu.pw.mini.optimization.utils;

public class Sanitizer {
    public static String sanitizeFilename(String inputName) {
        return inputName.replaceAll("[^a-zA-Z0-9-_.]", "_");
    }

}

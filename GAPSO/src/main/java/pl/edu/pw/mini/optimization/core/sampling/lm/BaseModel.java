package pl.edu.pw.mini.optimization.core.sampling.lm;

import org.apache.commons.math3.linear.SingularMatrixException;

public abstract class BaseModel {
    protected double regressionSE = Double.NaN;

    public abstract CredibleValue evaluateInModel(double[] x);

    public double getRegressionSE() {
        return regressionSE;
    }
}

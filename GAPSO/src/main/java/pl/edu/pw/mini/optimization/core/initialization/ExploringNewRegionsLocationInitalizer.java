package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.bounds.Region;
import pl.edu.pw.mini.optimization.core.initialization.knowledge.OptimumInfo;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.PropertiesAccessor.getGPSOConfigurationInstance;

public abstract class ExploringNewRegionsLocationInitalizer extends PreviousOptimaAwareLocationInitializer {
    protected final double OPTIMUM_DISTANCE_TOLERANCE = 1e-4;
    protected final double maxBestNeighbourhood;
    protected double minOptimumDistance;
    protected ExploitingPreviousOptimumLocationInitializer exploitingInitializer;
    protected List<Sample> previousOptimumEstimations;
    protected List<Region> regionsForExploration;


    public ExploringNewRegionsLocationInitalizer(SamplesGatherer function) {
        super(function);
        minOptimumDistance = Double.POSITIVE_INFINITY;
        exploitingInitializer = new ExploitingPreviousOptimumLocationInitializer(function);
        previousOptimumEstimations = new ArrayList<>();
        regionsForExploration = new ArrayList<>();
        regionsForExploration.add(new Region(bounds.toArray()));
        maxBestNeighbourhood = getGPSOConfigurationInstance().getMaxBestNeighbourhood();
    }

    @Override
    public double[] getNext() {
        if (exploitingInitializer != null) {
            if (exploitingInitializer.isActive()) {
                return exploitingInitializer.getNext();
            }
        }
        return getRandomWithinFunctionBounds();
    }

    protected OptimumInfo processLastOptimumEstimation() {
        Sample newOptimumEstimation = exploitingInitializer.getOptimumEstimation();
        Sample optimumToReplace = checkCloseOptimumExist(newOptimumEstimation);

        boolean bestImproved = !previousOptimumEstimations
                .stream()
                .anyMatch(item -> item.getValue() < newOptimumEstimation.getValue());

        if (optimumToReplace == null) {
            for (Sample optimum : previousOptimumEstimations) {
                minOptimumDistance = Math.min(minOptimumDistance,
                        optimum.getDistance(newOptimumEstimation.getX()));
            }
            previousOptimumEstimations.add(newOptimumEstimation);
            Region includingRegion = identifyIncludingRegion(newOptimumEstimation);
            if (includingRegion != null) {
                List<Region> newRegions = includingRegion.splitRegion(newOptimumEstimation);
                regionsForExploration.remove(includingRegion);
                regionsForExploration.addAll(newRegions);
            }
            return new OptimumInfo(newOptimumEstimation, null, true, bestImproved);
        } else if (optimumToReplace.getValue() >= newOptimumEstimation.getValue()) {
            for (Region region : regionsForExploration) {
                region.moveBoundary(optimumToReplace, newOptimumEstimation);
            }
            return new OptimumInfo(newOptimumEstimation, optimumToReplace, true, bestImproved);
        }
        return new OptimumInfo(newOptimumEstimation, null, false, false);
    }

    private Region identifyIncludingRegion(Sample newOptimumEstimation) {
        Region includingRegion = null;
        for (Region region : regionsForExploration) {
            if (region.isPointStrictlyInside(newOptimumEstimation.getX())) {
                includingRegion = region;
                break;
            }
        }
        return includingRegion;
    }

    protected Sample checkCloseOptimumExist(Sample newOptimumEstimation) {
        for (Sample oldOptimum: previousOptimumEstimations) {
            if (!oldOptimum.isSignificantlyDifferentLocation(newOptimumEstimation.getX(), 1e-4)) {
                return oldOptimum;
            }
        }
        return null;
    }

    protected void createNewInitializer() {
        double smallestSingleDirection = exploitingInitializer.getSmallestImprovementSingleDirectionDiameter();
        double sizeTolerance = exploitingInitializer.getSizeTolerance();
        int countToleranceWeight = exploitingInitializer.getCountToleranceWeight();
        exploitingInitializer = new ExploitingPreviousOptimumLocationInitializer(
                sampledFunction,
                ((sizeTolerance * countToleranceWeight) + smallestSingleDirection) / (countToleranceWeight + 1),
                countToleranceWeight);
    }

    protected int getBestOptimumIdx() {
        int bestOptimumIdx = -1;
        Sample bestOptimumEstimation = null;
        for (Sample oldOptimum: previousOptimumEstimations) {
            if (bestOptimumEstimation == null ||
                    oldOptimum.getValue() - bestOptimumEstimation.getValue()  < FUNCTION_VALUE_TOLERANCE) {
                bestOptimumEstimation = oldOptimum;
                bestOptimumIdx = previousOptimumEstimations.indexOf(oldOptimum);
            }
        }
        return bestOptimumIdx;
    }

    protected double[][] computeNewBoundsCloseToOptimum(Sample previousOptimumEstimation, double radius) {
        double[][] newBounds;
        newBounds = new double[2][];
        newBounds[0] = new double[dimension];
        newBounds[1] = new double[dimension];
        for (int d = 0; d < dimension; ++d) {
            double avgDLocation = previousOptimumEstimation.getX()[d];
            newBounds[0][d] = avgDLocation - radius * Generator.RANDOM.nextDouble();
            newBounds[1][d] = avgDLocation + radius * Generator.RANDOM.nextDouble();
        }
        return newBounds;
    }

    @Override
    public boolean isActive() {
        if (exploitingInitializer != null) {
            return (exploitingInitializer.isActive());
        }
        return false;
    }

    @Override
    public void registerNewOptimumEstimation(Sample sample) {
        if (exploitingInitializer != null) {
            exploitingInitializer.registerNewOptimumEstimation(sample);
        }
    }
}
package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.BoundsGenerator;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.FunctionBoundaryBoundsGenerator;
import pl.edu.pw.mini.optimization.core.initialization.bounds.generators.OtherPointBoundsGenerator;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.ArrayList;
import java.util.List;

public class NonoverlappingChaoticSystematicRegionsExploringLocationInitializer extends ExploringNewRegionsLocationInitalizer {
    private final EnumeratedDistribution<Integer> choicesDistrbution;
    private final double maxBestNeighbourhood;
    private final double toBoundaryMaxFraction;
    private final double betweenOptimaMaxFraction;

    public NonoverlappingChaoticSystematicRegionsExploringLocationInitializer(SamplesGatherer function) {
        super(function);
        List<Pair<Integer,Double>> choicesList = new ArrayList<>();
        double randomPointExploration = PropertiesAccessor.getGPSOConfigurationInstance().getExplorationStrategyWeightRandomPointExploration();
        double bestPointExploration = PropertiesAccessor.getGPSOConfigurationInstance().getExplorationStrategyWeightBestPointExploration();
        double bestPointExploatation = PropertiesAccessor.getGPSOConfigurationInstance().getExplorationStrategyWeightBestPointExploitation();
        double differencePointExploration = PropertiesAccessor.getGPSOConfigurationInstance().getExplorationStrategyWeightDifferencePointExploration();
        choicesList.add(new Pair<Integer,Double>(0,randomPointExploration));
        choicesList.add(new Pair<Integer,Double>(1,bestPointExploration));
        choicesList.add(new Pair<Integer,Double>(2,bestPointExploatation));
        choicesList.add(new Pair<Integer,Double>(3,differencePointExploration));
        choicesDistrbution = new EnumeratedDistribution<>(Generator.RANDOM, choicesList);
        maxBestNeighbourhood = PropertiesAccessor.getGPSOConfigurationInstance().getMaxBestNeighbourhood();
        toBoundaryMaxFraction = PropertiesAccessor.getGPSOConfigurationInstance().getToBoundaryMaxFraction();
        betweenOptimaMaxFraction = PropertiesAccessor.getGPSOConfigurationInstance().getBetweenOptimaMaxFraction();

    }

    @Override
    public void resetState() {
        if (exploitingInitializer != null) {
            if (exploitingInitializer.isActive()) {
                exploitingInitializer.resetState();
            } else {
                processLastOptimumEstimation();
                createNewInitializer();
                double[][] newBounds = null;

                int choice = choicesDistrbution.sample();

                int randomOptimumIdx = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
                int bestOptimumIdx = getBestOptimumIdx();

                List<BoundsGenerator> boundaryGenerators = generateBoundaryBoundsGenerators();

                if (choice == 0) { //random point neighbourhood
                    boundaryGenerators.addAll(generateOtherOptimaBoundsGenerators(randomOptimumIdx));
                    Sample originPoint = previousOptimumEstimations.get(randomOptimumIdx);
                    newBounds = generateBounds(boundaryGenerators, originPoint);
                } else if (choice == 1) { //best point neighbourhood
                    boundaryGenerators.addAll(generateOtherOptimaBoundsGenerators(bestOptimumIdx));
                    Sample originPoint = previousOptimumEstimations.get(bestOptimumIdx);
                    newBounds = generateBounds(boundaryGenerators, originPoint);
                } else if (choice == 2) { //best point exploitation
                    Sample bestOptimumEstimation = previousOptimumEstimations.get(bestOptimumIdx);
                    newBounds = computeNewBoundsCloseToOptimum(bestOptimumEstimation, maxBestNeighbourhood);
                } else if (choice == 3) {
                    if (previousOptimumEstimations.size() > 1) {
                        int diffVectorIdx1 = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
                        int diffVectorIdx2 = diffVectorIdx1;
                        while (diffVectorIdx2 == diffVectorIdx1) {
                            diffVectorIdx2 = Generator.RANDOM.nextInt(previousOptimumEstimations.size());
                        }
                        double[] diffVector1 = previousOptimumEstimations.get(diffVectorIdx1).getX();
                        double[] diffVector2 = previousOptimumEstimations.get(diffVectorIdx2).getX();
                        double[] originVector = previousOptimumEstimations.get(randomOptimumIdx).getX();
                        double[] x = new double[originVector.length];
                        for (int dim = 0; dim < originVector.length; ++dim) {
                            x[dim] = originVector[dim] + (diffVector2[dim] - diffVector1[dim]);
                        }
                        Sample originPoint = new SingleSample(x, Double.POSITIVE_INFINITY);
                        if (checkCloseOptimumExist(originPoint) != null) {
                            boundaryGenerators.addAll(generateOtherOptimaBoundsGenerators(randomOptimumIdx));
                            originPoint = previousOptimumEstimations.get(randomOptimumIdx);
                            newBounds = generateBounds(boundaryGenerators, originPoint);
                        } else {
                            newBounds = computeNewBoundsCloseToOptimum(originPoint, maxBestNeighbourhood);
                        }
                    } else {
                        boundaryGenerators.addAll(generateOtherOptimaBoundsGenerators(randomOptimumIdx));
                        Sample originPoint = previousOptimumEstimations.get(randomOptimumIdx);
                        newBounds = generateBounds(boundaryGenerators, originPoint);
                    }
                } else {
                    throw new IllegalArgumentException("Wrong initializer choice: " + choice);
                }
                exploitingInitializer.resetOriginalFunctionBounds(function);
                exploitingInitializer.shrinkBoundsWithinBounds(newBounds);
                exploitingInitializer.computeDirectionParameters();
            }
        }
    }

    private double[][] generateBounds(List<BoundsGenerator> boundaryGenerators, Sample originPoint) {
        double[][] newBounds;
        boundaryGenerators = getNonDominatedGenerators(boundaryGenerators, originPoint);
        BoundsGenerator generator = boundaryGenerators.get(Generator.RANDOM.nextInt(boundaryGenerators.size()));
        newBounds = generator.generateSearchBounds(originPoint);
        return newBounds;
    }

    private List<BoundsGenerator> generateBoundaryBoundsGenerators() {
        List<BoundsGenerator> generators = new ArrayList<>();
        for (int side = 0; side < 2; ++side) {
            for (int dim = 0; dim < dimension; ++dim) {
                BoundsGenerator generator = new FunctionBoundaryBoundsGenerator(bounds.toArray(), side, dim, toBoundaryMaxFraction);
                generators.add(generator);
            }
        }
        return generators;
    }

    private List<BoundsGenerator> generateOtherOptimaBoundsGenerators(int excludeIdx) {
        List<BoundsGenerator> generators = new ArrayList<>();
        for (int i = 0; i < previousOptimumEstimations.size(); ++i) {
            if (i != excludeIdx) {
                generators.add(new OtherPointBoundsGenerator(previousOptimumEstimations.get(i), betweenOptimaMaxFraction));
            }
        }
        return generators;
    }

    private List<BoundsGenerator> getNonDominatedGenerators(List<BoundsGenerator> generators, Sample originPoint) {
        List<BoundsGenerator> nonDominatedGenerators = new ArrayList<>();
        for (int gIdx1 = 0; gIdx1 < generators.size(); ++gIdx1) {
            BoundsGenerator generatorToTest = generators.get(gIdx1);
            boolean isNonDominated = true;
            for (int gIdx2 = 0; gIdx2 < generators.size(); ++gIdx2) {
                if (gIdx1 == gIdx2) {
                    continue;
                }
                BoundsGenerator testingGenerator = generators.get(gIdx2);
                if (generatorToTest.isDominatedByForReferencePoint(testingGenerator, originPoint)) {
                    isNonDominated = false;
                    break;
                }
            }
            if (isNonDominated) {
                nonDominatedGenerators.add(generatorToTest);
            }
        }
        return nonDominatedGenerators;
    }



}

package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.utils.Generator;

public class BoundaryVertexLocationInitializer extends BaseLocationInitializer {
    private boolean[] selectUpperBoundArray;
    private int generatedSamplesCount;

    BoundaryVertexLocationInitializer(Function function) {
        super(function);
        initializeOriginVertexForOnBoundGenerator();
    }

    protected void initializeOriginVertexForOnBoundGenerator() {
        selectUpperBoundArray = new boolean[this.function.getDimension()];
        generatedSamplesCount = 0;
        for (int d = 0; d < selectUpperBoundArray.length; ++d) {
            selectUpperBoundArray[d] = Generator.RANDOM.nextBoolean();
        }
    }

    protected double[] getBoundaryVertex() {
        if (generatedSamplesCount <= dimension) {
            double[] x = new double[selectUpperBoundArray.length];
            for (int d = 0; d < x.length; ++d) {
                if (selectUpperBoundArray[d]) {
                    x[d] = bounds.getUpper()[d];
                } else {
                    x[d] = bounds.getLower()[d];
                }

            }
            if (generatedSamplesCount > 0) {
                switchOneDimensionToOppositeBoundary(x);
            }
            ++generatedSamplesCount;
            return x;
        }
        return getRandomWithinFunctionBounds();
    }

    private void switchOneDimensionToOppositeBoundary(double[] x) {
        if (!selectUpperBoundArray[generatedSamplesCount - 1]) {
            x[generatedSamplesCount - 1] = bounds.getUpper()[generatedSamplesCount - 1];
        } else {
            x[generatedSamplesCount - 1] = bounds.getLower()[generatedSamplesCount - 1];
        }
    }

    @Override
    public double[] getNext() {
        return getBoundaryVertex();
    }

    @Override
    public void resetState() {
        initializeOriginVertexForOnBoundGenerator();
    }

    @Override
    public boolean isActive() {
        return generatedSamplesCount <= dimension;
    }
}

package pl.edu.pw.mini.optimization.core.sampling.lm;

import com.fasterxml.jackson.databind.ser.Serializers;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.util.ArrayList;
import java.util.List;

public class SquareModel extends BaseModel {
    private final OLSMultipleLinearRegression olslm;
    private final int dim;
    private double[] ba;
    private double regressionSE = Double.NaN;
    private double rSquared = Double.NaN;
    private List<PredictedSample> tests = new ArrayList<>();

    private class PredictedSample {
        private double[] x;
        private double expected;
        private double actual;

        public PredictedSample(double[] x, double expected, double actual) {
            this.x = x;
            this.expected = expected;
            this.actual = actual;
        }

        public double getError() {
            return Math.abs(expected - actual);
        }
    }

    public SquareModel(int dim, List<Sample> data) {
        this.dim = dim;
        olslm = new OLSMultipleLinearRegression();
        int samplesCount = data.size();
        double[] y = new double[samplesCount];
        double[][] x = new double[samplesCount][];

        for (int i = 0; i < samplesCount; ++i) {
            y[i] = data.get(i).getValue();
            x[i] = new double[2 * dim];
            for (int j = 0; j < dim; ++j) {
                double xj = data.get(i).getX()[j];
                x[i][j] = xj;
                x[i][j + dim] = xj * xj;
            }
        }
        olslm.newSampleData(y, x);
    }

    public double getRSquared() {
        return rSquared;
    }

    public double getRegressionSE() {
        return this.regressionSE;
    }

    public double[] getBoundedParabolaPeak(Bounds functionBounds) throws SingularMatrixException {
        double[] boundedPeak;//TODO: check for singularity
        boundedPeak = new double[dim];
        createModelParameters();
        for (int i = 0; i < dim; i++) {
            if (ba[i + dim + 1] > 0) {
                boundedPeak[i] += getBoundedCoordinate(functionBounds, -ba[i + 1] / ba[i + dim + 1] / 2.0, i);
            } else if (ba[i + dim + 1] == 0) {
                boundedPeak[i] += getBoundedCoordinate(functionBounds, ba[i + 1] > 0 ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY, i);
            } else if (linearCutValue(ba[i + dim + 1], ba[i + 1], functionBounds.getLower()[i])
                    < linearCutValue(ba[i + dim + 1], ba[i + 1], functionBounds.getUpper()[i])) {
                boundedPeak[i] +=functionBounds.getLower()[i];
            } else {
                boundedPeak[i] += functionBounds.getUpper()[i];
            }
        }
        return boundedPeak;
    }

    private void createModelParameters() throws SingularMatrixException {
        ba = olslm.estimateRegressionParameters();
        regressionSE = olslm.estimateRegressionStandardError();
        rSquared = olslm.calculateRSquared();
    }

    private double getBoundedCoordinate(Bounds bounds, double value, int dimension) {
        if (value < bounds.getLower()[dimension])
            return bounds.getLower()[dimension];
        if (value > bounds.getUpper()[dimension])
            return bounds.getUpper()[dimension];
        return value;
    }

    @Override
    public CredibleValue evaluateInModel(double[] x) throws SingularMatrixException {
        if (ba == null) {
            createModelParameters();
        }
        double value = ba[0];
        for (int i = 0; i < x.length; ++i) {
            value += ba[i + 1 + dim] * x[i] * x[i];
            value += ba[i + 1] * x[i];
        }
        CredibleValue cv = new CredibleValue(value, this);
        return cv;
    }

    public void storeTestResult(double[] x, double expected, double actual) {
        this.tests.add(new PredictedSample(x, expected, actual));
    }

    public int getTestsCount() {
        return this.tests.size();
    }

    public double getAverageTestsResult() {
        return this
                .tests
                .stream()
                .mapToDouble(item -> item.getError())
                .average()
                .getAsDouble();
    }

    private double linearCutValue(double a, double b, double x) {
        return a * x * x + b * x;
    }


}

package pl.edu.pw.mini.optimization.core.sampling.lm;

import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.util.List;

public class LinearModel {
    private final OLSMultipleLinearRegression olslm;
    private final int dim;

    public LinearModel(int dim, List<Sample> data) {
        this.dim = dim;
        olslm = new OLSMultipleLinearRegression();
        int samplesCount = data.size();
        double[] y = new double[samplesCount];
        double[][] x = new double[samplesCount][];

        for (int i = 0; i < samplesCount; ++i) {
            y[i] = data.get(i).getValue();
            x[i] = new double[dim];
            for (int j = 0; j < dim; ++j) {
                double xj = data.get(i).getX()[j];
                x[i][j] = xj;
            }
        }
        olslm.newSampleData(y, x);
    }

    public double[] getBestBound(Bounds functionBounds) throws SingularMatrixException {
        double[] boundedPeak;//TODO: check for singularity
        boundedPeak = new double[dim];
        double[] ba = olslm.estimateRegressionParameters();
        for (int i = 0; i < dim; i++) {
            if (ba[i + 1] > 1e-8) {
                boundedPeak[i] = functionBounds.getLower()[i];
            } else if (ba[i + 1] < -1e-8) {
                boundedPeak[i] = functionBounds.getUpper()[i];
            } else {
                boundedPeak[i] = (functionBounds.getUpper()[i] + functionBounds.getLower()[i]) / 2.0;
            }
        }
        return boundedPeak;
    }


}

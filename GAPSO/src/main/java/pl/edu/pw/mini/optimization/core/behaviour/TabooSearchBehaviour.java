package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation.Evaluation;
import pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation.SamplesEvaluator;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.Arrays;

public class TabooSearchBehaviour extends RandomSearchBehaviour {
    private final double tryoutsMultiplier;

    public TabooSearchBehaviour(double weigth, double tryoutsMultiplier) {
        super(weigth);
        this.tryoutsMultiplier = tryoutsMultiplier;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        particle.resetBest();

        double largestAverageDistance = Double.NEGATIVE_INFINITY;
        SamplesEvaluator se = new SamplesEvaluator(particle.getFunction().getIndex());
        int dimPlusOne = particle.getFunction().getDimension() + 1;
        int tests = (int)(tryoutsMultiplier * dimPlusOne);
        int dim = particle.getFunction().getDimension();
        Bounds bounds = particle.getFunction().getBounds();

        double[] bestX = Generator.generateRandomWithUnsampledBias(se, tests, dim, bounds);
        double[] bestV = Arrays.copyOf(bestX, bestX.length);
        for (int i = 0; i < bestX.length; ++i) {
            bestV[i] -= particle.getX()[i];
        }
        return bestV;
    }
}

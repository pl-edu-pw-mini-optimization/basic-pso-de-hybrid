package pl.edu.pw.mini.optimization.core.functions;

public abstract class Function {
    public abstract double getValue(double[] x);

    public abstract Bounds getBounds();

    public abstract int getDimension();

    public abstract String getName();
}

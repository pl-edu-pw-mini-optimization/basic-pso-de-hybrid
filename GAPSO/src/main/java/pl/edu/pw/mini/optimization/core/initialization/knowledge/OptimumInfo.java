package pl.edu.pw.mini.optimization.core.initialization.knowledge;

import pl.edu.pw.mini.optimization.core.sampling.Sample;

public class OptimumInfo {
    private final Sample foundOptimum;
    private final Sample optimumToReplace;
    private final boolean newOptimum;
    private final boolean bestValueImproved;

    public OptimumInfo(Sample foundOptimum, Sample optimumToReplace, boolean newOptimum, boolean bestValueImproved) {
        this.foundOptimum = foundOptimum;
        this.optimumToReplace = optimumToReplace;
        this.newOptimum = newOptimum;
        this.bestValueImproved = bestValueImproved;
    }

    public boolean isBestValueImproved() {
        return bestValueImproved;
    }

    public boolean isNewOptimum() {
        return newOptimum;
    }

    public Sample getOptimumToReplace() {
        return optimumToReplace;
    }

    public Sample getFoundOptimum() {
        return foundOptimum;
    }
}

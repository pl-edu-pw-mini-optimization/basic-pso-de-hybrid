package pl.edu.pw.mini.optimization.core.functions;

public class LinearFunction extends Function {
    private final int _dimension;
    private final Bounds _bounds;

    public LinearFunction(int dimension) {
        this._dimension = dimension;
        this._bounds = new Bounds(-1.0, 3.0, dimension);

    }

    @Override
    public double getValue(double[] x) {
        double result = 0.0;
        double multiplier = 1.0;
        for (double x1 : x) {
            result += multiplier * x1;
            multiplier *= -1.0;
        }
        return result;
    }

    @Override
    public Bounds getBounds() {
        return _bounds;
    }

    @Override
    public int getDimension() {
        return _dimension;
    }

    @Override
    public String getName() {
        return "Linear";
    }
}

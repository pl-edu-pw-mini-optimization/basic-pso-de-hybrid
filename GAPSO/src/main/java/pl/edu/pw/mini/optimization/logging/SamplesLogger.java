package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.NamedObject;

public interface SamplesLogger extends BaseLogger {
    void registerSample(Sample sample, NamedObject namedObject);
}

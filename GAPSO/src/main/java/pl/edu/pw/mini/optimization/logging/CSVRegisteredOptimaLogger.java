package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.initialization.bounds.Region;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CSVRegisteredOptimaLogger extends BaseCSVLogger implements RegisteredOptimaLogger {
    private class SearchLog {
        private Region region = null;
        private Sample optimum = null;
        private Integer method = null;

        public double[][] getSearchBounds() {
            return region.getCurrentAreaOfInterest();
        }

        public void setSearchBounds(Bounds searchBounds) {
            this.region = new Region(searchBounds.toArray());
        }

        public Sample getOptimum() {
            return optimum;
        }

        public void setOptimum(Sample optimum) {
            this.optimum = optimum;
        }

        public int getMethod() {
            return method;
        }

        public void setMethod(int method) {
            this.method = method;
        }

        public boolean isComplete() {
            return method != null && region != null && optimum != null;
        }

        public boolean isOptimumWithinBounds() throws NullPointerException {
            if (!isComplete()) {
                throw new NullPointerException("One of the fields: method, region or optimum is not initialized");
            } else {
                return region.isPointInside(optimum.getX());
            }
        }
    }

    private SearchLog currentSearchLog;
    private List<SearchLog> searchLogs;

    @Override
    protected void generateHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("Function");
        sb.append(',');
        sb.append("Run");
        sb.append(',');
        sb.append("Iteration");
        sb.append(',');
        for (int dim = 0; dim < this.dimension; ++dim) {
            sb.append("X");
            sb.append(dim+1);
            sb.append(',');
        }
        sb.append("Y");
        sb.append(',');
        for (int dim = 0; dim < this.dimension; ++dim) {
            sb.append("LB");
            sb.append(dim+1);
            sb.append(',');
        }
        for (int dim = 0; dim < this.dimension; ++dim) {
            sb.append("UB");
            sb.append(dim+1);
            sb.append(',');
        }
        sb.append("Method");
        sb.append(System.lineSeparator());
        logLineToLogFile(sb);
    }

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        super.registerRun(configuration, functionName, functionDimension);
        currentSearchLog = new SearchLog();
        searchLogs = new ArrayList<>();
    }

    private void tryStashCurrentSearchLog() {
        if (currentSearchLog!=null && searchLogs!=null) {
            if (currentSearchLog.isComplete()) {
                searchLogs.add(currentSearchLog);
                currentSearchLog = new SearchLog();
            }
        }
    }

    @Override
    protected void logEntry() {
        tryStashCurrentSearchLog();
        if (this.active) {
            for (SearchLog logLine : searchLogs) {
                lastLog = new Date();
                StringBuilder sb = new StringBuilder();
                sb.append(functionName);
                sb.append(',');
                sb.append(String.format("%3d",run));
                sb.append(',');
                sb.append(String.format("%3d",iteration));
                sb.append(',');
                for (int d = 0; d < this.dimension; ++d) {
                    sb.append(String.format(Locale.ENGLISH, "%11.8f",logLine.getOptimum().getX()[d]));
                    sb.append(',');
                }
                sb.append(String.format(Locale.ENGLISH, "%16.8f", logLine.getOptimum().getValue()));
                sb.append(',');

                for (int d = 0; d < this.dimension; ++d) {
                    sb.append(String.format(Locale.ENGLISH, "%11.8f",logLine.getSearchBounds()[0][d]));
                    sb.append(',');
                }

                for (int d = 0; d < this.dimension; ++d) {
                    sb.append(String.format(Locale.ENGLISH, "%11.8f",logLine.getSearchBounds()[1][d]));
                    sb.append(',');
                }

                sb.append(logLine.getMethod());

                sb.append(System.lineSeparator());
                logLineToLogFile(sb);
            }
            searchLogs.clear();
        }
    }

    @Override
    protected String logType() {
        return "optima";
    }


    @Override
    public void registerSearchBounds(Bounds searchBounds) {
        tryStashCurrentSearchLog();
        currentSearchLog.setSearchBounds(searchBounds);
    }

    @Override
    public void registerInitializationMethod(int method) {
        tryStashCurrentSearchLog();
        currentSearchLog.setMethod(method);
    }

    @Override
    public void registerOptimum(Sample sample) {
        tryStashCurrentSearchLog();
        currentSearchLog.setOptimum(sample);
    }
}

package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.NamedObject;

public class NullSamplesLogger implements SamplesLogger {
    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {

    }


    @Override
    public void registerIteration() {

    }

    @Override
    public void finalize() {

    }

    @Override
    public void registerSample(Sample sample, NamedObject namedObject) {

    }
}

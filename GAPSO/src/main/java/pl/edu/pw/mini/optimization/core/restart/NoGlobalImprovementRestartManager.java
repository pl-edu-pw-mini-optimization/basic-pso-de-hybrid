package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Loggers;

public class NoGlobalImprovementRestartManager extends SwarmRestartManager {
    private final int maxIterations;
    private int iterationsSinceImnprovement;

    public NoGlobalImprovementRestartManager(int maxIterations) {
        this.maxIterations = maxIterations;
        this.iterationsSinceImnprovement = 0;
    }

    @Override
    public void registerNewIteration() {
        this.iterationsSinceImnprovement += 1;
    }

    @Override
    public void registerImprovement() {
        this.iterationsSinceImnprovement = 0;
    }

    @Override
    public void registerParticle(Particle particle) {
        //DO NOTHING ON PURPOSE
    }

    @Override
    public boolean shouldBeRestarted() {
        Loggers.SWARM_LOGGER.registerStagnation(iterationsSinceImnprovement);
        return iterationsSinceImnprovement > maxIterations;
    }

    @Override
    public String toString() {
        return "NoImprov";
    }
}

package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pl.edu.pw.mini.optimization.utils.Generator.generateNewDESample;

public class DEBest1BinLocationInitializer extends SampledLocationInitializer {
    private double _scalingFactor = 1.0;
    private double _crossProb = 0.5;

    DEBest1BinLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
    }

    @Override
    public double[] getNext() {
        List<Sample> data = sampledFunction.getNearestSamples(sampledFunction.getSamplesCount(),
                new double[dimension]);
        List<Integer> chosen = new ArrayList<>();
        chosen.add(Generator.RANDOM.nextInt(data.size()));
        while (chosen.size() < 3) {
            int tryAdd = Generator.RANDOM.nextInt(data.size());
            if (!chosen.contains(tryAdd)) {
                chosen.add(tryAdd);
            }
        }
        Integer[] chosenArray = chosen.toArray(new Integer[0]);
        double[] localXBest = getBestLocation(data);
        double[] current = data.get(chosenArray[0]).getX();
        double[] diffVector1 = data.get(chosenArray[1]).getX();
        double[] diffVector2 = data.get(chosenArray[2]).getX();
        double scale = getScale();
        double crossProb = getCrossProb();

        return generateNewDESample(localXBest, current, diffVector1, diffVector2, scale, crossProb, bounds);
    }

    @Override
    public boolean isActive() {
        return sampledFunction.getSamplesCount() > 3;
    }

    protected double getCrossProb() {
        return _crossProb;
    }

    protected double getScale() {
        return Generator.RANDOM.nextDouble() * _scalingFactor;
    }

    protected double[] getBestLocation(List<Sample> samples) {
        double[] localXBest = samples.get(0).getX();
        double localYBest = samples.get(0).getValue();
        for (Sample sample : samples) {
            if (sample.getValue() < localYBest) {
                localXBest = sample.getX();
                localYBest = sample.getValue();
            }
        }
        return localXBest;
    }
}

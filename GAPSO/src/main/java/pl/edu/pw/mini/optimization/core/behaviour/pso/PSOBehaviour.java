package pl.edu.pw.mini.optimization.core.behaviour.pso;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;

import java.util.List;

public abstract class PSOBehaviour extends Behaviour {
    protected final double _omega;
    protected final double _c1;
    protected final double _c2;

    public PSOBehaviour(double weight, double omega, double c1, double c2) {
        super(weight);
        _omega = omega;
        _c1 = c1;
        _c2 = c2;
    }

    protected double[] getBestLocalRingLocation(Particle particle, List<Particle> particles) {
        int particleIndex = particles.indexOf(particle);
        int nextParticleIndex = (particleIndex < particles.size() - 1) ? (particleIndex + 1) : 0;
        int previousParticleIndex = (particleIndex > 0) ? (particleIndex - 1) : (particles.size() - 1);
        double[] localXBest = particle.getXBest();
        double localYBest = particle.getYBest();
        if (particles.get(nextParticleIndex).getYBest() < localYBest) {
            localXBest = particles.get(nextParticleIndex).getXBest();
            localYBest = particles.get(nextParticleIndex).getYBest();
        }
        if (particles.get(previousParticleIndex).getYBest() < localYBest) {
            localXBest = particles.get(previousParticleIndex).getXBest();
            localYBest = particles.get(previousParticleIndex).getYBest();
        }
        return localXBest;
    }

    protected double[] getBestGlobalLocation(Particle particle, List<Particle> particles) {
        return particle.getGlobalBestSoFar();
    }
}

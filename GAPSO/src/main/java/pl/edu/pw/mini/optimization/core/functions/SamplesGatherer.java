package pl.edu.pw.mini.optimization.core.functions;

import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.ArrayList;
import java.util.List;

public class SamplesGatherer extends Function {

    private final Function _function;
    //private final TreeMap<Double, SingleSample> samples = new TreeMap<>();
    private RTree rTree;
    private double modelEstimationProbability;

    public SamplesGatherer(Function function, double modelEstimationProbability) {
        _function = function;
        this.modelEstimationProbability = modelEstimationProbability;
        rTree = new RTree( 2 * (2 * _function.getDimension() + 1));
    }

    @Override
    public double getValue(double[] x) {
        return getValue(x, Double.POSITIVE_INFINITY);
    }

    public double getValue(double[] x, double lowerBoundaryEstimation) {
        if (PropertiesAccessor.getGPSOConfigurationInstance().useFunctionCacheToCheckForDuplicates()) {
            Sample cachedSample = rTree.getCached(x);
            if (cachedSample != null) {
                return cachedSample.getValue();
            }
        }
        if (rTree.getCount() >= PropertiesAccessor.getGPSOConfigurationInstance().getMaxSamplesCount()) {
            rTree.clearIndex();
        }
        /*
        SquareModel sm = rTree.getPredictor(x);
        if (Generator.RANDOM.nextDouble() > (1 - modelEstimationProbability)) {
            if (sm != null && sm.getRSquared() > 0.99 && sm.getTestsCount() > getDimension()
                    && sm.getAverageTestsResult() < 2 * sm.getRegressionSE()) {
                SquareModel.CredibleValue cv = sm.evaluateInModel(x);
                if (cv != null && lowerBoundaryEstimation < cv.getValue() - sm.getAverageTestsResult()) {
                    return cv.getValue();
                }
            }
        }
        */
        double value = _function.getValue(x);
        SingleSample sample = new SingleSample(x, value);
        rTree.indexSample(sample);

        if (isAveraging()) {
            return getNearestSamples(getAveragingSampleSize(), x)
                    .stream()
                    .mapToDouble(s -> s.getValue())
                    .average()
                    .getAsDouble();
        }
        /*
        if (sm != null)
            sm.storeTestResult(x, sm.evaluateInModel(x).getValue() , value);
        */
        return value;
    }

    public int getAveragingSampleSize() {
        return 5 * _function.getDimension();
    }

    public int getMinSquareSampleSize() {
        return 2 * _function.getDimension() + 1;
    }

    public int getSquareSampleSize() {
        return 5 * _function.getDimension();
    }

    public boolean isAveraging() {
        return rTree.getCount() < PropertiesAccessor.getGPSOConfigurationInstance().getSamplingAveragingTime() * _function.getDimension();
    }

    @Override
    public Bounds getBounds() {
        return _function.getBounds();
    }

    @Override
    public int getDimension() {
        return _function.getDimension();
    }

    @Override
    public String getName() {
        return _function.getName();
    }

    public List<Sample> getNearestSamples(int k, double point[]) {
        return new ArrayList<>(rTree.getKNearestSamples(point, k));
    }

    public List<Sample> getNearestInDimensionSamples(int k, int dim, double point[]) {
        return new ArrayList<>(rTree.getKNearestInDimensionSamples(point, dim, k));
    }

    public List<Sample> getHighLevelSamples() {
        return new ArrayList<>(rTree.getKEvenlyDistributedSamples(2 * _function.getDimension() + 1));
    }

    public double[][] getPromisingRectangleBounds(double cFactor) {
        return rTree.getHighRankingUCTBoundingRectangle(cFactor);
    }

    public int getSamplesCount() {
        return rTree.getCount();
    }

    public boolean notInALeafNode(double[] x) {
        return !rTree.someLeafOverlaps(x);
    }

    public RTree getIndex() {
        return rTree;
    }
}

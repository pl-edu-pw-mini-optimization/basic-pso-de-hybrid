package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.behaviour.pso.*;
import pl.edu.pw.mini.optimization.utils.NamedObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Behaviour implements NamedObject {
    private double bestSwarmValue;
    protected List<List<Pair<Particle, Double>>> changesRegister;
    private double weight;

    public Behaviour(double weigth)
    {
        this.weight = weigth;
        resetResultsAccumulation();
    }

    public void resetResultsAccumulation() {
        changesRegister = new ArrayList<>();
        this.bestSwarmValue = Double.POSITIVE_INFINITY;
    }

    public double getWeight() {
        return weight;
    }

    public void registerValue(double yBest) {
        this.bestSwarmValue = Math.min(bestSwarmValue, yBest);
    }

    public void registerCurrentValue(Particle particle, double currentValue) {
        changesRegister.get(changesRegister.size() - 1).add(new Pair<>(particle, this.bestSwarmValue - currentValue));
    }

    public void registerIteration(int historySize) {
        while (!changesRegister.isEmpty() && changesRegister.size() > historySize) {
            changesRegister.remove(0);
        }
        if (changesRegister.size() > 0 && changesRegister.size() >= historySize) {
            weight = changesRegister
                    .stream()
                    .mapToDouble(item -> item
                            .stream()
                            .mapToDouble(pair -> Math.max(0, pair.getValue()))
                            .sum()
                    )
                    .sum()
                    /
                    changesRegister
                            .stream()
                            .mapToLong(item -> item
                                    .stream()
                                    .count()
                            )
                            .sum()
            ;
            if (Double.isNaN(weight) || Double.isInfinite(weight)) {
                weight = 0.0;
            }
        }
        changesRegister.add(new ArrayList<>());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    private final static BehaviourFactory[] factories = new BehaviourFactory[]{
            new BehaviourFactory() { //1
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new SLPSO2007Behaviour(weight, 0.64, 1.4, 1.4);
                }

                @Override
                public String getName() {
                    return "Std-Local-PSO";
                }

                @Override
                public String getShortName() {
                    return "PSO-L-2007";
                }
            },
            new BehaviourFactory() { //2
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new DEBest1BinBehaviour(weight, 0.9, 1.4); //bear in mind - scaling factor is multiplied by a random number
                }

                @Override
                public String getName() {
                    return "DE-best-1-bin";
                }

                @Override
                public String getShortName() {
                    return "DE";
                }
            },
            new BehaviourFactory() { //3
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new RandomSearchBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "Random-Search";
                }

                @Override
                public String getShortName() {
                    return "RS";
                }
            },
            new BehaviourFactory() { //4
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new PowellsBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "Powell's";
                }

                @Override

                public String getShortName() {
                    return "Pow";
                }
            },
            new BehaviourFactory() { //5
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new LinearModelBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "Local-Linear-Model";
                }

                @Override
                public String getShortName() {
                    return "LM";
                }
            },
            new BehaviourFactory() { //6
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new GlobalLinearModelBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "Global-Linear-Model";
                }

                @Override
                public String getShortName() {
                    return "GLM";
                }
            },
            new BehaviourFactory() { //7
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new UCBSamplingBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "UCB-Sampling-Behaviour";
                }

                @Override
                public String getShortName() {
                    return "UCB";
                }
            },
            new BehaviourFactory() { //8
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new JADEbBest1BinBehaviour(weight, 0.5, 0.5); //normal and cauchy distributions applied
                }

                @Override
                public String getName() {
                    return "JADEb-best-1-bin";
                }

                @Override
                public String getShortName() {
                    return "JADEb";
                }
            },
            new BehaviourFactory() { //9
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new TabooSearchBehaviour(weight, 1.0); //number of multipier of dim + 1 tryouts per one successful choice
                }

                @Override
                public String getName() {
                    return "Taboo-Search";
                }

                @Override
                public String getShortName() {
                    return "Taboo";
                }
            },
            new BehaviourFactory() { //10
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new UCTSamplingBehaviour(weight, 0.7);
                }

                @Override
                public String getName() {
                    return "UCT";
                }

                @Override
                public String getShortName() {
                    return "UCT";
                }
            },
            new BehaviourFactory() { //11
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new UCTSamplingModelBehaviour(weight, 0.7);
                }

                @Override
                public String getName() {
                    return "Modeled-UCT";
                }

                @Override
                public String getShortName() {
                    return "MUCT";
                }
            },

            new BehaviourFactory() { //12
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new UCTSamplingUnboundedModelBehaviour(weight, 0.7);
                }

                @Override
                public String getName() {
                    return "Function-Bounded-Modeled-UCT";
                }

                @Override
                public String getShortName() {
                    return "FBMUCT";
                }
            },
            new BehaviourFactory() { //13
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new EABehaviour(weight, 0.8, 1.0);
                }

                @Override
                public String getName() {
                    return "Evoluationary-Algorithm";
                }

                @Override
                public String getShortName() {
                    return "EA";
                }
            },

            new BehaviourFactory() { //14
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new PolynomialModelBehaviour(weight);
                }

                @Override
                public String getName() {
                    return "Polynomial-Model";
                }

                @Override
                public String getShortName() {
                    return "PM";
                }
            },
            new BehaviourFactory() { //15
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new SGPSO2007Behaviour(weight, 0.64, 1.4, 1.4);
                }

                @Override
                public String getName() {
                    return "Std-Global-PSO";
                }

                @Override
                public String getShortName() {
                    return "PSO-G-2007";
                }
            },
            new BehaviourFactory() { //16
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new SLPSO2011Behaviour(weight, 0.64, 1.4, 1.4);
                }

                @Override
                public String getName() {
                    return "Std-2011-Local-PSO";
                }

                @Override
                public String getShortName() {
                    return "PSO-L-2011";
                }
            },
            new BehaviourFactory() { //17
                @Override
                public Behaviour getBehaviour(double weight) {
                    return new SGPSO2011Behaviour(weight, 0.64, 1.4, 1.4);
                }

                @Override
                public String getName() {
                    return "Std-2011-Global-PSO";
                }

                @Override
                public String getShortName() {
                    return "PSO-G-2011";
                }
            },

    };

    public static List<Behaviour> getBehaviours(int[] setup) {
        List<Behaviour> behaviours = new ArrayList<>();
        for (int i = 0; i < setup.length; ++i) {
            if (i > factories.length - 1) {
                throw new IllegalArgumentException("Non-existing behaviour selected");
            }
            if (setup[i] > 0) {
                behaviours.add(factories[i].getBehaviour(setup[i]));
            }
        }
        if (behaviours.size() <= 0) {
            throw new IllegalArgumentException("No behaviours configured");
        }
        return behaviours;
    }

    public static int[] setupFromDictionary(Map<String,Integer> poolDictionary) {
        int[] setup = new int[factories.length];
        for (int factIdx = 0; factIdx < factories.length; ++factIdx) {
            if (poolDictionary.containsKey(factories[factIdx].getShortName())) {
                setup[factIdx] = poolDictionary.get(factories[factIdx].getShortName());
            }
        }
        return setup;
    }

    public static String toString(int[] setup) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < setup.length; ++i) {
            if (i > factories.length - 1) {
                throw new IllegalArgumentException("Non-existing behaviour selected");
            }
            if (setup[i] != 0) {
                if (sb.length() != 0)
                    sb.append(",");
                sb.append(factories[i].getShortName());
                sb.append(":");
                sb.append(setup[i]);
            }
        }
        return sb.toString();
    }

    public static String usageString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < factories.length; ++i) {
            if (sb.length() != 0)
                sb.append(" ");
            sb.append(factories[i].getName());
        }
        return sb.toString();
    }

    public static String getId(int[] setup) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < setup.length; ++i) {
            if (i > factories.length - 1) {
                throw new IllegalArgumentException("Non-existing behaviour selected");
            }
            if (setup[i] != 0) {
                sb.append(factories[i].getShortName().charAt(0));
            }
        }
        for (int i = 0; i < setup.length; ++i) {
            if (i > factories.length - 1) {
                throw new IllegalArgumentException("Non-existing behaviour selected");
            }
            if (setup[i] != 0) {
                if (sb.length() != 0)
                    sb.append("_");
                sb.append(setup[i]);
            }
        }
        return sb.toString();
    }

    public abstract double[] computeVelocity(Particle particle);

}



package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

public class NullRegisteredOptimaLogger implements RegisteredOptimaLogger {
    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {

    }

    @Override
    public void registerIteration() {

    }

    @Override
    public void finalize() {

    }

    @Override
    public void registerSearchBounds(Bounds searchBounds) {

    }

    @Override
    public void registerInitializationMethod(int method) {

    }

    @Override
    public void registerOptimum(Sample sample) {

    }
}

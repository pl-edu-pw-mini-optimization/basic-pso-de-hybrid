package pl.edu.pw.mini.optimization.core.popsize;

public class JadeDimensionSizeMultiplier extends PopulationSizeAdjuster {
    private final int multiplier;
    private final static int DEFAULT_MULTIPLIER = 5;

    public JadeDimensionSizeMultiplier() {
        this(DEFAULT_MULTIPLIER);
    }

    public JadeDimensionSizeMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return multiplier + "D";
    }

    @Override
    public int getSize(int dimension, int budgetSizeRank, int behavioursCount) {
        return multiplier * dimension;
    }
}

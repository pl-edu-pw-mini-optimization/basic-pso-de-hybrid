package pl.edu.pw.mini.optimization.core.behaviour.pso;

import pl.edu.pw.mini.optimization.core.Particle;

import java.util.List;

public class SLPSO2011Behaviour extends StandardPSO2011Behaviour {
    public SLPSO2011Behaviour(double weight, double omega, double c1, double c2) {
        super(weight, omega, c1, c2);
    }

    @Override
    protected double[] getBestLocation(Particle particle, List<Particle> particles) {
        return this.getBestLocalRingLocation(particle, particles);
    }
}

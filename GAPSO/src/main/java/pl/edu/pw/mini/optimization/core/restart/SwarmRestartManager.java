package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;

public abstract class SwarmRestartManager {
    public abstract void registerNewIteration();
    public abstract void registerImprovement();
    public abstract void registerParticle(Particle particle);
    public abstract boolean shouldBeRestarted();
    @Override
    public abstract String toString();
}

package pl.edu.pw.mini.optimization.logging;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

public interface RegisteredOptimaLogger extends BaseLogger {
    void registerSearchBounds(Bounds searchBounds);
    void registerInitializationMethod(int method);
    void registerOptimum(Sample sample);
}

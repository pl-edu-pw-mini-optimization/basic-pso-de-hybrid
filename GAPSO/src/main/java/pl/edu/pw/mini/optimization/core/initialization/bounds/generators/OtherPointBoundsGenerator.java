package pl.edu.pw.mini.optimization.core.initialization.bounds.generators;

import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

public class OtherPointBoundsGenerator extends BoundsGenerator {

    private final Sample referencePoint;
    private final double fraction;

    public OtherPointBoundsGenerator(Sample referencePoint, double fraction) {
        this.referencePoint = referencePoint;
        this.fraction = fraction;
    }

    @Override
    public double[][] getBounds(Sample origin) {
        double[][] bounds = new double[2][];
        bounds[0] = new double[referencePoint.getX().length];
        bounds[1] = new double[referencePoint.getX().length];
        for (int d = 0; d < referencePoint.getX().length; ++d) {
            bounds[0][d] = Math.min(referencePoint.getX()[d],origin.getX()[d]);
            bounds[1][d] = Math.max(referencePoint.getX()[d],origin.getX()[d]);
        }
        return bounds;
    }

    @Override
    public double[][] generateSearchBounds(Sample origin) {
        double[][] newBounds;
        newBounds = new double[2][];
        newBounds[0] = new double[origin.getX().length];
        newBounds[1] = new double[origin.getX().length];
        for (int d = 0; d < origin.getX().length; ++d) {
            double avgDLocation = (origin.getX()[d] +
                    referencePoint.getX()[d]) / 2.0;
            double difference = Math.abs((origin.getX()[d] -
                    referencePoint.getX()[d]));
            newBounds[0][d] = avgDLocation - (difference / 2.0) * fraction * Generator.RANDOM.nextDouble();
            newBounds[1][d] = avgDLocation + (difference / 2.0) * fraction * Generator.RANDOM.nextDouble();
        }
        return newBounds;
    }

}

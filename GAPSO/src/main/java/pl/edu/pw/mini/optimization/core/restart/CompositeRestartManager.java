package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;

import java.util.List;
import java.util.stream.Collectors;

public abstract class CompositeRestartManager extends SwarmRestartManager {
    protected final List<SwarmRestartManager> restartManagers;

    public CompositeRestartManager(List<SwarmRestartManager> restartManagers) {
        this.restartManagers = restartManagers;
    }

    @Override
    public void registerNewIteration() {
        restartManagers.forEach(rm -> rm.registerNewIteration());
    }

    @Override
    public void registerImprovement() {
        restartManagers.forEach(rm -> rm.registerImprovement());
    }

    @Override
    public void registerParticle(Particle particle) {
        restartManagers.forEach(rm -> rm.registerParticle(particle));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('-');
        List<String> rms = restartManagers
                .stream()
                .map(rm -> rm.toString())
                .collect(Collectors.toList());
        sb.append(String.join(getSeparToken(), rms));
        sb.append('-');
        return sb.toString();
    }

    protected abstract String getSeparToken();
}

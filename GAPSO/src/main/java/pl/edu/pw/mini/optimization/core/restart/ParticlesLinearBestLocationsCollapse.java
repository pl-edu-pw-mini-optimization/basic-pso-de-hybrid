package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.Arrays;

public class ParticlesLinearBestLocationsCollapse extends ParticlesRestartManager {

    private final double epsDiameter;
    private final int dimension;

    public ParticlesLinearBestLocationsCollapse(double epsDiameter, int dimension) {
        this.epsDiameter = epsDiameter;
        this.dimension = dimension;
    }

    @Override
    public boolean shouldBeRestarted() {
        if (particles.size() < 2) {
            return false;
        }
        double[] maxBound = new double[dimension];
        Arrays.fill(maxBound, Double.NEGATIVE_INFINITY);
        double[] minBound = new double[dimension];
        Arrays.fill(minBound, Double.POSITIVE_INFINITY);
        for (Particle particle: particles) {
            double[] xBest = particle.getXBest();
            for (int d = 0; d < dimension; ++d) {
                maxBound[d] = Math.max(maxBound[d], xBest[d]);
                minBound[d] = Math.min(minBound[d], xBest[d]);
            }
        }

        double totalAverageSpread = 0.0;
        for (int d = 0; d < dimension; ++d) {
            totalAverageSpread += (maxBound[d] - minBound[d]) / dimension;
        }
        Loggers.SWARM_LOGGER.registerVariation(totalAverageSpread);
        return (totalAverageSpread < epsDiameter);
    }

    @Override
    public String toString() {
        return "LCollapsed";
    }
}

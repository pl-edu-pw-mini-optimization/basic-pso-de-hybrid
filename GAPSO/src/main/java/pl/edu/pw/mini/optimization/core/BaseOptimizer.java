package pl.edu.pw.mini.optimization.core;

import pl.edu.pw.mini.optimization.OptimizationAlgorithm;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;

import java.util.Arrays;

public abstract class BaseOptimizer implements OptimizationAlgorithm {
    protected final double[] solution;
    protected double bestValue;
    protected SamplesGatherer qualityFunction;

    public BaseOptimizer(SamplesGatherer qualityFunction) {
        this.bestValue = Double.MAX_VALUE;
        this.solution = new double[qualityFunction.getDimension()];
        this.qualityFunction = qualityFunction;
    }

    protected void updateOptimumEstimationWithCandidateSolution(double candidateValue, double[] candidateLocation) {
        bestValue = candidateValue;
        System.arraycopy(candidateLocation, 0, solution, 0, candidateLocation.length);
    }

    @Override
    public void clearMemory() {
        getSamplesIndex().clearIndex();
    }

    @Override
    public RTree getSamplesIndex() {
        return qualityFunction.getIndex();
    }

    @Override
    public double getOptimumValue() {
        return bestValue;
    }

}

package pl.edu.pw.mini.optimization.core.sampling.lm;

public class CredibleValue {
    private double value;
    private BaseModel squareModel;

    public CredibleValue(double value, BaseModel squareModel) {
        this.value = value;
        this.squareModel = squareModel;
    }

    public double getValue() {
        return this.value;
    }

    public double getRegressionSE() {
        return this.squareModel.getRegressionSE();
    }

    public BaseModel getModel() {
        return this.squareModel;
    }
}


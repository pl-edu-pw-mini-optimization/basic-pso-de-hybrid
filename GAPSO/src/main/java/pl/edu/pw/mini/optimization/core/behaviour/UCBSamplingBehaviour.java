package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

//TODO: I think that I should compute the minValue
//also size should be gathered
//no move - capacity not completed
//UCT should be done and not UCB
public class UCBSamplingBehaviour extends RandomSearchBehaviour {
    public UCBSamplingBehaviour(double weight) {
        super(weight);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        List<Sample> data = particle.getFunction().getHighLevelSamples();
        if (data.isEmpty())
            return super.computeVelocity(particle);
        double[] newV1 = goToAnySingleSolutionCluster(particle, data);
        if (newV1 != null) return newV1;

        double minValue = data
                .stream()
                .mapToDouble(item -> item.getValue())
                .min()
                .getAsDouble();
        double maxValue = data
                .stream()
                .mapToDouble(item -> item.getValue())
                .max()
                .getAsDouble();
        long totalCount = data
                .stream()
                .mapToLong(item -> item.getWeight())
                .sum();
        Sample highestRankingSample = data
                .stream()
                .sorted(new Comparator<Sample>() {
                    @Override
                    public int compare(Sample o1, Sample o2) {
                        return Double.compare(
                                //reversed order - cause largest first
                                getUCBScore(o2, maxValue, minValue, totalCount),
                                getUCBScore(o1, maxValue, minValue, totalCount));
                    }
                })
        .findFirst()
        .get();
        return getToDisturbedMagnetPoint(particle, highestRankingSample.getX());
    }

    private double getUCBScore(Sample sample, double maxValue, double minValue, long totalCount) {
        return ((sample.getValue() - maxValue) * (minValue - maxValue)) / sample.getWeight()
        + 0.1 * Math.sqrt(Math.log(totalCount) / sample.getWeight());
    }

    private double[] goToAnySingleSolutionCluster(Particle particle, List<Sample> data) {
        Optional<Sample> nonTested = data
                .stream()
                .filter(item -> item.getWeight() == 1)
                .findAny();
        if (nonTested.isPresent()) {
            double[] magnetPoint = nonTested.get().getX();
            return getToDisturbedMagnetPoint(particle, magnetPoint);
        }
        return null;
    }

    private double[] getToDisturbedMagnetPoint(Particle particle, double[] magnetPoint) {
        double[] newV = new double[particle.getV().length];
        for (int i = 0; i < newV.length; ++i) {
            newV[i] = -particle.getX()[i]
                    + magnetPoint[i] + Generator.RANDOM.nextGaussian();
        }
        return newV;
    }

}

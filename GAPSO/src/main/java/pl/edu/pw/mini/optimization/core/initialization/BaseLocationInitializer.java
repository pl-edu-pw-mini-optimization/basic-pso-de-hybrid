package pl.edu.pw.mini.optimization.core.initialization;

import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.Arrays;

public abstract class BaseLocationInitializer {
    protected final double FUNCTION_VALUE_TOLERANCE = 1e-8;
    protected final Function function;
    protected Bounds bounds;
    protected final int dimension;

    BaseLocationInitializer(Function function) {
        this.function = function;
        this.dimension = function.getDimension();
        resetOriginalFunctionBounds(function);
    }

    protected void resetOriginalFunctionBounds(Function function) {
        this.bounds = new Bounds(function.getBounds().getLower(), function.getBounds().getUpper());
    }

    protected double[] getRandomWithinFunctionBounds() {
        return Generator.generateRandomWithinBounds(bounds, dimension);
    }

    public abstract double[] getNext();
    public abstract void resetState();
    public abstract boolean isActive();

    public void shrinkBoundsWithinBounds(double[][] newBounds) {
        bounds.shrinkBoundsWithinBounds(newBounds);
    }
}

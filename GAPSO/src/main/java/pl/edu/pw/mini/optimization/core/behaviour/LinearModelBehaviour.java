package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.List;

public class LinearModelBehaviour extends RandomSearchBehaviour {

    public LinearModelBehaviour(double weight) {
        super(weight);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        int dim = particle.getFunction().getDimension();
        List<Sample> data = particle.getFunction().getNearestSamples(
                (particle.getFunction().isAveraging()) ?
                        (particle.getFunction().getAveragingSampleSize()) :
                        (particle.getFunction().getSquareSampleSize()),
                particle.getXBest());
        return getVelocityToBoundedModelMinimum(particle, dim, data);
    }

    protected double[] getVelocityToBoundedModelMinimum(Particle particle, int dim, List<Sample> data) {
        if (data.size() < (particle.getFunction().getMinSquareSampleSize())) {
            return super.computeVelocity(particle);
        } else {
            for (Sample dataSample : data) {
                Loggers.MOVES_LOGGER.registerBaseSample(dataSample.getX(), "DataPoint");
            }
            //TODO what about testing current bounds?
            Bounds functionBounds = particle.getFunction().getBounds();
            SquareModel sm = new SquareModel(dim, data);

            double[] newVelocity = null;
            try {
                newVelocity = sm.getBoundedParabolaPeak(functionBounds);

            } catch (SingularMatrixException ex) {
                return super.computeVelocity(particle);
            }
            for (int i = 0; i < dim; i++) {
                newVelocity[i] -= particle.getX()[i];
            }
            return newVelocity;
        }
    }
}

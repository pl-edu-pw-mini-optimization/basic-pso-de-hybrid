package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.util.List;

//TODO: algorithm should dive in to get values from multiple smaller samples
//The idea:
//Traverse the whole tree getting Clusters with more than K samples inside,
//but less then CAPACITY?
public class GlobalLinearModelBehaviour extends LinearModelBehaviour {
    public GlobalLinearModelBehaviour(double weight) {
        super(weight);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        int dim = particle.getFunction().getDimension();
        //TODO: How to check nicely if it is samples gatherer?
        List<Sample> data = particle.getFunction().getHighLevelSamples();
        return getVelocityToBoundedModelMinimum(particle, dim, data);
    }
}

package pl.edu.pw.mini.optimization.core;

import pl.edu.pw.mini.optimization.core.popsize.PopulationSizeAdjuster;
import pl.edu.pw.mini.optimization.core.restart.SwarmRestartManager;

import java.util.Map;

public interface GPSOConfigurator {
    int[] getAlgorithmsInitialPollList() throws NumberFormatException;
    Map<String,Integer> getAlgorithmsDictionaryPollList();
    PopulationSizeAdjuster getPopulationSizeAdjuster();
    SwarmRestartManager getSwarmRestartManager(int dimension);
    int getBudgetMultiplier();
    boolean isTabuInitializerToBeUsed();
    double getModelEstimationProbability();

    double getExplorationStrategyWeightRandomPointExploration();
    double getExplorationStrategyWeightBestPointExploration();
    double getExplorationStrategyWeightBestPointExploitation();

    double getMaxBestNeighbourhood();
    double getToBoundaryMaxFraction();
    double getBetweenOptimaMaxFraction();

    int getSquareInitializerDelay(int dimension);

    int getSamplingAveragingTime();

    double getExplorationStrategyWeightDifferencePointExploration();

    int getRestartTime();

    int getAdaptationHistoryLength();

    boolean resetAdaptationWeightAtRestart();

    double getInitialExplorationAreaRatio();

    double getExplorationStrategyWeightCompleteBoundsReset();

    boolean adaptInitializationStrategy();

    boolean useFunctionCacheToCheckForDuplicates();

    int getMaxSamplesCount();

    String[] getInitializersSequence();

    double getExplorationStrategyWeightRoulette();

    double getExplorationStrategyWeightValueBasedHeuristic();

    double getExplorationStrategyWeightModelBasedHeuristic();

    double getExplorationStrategyWeightEASwapping();

    boolean isEnableBehaviorsMixing();

    String toString();
}

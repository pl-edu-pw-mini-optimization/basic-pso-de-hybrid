package pl.edu.pw.mini.optimization.core.initialization;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.LinearModel;

import java.util.List;

public class LinearSolverLocationInitializer extends SampledLocationInitializer {

    private boolean fired;

    LinearSolverLocationInitializer(SamplesGatherer sampledFunction) {
        super(sampledFunction);
        fired = false;
    }

    protected double[] getBestLinearSolutionBoundkWithRandomFallback() {
        List<Sample> data = sampledFunction.getNearestSamples(sampledFunction.getSamplesCount(),
                new double[dimension]);
        if (data.size() >= dimension + 1) {
            try {
                LinearModel lm = new LinearModel(dimension, data);
                double[] x = lm.getBestBound(bounds);
                fired = true;
                return x;
            } catch (SingularMatrixException ex) {
                return getRandomWithinFunctionBounds();
            }
        }
        return getRandomWithinFunctionBounds();
    }

    @Override
    public double[] getNext() {
        return getBestLinearSolutionBoundkWithRandomFallback();
    }

    @Override
    public boolean isActive() {
        return (!fired && (sampledFunction.getSamplesCount() == dimension + 1));
    }
}

package pl.edu.pw.mini.optimization.logging;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static pl.edu.pw.mini.optimization.utils.Sanitizer.sanitizeFilename;

public abstract class BaseCSVLogger implements BaseLogger {
    protected static final long LOG_MINUTES_FREQUENCY = 5;
    protected final long experimentStart;
    protected int run;
    protected Date lastLog;
    protected int iteration;

    protected boolean active;
    protected String configuration;
    protected String functionName;
    protected int dimension;
    private String logfileName;

    protected BaseCSVLogger() {
        experimentStart = (new Date()).getTime();
        lastLog = new Date();
        active = false;
        run = 0;
    }

    protected void logLineToLogFile(StringBuilder sb) {
        try {
            Files.write(
                    Paths.get(logfileName),
                    sb.toString().getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            //
            System.err.println("Unable to log to file");
            System.err.println(sb.toString());
        }
    }

    @Override
    public void registerRun(String configuration, String functionName, int functionDimension) {
        logEntry();
        this.active = true;
        this.configuration = sanitizeFilename(configuration);
        this.functionName = functionName;
        this.dimension = functionDimension;
        this.iteration = -1;
        this.run++;
        String newlogFileName = logType() + "-" + configuration + '-' + experimentStart + "-" + functionName + ".csv";
        if (!newlogFileName.equals(this.logfileName)) {
            this.logfileName = newlogFileName;
            generateHeader();
        }
    }


    @Override
    public void registerIteration() {
        this.iteration++;
        long diffInMillies = Math.abs((new Date()).getTime() - lastLog.getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
        if (diff >= LOG_MINUTES_FREQUENCY) {
            logEntry();
        }
    }

    @Override
    public void finalize() {
        logEntry();
    }

    protected abstract void logEntry();

    protected abstract String logType();

    protected abstract void generateHeader();
}

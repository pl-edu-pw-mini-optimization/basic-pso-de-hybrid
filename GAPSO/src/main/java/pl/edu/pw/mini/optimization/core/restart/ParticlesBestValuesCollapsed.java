package pl.edu.pw.mini.optimization.core.restart;

import pl.edu.pw.mini.optimization.core.Particle;

import java.util.List;
import java.util.stream.DoubleStream;


public class ParticlesBestValuesCollapsed extends ParticlesRestartManager {
    private double tolerance;

    public ParticlesBestValuesCollapsed(double tolerance) {
        this.tolerance = tolerance;
    }

    @Override
    public boolean shouldBeRestarted() {
        if (particles.size() < 2) {
            return false;
        }

        return (particles
                .stream()
                .mapToDouble(item -> item.getYBest())
                .max().getAsDouble() -
                particles
                        .stream()
                        .mapToDouble(item -> item.getYBest())
                        .min().getAsDouble() < tolerance);
    }

    @Override
    public String toString() {
        return "Plateau";
    }
}

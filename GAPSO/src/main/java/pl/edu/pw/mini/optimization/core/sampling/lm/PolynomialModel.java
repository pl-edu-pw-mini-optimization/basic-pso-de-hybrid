package pl.edu.pw.mini.optimization.core.sampling.lm;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.analysis.solvers.LaguerreSolver;
import org.apache.commons.math3.analysis.solvers.NewtonRaphsonSolver;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolynomialModel extends BaseModel {
    private OLSMultipleLinearRegression olslm;
    private int dim;
    private int polynomialDegree;
    private double[] ba;
    private double regressionSE = Double.NaN;
    private double rSquared = Double.NaN;
    private List<PredictedSample> tests = new ArrayList<>();
    private double[] dataBounds;

    private class PredictedSample {
        private double[] x;
        private double expected;
        private double actual;

        public PredictedSample(double[] x, double expected, double actual) {
            this.x = x;
            this.expected = expected;
            this.actual = actual;
        }

        public double getError() {
            return Math.abs(expected - actual);
        }
    }

    public PolynomialModel(Particle particle, int dim, int polynomialDegree, List<Sample> data) {
        this.polynomialDegree = polynomialDegree;
        this.olslm = new OLSMultipleLinearRegression();
        this.dim = dim;
        int samplesCount = data.size();
        double[] y = new double[samplesCount];
        double[][] x = new double[samplesCount][];
        this.dataBounds = new double[2];

        // Assing data bounds
        dataBounds = new double[2];
        this.dataBounds[0] = Double.POSITIVE_INFINITY;
        this.dataBounds[1]= Double.NEGATIVE_INFINITY;

        for (int i = 0; i < samplesCount; ++i) {
            double xj = data.get(i).getX()[dim];
            if(xj < this.dataBounds[0]) {
                this.dataBounds[0] = xj;
            }
            if(xj > this.dataBounds[1]) {
                this.dataBounds[1]= xj;
            }
        }
        // Create linear model
        for (int i = 0; i < samplesCount; ++i) {
            y[i] = data.get(i).getValue();
            x[i] = new double[this.polynomialDegree];
            double xj = data.get(i).getX()[dim];
            x[i][0] = xj;
            x[i][1] = xj * xj;
            x[i][2] = xj * xj * xj;
            x[i][3] = xj * xj * xj * xj;
//            x[i][4] = xj * xj * xj * xj * xj;
//            x[i][5] = xj * xj * xj * xj * xj * xj;
//            x[i][6] = xj * xj * xj * xj * xj * xj * xj;
//            x[i][7] = xj * xj * xj * xj * xj * xj * xj * xj;
        }
        olslm.newSampleData(y, x);
    }

    public double getRSquared() {
        return rSquared;
    }

    public double getRegressionSE() {
        return this.regressionSE;
    }

    public double getBoundedPolynomianMinimum(Bounds functionBounds) throws SingularMatrixException {
        double boundedPeak;//TODO: check for singularity
        boundedPeak = 0;
        // Calculate regression coefficients
        createModelParameters();
        double[] coeff = new double[this.polynomialDegree + 1];
        for (int i = 0; i < this.polynomialDegree+1; i++) {
            coeff[i] = ba[i];
        }

        // Polynomial function
        PolynomialFunction polynomial = new PolynomialFunction(coeff);

        // Polynomial sampling
        double x, y;
        double xOptim = this.dataBounds[0];
        double yOptim = polynomial.value(xOptim);
        double delta = (this.dataBounds[1] - this.dataBounds[0]) / 1000;
        //double jOptim = 0;
        for (int i = 0; i < 1000; i++) {
            x = this.dataBounds[0] + delta * i;
            y = polynomial.value(x);
            if (y < yOptim) {
                yOptim = y;
                xOptim = x;
                boundedPeak = xOptim;
            }
        }

       /* // Calculate polynomial minimum for each dimension
        for (int i = 0; i < dim; i++) {
            // Rewrite per-dimension coefficients to another array
            double[] coeff = new double[this.polynomialDegree + 1];
            for (int j = 0; j < this.polynomialDegree; j++) {
                coeff[1 + j] = ba[j * dim + i];
            }
            // Add 0 as intercept
            coeff[0] = 0;
            PolynomialFunction polynomial = new PolynomialFunction(coeff);
            //  first derivative of polynomial to find extremas
            PolynomialFunction polynomialD1 = polynomial.polynomialDerivative();
            // Find zeros
            LaguerreSolver laguerreSolver = new LaguerreSolver(0.1, 0.1, 0.1);
            //NewtonRaphsonSolver newtonRaphsonSolver = new NewtonRaphsonSolver();
            //System.out.println(Arrays.toString(polynomialD1.getCoefficients()));
            Complex[] roots = null;
            //double[] roots = new double[5];
            try {
                roots = laguerreSolver.solveAllComplex(polynomialD1.getCoefficients(), 0);
                //roots = newtonRaphsonSolver.solve(1000, polynomial, -5,5);
            } catch (Exception e) {
                System.out.print("Exception: ");
                System.out.println(e.getMessage());
                roots = new Complex[0];
            }

            double leftBound = functionBounds[0][i];
            double rightBound = functionBounds[1][i];

            // Check how many real roots inside bounds
            int realRootsCounter = 0;
            for (int j = 0; j < roots.length; j++) {
                if (roots[j].getImaginary() == 0 & roots[j].getReal() >= leftBound & roots[j].getReal() <= rightBound)
                    realRootsCounter++;
            }

            // create real roots inside bounds array
            double[] realRoots = new double[realRootsCounter];
            int j2 = 0;
            for (int j = 0; j < roots.length; j++) {
                if (roots[j].getImaginary() == 0 & roots[j].getReal() >= leftBound & roots[j].getReal() <= rightBound) {
                    realRoots[j2] = roots[j].getReal();
                    j2++;
                }
            }

            // Create values in extrema
            double[] extrema = new double[realRootsCounter];
            for (int j = 0; j < realRootsCounter; j++) {
                extrema[j] = polynomial.value(realRoots[j]);
            }

            // Create values in function bounds
            double leftValue = polynomial.value(leftBound);
            double rightValue = polynomial.value(rightBound);

            // Find minimum extremum
            double minExtremum = Double.POSITIVE_INFINITY;
            int minExtremumIdx = -1;
            for (int j = 0; j < realRootsCounter; j++) {
                if (extrema[j] < minExtremum) {
                    minExtremum = extrema[j];
                    minExtremumIdx = j;
                }
                extrema[j] = polynomial.value(realRoots[j]);
            }

            double minExtremumArg = Double.POSITIVE_INFINITY;

            // If at least one extremum exist
            if (realRootsCounter > 0) {
                minExtremumArg = realRoots[minExtremumIdx];
            }

            if (minExtremum <= leftValue & minExtremumArg <= rightValue) {
                boundedPeak[i] = minExtremumArg;
            } else if (leftValue <= rightValue) {
                boundedPeak[i] = leftBound;
            } else {
                boundedPeak[i] = rightBound;
            }
            //int test = 5;
        }


        // Calculate polynomial minimum for each dimension
        System.out.println(for (int i = 0; i < dim; i++) {
            // Rewrite per-dimension coefficients to another array
            double[] coeff = new double[this.polynomialDegree + 1];
            for (int j = 0; j < this.polynomialDegree; j++) {
                coeff[1 + j] = ba[j * dim + i];
            }
            // Add 0 as intercept
            coeff[0] = 0;
            PolynomialFunction polynomial = new PolynomialFunction(coeff);

            // Trivial polynomial sampling
            double x;
            double xOptim = Double.POSITIVE_INFINITY;
            double delta = (this.dataBounds[1][i] - this.dataBounds[0][i]) / 1000;
            //double jOptim = 0;
            for (int j = 0; j < 1000; j++) {
                x = this.dataBounds[0][i] + delta * j;
                if (x < xOptim) {
                    xOptim = x;
                    boundedPeak[i] = xOptim;
                    //jOptim = j;
                }
            }
            //jOptim);

        }*/
        return boundedPeak;
    }

    private void createModelParameters() throws SingularMatrixException {
        ba = olslm.estimateRegressionParameters();
        regressionSE = olslm.estimateRegressionStandardError();
        rSquared = olslm.calculateRSquared();
    }

    private double getBoundedCoordinate(double[][] bounds, double value, int dimension) {
        if (value < bounds[0][dimension])
            return bounds[0][dimension];
        if (value > bounds[1][dimension])
            return bounds[1][dimension];
        return value;
    }


    public CredibleValue evaluateInModel(double[] x) throws SingularMatrixException {
        if (ba == null) {
            createModelParameters();
        }
        double value = ba[0];
        for (int i = 0; i < x.length; ++i) {
            value += ba[i + 1 + dim] * x[i] * x[i];
            value += ba[i + 1] * x[i];
        }
        CredibleValue cv = new CredibleValue(value, this);
        return cv;
    }

    public void storeTestResult(double[] x, double expected, double actual) {
        this.tests.add(new PredictedSample(x, expected, actual));
    }

    public int getTestsCount() {
        return this.tests.size();
    }

    public double getAverageTestsResult() {
        return this
                .tests
                .stream()
                .mapToDouble(item -> item.getError())
                .average()
                .getAsDouble();
    }

    private double linearCutValue(double a, double b, double x) {
        return a * x * x + b * x;
    }


}
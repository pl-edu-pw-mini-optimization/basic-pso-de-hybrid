package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.initialization.BaseLocationInitializer;

public class InitializerBehaviourWrapper extends RandomSearchBehaviour {

    private final BaseLocationInitializer initializer;

    public InitializerBehaviourWrapper(BaseLocationInitializer initializer, double weight) {
        super(weight);
        this.initializer = initializer;
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        if (initializer.isActive()) {
            double[] x = initializer.getNext();
            for (int dim = 0; dim < x.length; ++dim) {
                x[dim] -= particle.getX()[dim];
            }
        }
        return super.computeVelocity(particle);
    }

    @Override
    public String toString() {
        return initializer.getClass().getSimpleName();
    }
}

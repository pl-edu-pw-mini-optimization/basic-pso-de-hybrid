package pl.edu.pw.mini.optimization.core.initialization.bounds.generators;

import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.utils.Generator;

public class FunctionBoundaryBoundsGenerator extends BoundsGenerator {
    private final double[][] originalBounds;
    private final int side;
    private final int dimension;
    private final double fraction;

    public FunctionBoundaryBoundsGenerator(double[][] originalBounds, int side, int dimension, double fraction) {
        this.originalBounds = originalBounds;
        this.side = side;
        this.dimension = dimension;
        this.fraction = fraction;
    }

    @Override
    public double[][] getBounds(Sample origin) {
        double[][] bounds = new double[2][];
        bounds[0] = new double[originalBounds[0].length];
        bounds[1] = new double[originalBounds[1].length];
        for (int d = 0; d < originalBounds[0].length; ++d) {
            if (d != dimension) {
                bounds[0][d] = originalBounds[0][d];
                bounds[1][d] = originalBounds[1][d];
            } else {
                if (side == 0) {
                    bounds[0][d] = originalBounds[0][d];
                    bounds[1][d] = origin.getX()[d];
                } else {
                    bounds[0][d] = origin.getX()[d];
                    bounds[1][d] = originalBounds[1][d];
                }
            }
        }
        return bounds;
    }

    @Override
    public double[][] generateSearchBounds(Sample origin) {
        double[][] bounds = new double[2][];
        bounds[0] = new double[originalBounds[0].length];
        bounds[1] = new double[originalBounds[1].length];
        for (int d = 0; d < originalBounds[0].length; ++d) {
            if (d != dimension) {
                bounds[0][d] = originalBounds[0][d];
                bounds[1][d] = originalBounds[1][d];
            } else {
                if (side == 0) {
                    bounds[0][d] = originalBounds[0][d];
                    bounds[1][d] = originalBounds[0][d] + (origin.getX()[d] - originalBounds[0][d]) * fraction * Generator.RANDOM.nextDouble();
                } else {
                    bounds[0][d] = originalBounds[1][d] - (originalBounds[1][d] - origin.getX()[d]) * fraction * Generator.RANDOM.nextDouble();
                    bounds[1][d] = originalBounds[1][d];
                }
            }
        }
        return bounds;

    }
}

package pl.edu.pw.mini.optimization.core.behaviour;

import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.utils.NamedObject;

public class GridSearchPseudoBehaviour implements NamedObject {

    @Override
    public String toString() {
        return "GridSearch";
    }
}

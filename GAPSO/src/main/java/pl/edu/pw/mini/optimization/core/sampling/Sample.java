package pl.edu.pw.mini.optimization.core.sampling;

import java.util.List;

public abstract class Sample implements Comparable<Sample> {
    @Override
    public int compareTo(Sample o) {
        double[] thisSamplePoint = this.getX();
        double[] otherSamplePoint = o.getX();
        for (int i = 0; i < thisSamplePoint.length && i < otherSamplePoint.length; ++i) {
            if (thisSamplePoint[i] != otherSamplePoint[i])
                return Double.compare(thisSamplePoint[i], otherSamplePoint[i]);
        }
        return 0;
    }

    public abstract double[] getX();

    public abstract double getValue();

    public abstract double getRadius();

    public abstract long getWeight();

    public double getDistance(double[] point) {
        double distance = 0.0;
        double[] samplePoint = this.getX();
        for (int i = 0; i < point.length && i < samplePoint.length; ++i) {
            distance += (point[i] - samplePoint[i]) * (point[i] - samplePoint[i]);
        }
        return Math.sqrt(distance);
    }

    public boolean isSignificantlyDifferentLocation(double[] point, double tolerance) {
        double[] samplePoint = this.getX();
        for (int i = 0; i < point.length && i < samplePoint.length; ++i) {
            if (Math.abs(point[i] - samplePoint[i]) > tolerance) {
                return true;
            }
        }
        return false;
    }

    public double getDistanceInDimension(double[] point, int dim) {
        double distance = 0.0;
        double[] samplePoint = this.getX();
        for (int i = 0; i < point.length && i < samplePoint.length; ++i) {
            if(i != dim) {
                distance += (point[i] - samplePoint[i]) * (point[i] - samplePoint[i]);
            }
        }
        return Math.sqrt(distance);
    }

    public abstract List<DistancedSample> getKOrderedSamples(double[] point, int k);

}

package pl.edu.pw.mini.optimization.core.behaviour;

public abstract class BehaviourFactory {
    public abstract Behaviour getBehaviour(double weight);

    public abstract String getName();

    public abstract String getShortName();
}

package pl.edu.pw.mini.optimization.core;

import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.popsize.JadeDimensionSizeMultiplier;
import pl.edu.pw.mini.optimization.core.popsize.PopulationSizeAdjuster;
import pl.edu.pw.mini.optimization.core.restart.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Stream;

public class PropertiesGPSOConfigurator implements GPSOConfigurator {
    private boolean enableBehaviorsMixing;
    private int improvementRestart;
    private String swarmLocationsCollapseStr;
    private String stagnationBaseInterationsStr;
    private String populationSizeMultiplierStr;
    private String populationSizeAdjusterType;
    private static final String POPULATION_SIZE_ADJUSTER_JADE = "JADE";
    private String collapseAnalysisType;
    private static final String COLLAPSE_ANALYSIS_VARIANCE = "VARIANCE";
    private static final String COLLAPSE_ANALYSIS_LINEAR = "LINEAR";
    private String quadraticInitilizerDelay;
    private static final String QUADRATIC_INITILIZER_DIMENSION_DELAY = "DIM";
    private static final String QUADRATIC_INITILIZER_INFINITY_DELAY = "INF";
    private static final String QUADRATIC_INITILIZER_NO_DELAY = "NO";
    private String budgetMultiplierStr;
    private String behaviorsStr;
    private double explorationStrategyWeightRandomPointExploration;
    private double explorationStrategyWeightBestPointExploration;
    private double explorationStrategyWeightBestPointExploitation;
    private double explorationStrategyWeightDifferncePointExploration;
    private double explorationStrategyWeightCompleteBoundsReset;
    private boolean shouldInitializationStrategyBeAdapted;
    private double initialExplorationAreaRatio;

    private double maxBestNeighbourhood;
    private double toBoundaryMaxFraction;
    private double betweenOptimaMaxFraction;
    private int samplingAveragingTime;
    private int gapsoAdaptationHistoryLength;
    private boolean resetAdaptationAtReset;
    private boolean useCacheForDuplicates;
    private int samplesMemorySize;
    private String[] initializersSequence;
    private double explorationStrategyWeightRoulette;
    private double explorationStrategyWeightValueBasedHeuristic;
    private double explorationStrategyWeightModelBasedHeuristic;
    private double explorationStrategyWeightEASwapping;

    //gapso.iteations.batch=10

    public PropertiesGPSOConfigurator() {
        final Properties properties = new Properties();
        try {
            try {
                InputStream inputStream = Files.newInputStream(Paths.get("algorithm.properties"), StandardOpenOption.READ);
                properties.load(inputStream);
                inputStream.close();
            } catch (IOException e1) {
                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("algorithm.default.properties");
                properties.load(inputStream);
                inputStream.close();
            }
            behaviorsStr = properties.getProperty("gapso.behaviors");
            enableBehaviorsMixing = Boolean.parseBoolean(properties.getProperty("gapso.behaviors.mixing"));
            budgetMultiplierStr = properties.getProperty("gapso.budget");
            populationSizeAdjusterType = properties.getProperty("gapso.population.type");
            populationSizeMultiplierStr = properties.getProperty("gapso.population.multiplier");
            stagnationBaseInterationsStr = properties.getProperty("gapso.restarts.stagnation");
            swarmLocationsCollapseStr = properties.getProperty("gapso.restarts.collapse");
            collapseAnalysisType = properties.getProperty("gapso.restarts.collapse.type");
            quadraticInitilizerDelay = properties.getProperty("gapso.initializer.quadratic.delay.type");
            initializersSequence = properties
                    .getProperty("gapso.initializer.composite.sequence")
                    .split(",");
            if (initializersSequence.length == 1 && "".equals(initializersSequence[0])) {
                initializersSequence = new String[0];
            }

            improvementRestart = Integer.parseInt(stagnationBaseInterationsStr);
            gapsoAdaptationHistoryLength = Integer.parseInt(
                    properties.getProperty("gapso.history.length"));
            resetAdaptationAtReset = Boolean.parseBoolean(
                    properties.getProperty("gapso.reset.adaptation.at.reset"));

            explorationStrategyWeightRandomPointExploration =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.random.point.exploration"));
            explorationStrategyWeightBestPointExploration =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.best.point.exploration"));
            explorationStrategyWeightBestPointExploitation =
                Double.parseDouble(properties.getProperty("gapso.exploration.weight.best.point.exploitation"));
            explorationStrategyWeightDifferncePointExploration =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.difference.point.exploration"));
            explorationStrategyWeightCompleteBoundsReset =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.complete.bounds.reset"));
            explorationStrategyWeightRoulette =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.roulette"));
            explorationStrategyWeightValueBasedHeuristic =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.value.heuristic"));
            explorationStrategyWeightModelBasedHeuristic =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.model.heuristic"));
            explorationStrategyWeightEASwapping =
                    Double.parseDouble(properties.getProperty("gapso.exploration.weight.ea.swapping"));

            shouldInitializationStrategyBeAdapted =
                    Boolean.parseBoolean(properties.getProperty("gapso.exploration.adapt.exploration.strategy"));
            initialExplorationAreaRatio =
                    Double.parseDouble(properties.getProperty("gapso.exploration.initial.area.ratio"));

            maxBestNeighbourhood =
                Double.parseDouble(properties.getProperty("gapso.exploration.max.best.neighbourhood"));
            toBoundaryMaxFraction =
                Double.parseDouble(properties.getProperty("gapso.exploration.to.boundary.max.fraction"));
            betweenOptimaMaxFraction =
                Double.parseDouble(properties.getProperty("gapso.exploration.between.optima.max.fraction"));

            samplingAveragingTime =
                Integer.parseInt(properties.getProperty("gapso.sampling.averaging.time"));

            useCacheForDuplicates =
                    Boolean.parseBoolean(properties.getProperty("gapso.sampling.use.cache"));
            samplesMemorySize =
                    Integer.parseInt(properties.getProperty("gapso.samples.memory"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int[] getAlgorithmsInitialPollList() throws NumberFormatException {
            return Arrays.stream(
                    behaviorsStr
                            .split(","))
                    .mapToInt(item -> Integer.parseInt(item))
                    .toArray()
                    ;

    }

    @Override
    public Map<String, Integer> getAlgorithmsDictionaryPollList() {
        String[] entries =
                behaviorsStr
                        .split(",");
        Map<String, Integer> returnedMap = new TreeMap<>();
        for (int entryIdx = 0; entryIdx < entries.length; ++entryIdx) {
            String[] keyValue = entries[entryIdx].split(":");
            if (keyValue.length == 2 && !returnedMap.containsKey(keyValue[0])) {
                returnedMap.put(keyValue[0], Integer.parseInt(keyValue[1]));
            }
        }
        return returnedMap;
    }

    @Override
    public PopulationSizeAdjuster getPopulationSizeAdjuster() {
        if (POPULATION_SIZE_ADJUSTER_JADE.equals(populationSizeAdjusterType))
            return new JadeDimensionSizeMultiplier(Integer.parseInt(populationSizeMultiplierStr));
        return null;
    }

    @Override
    public SwarmRestartManager getSwarmRestartManager(int dimension) {

        double collapseRestart = Double.parseDouble(swarmLocationsCollapseStr);

        SwarmRestartManager locationsCollapseManager = null;
        if (COLLAPSE_ANALYSIS_VARIANCE.equals(collapseAnalysisType)) {
            locationsCollapseManager = new ParticlesBestLocationsCollapsed(collapseRestart, dimension);
        } else if (COLLAPSE_ANALYSIS_LINEAR.equals(collapseAnalysisType)) {
            locationsCollapseManager = new ParticlesLinearBestLocationsCollapse(collapseRestart, dimension);
        }
        ArrayList<SwarmRestartManager> restartManagers = new ArrayList<>();
        if (improvementRestart > 0) restartManagers.add(new NoGlobalImprovementRestartManager(improvementRestart + 2 * dimension));
        if (!Double.isInfinite(collapseRestart)) restartManagers.add(
                new OrCompositeRestartManager(
                        Arrays.asList(new SwarmRestartManager[]{
                                locationsCollapseManager,
                                new ParticlesBestValuesCollapsed(collapseRestart)
                                }
                        )
                )
        );
        return new AndCompositeRestartManager(restartManagers);
    }

    @Override
    public int getBudgetMultiplier() {
        return Integer.parseInt(budgetMultiplierStr);
    }

    @Override
    public boolean isTabuInitializerToBeUsed() {
        return false;
    }

    @Override
    public double getModelEstimationProbability() {
        return 0;
    }

    @Override
    public double getExplorationStrategyWeightRandomPointExploration() {
        return explorationStrategyWeightRandomPointExploration;
    }

    @Override
    public double getExplorationStrategyWeightBestPointExploration() {
        return explorationStrategyWeightBestPointExploration;
    }

    @Override
    public double getExplorationStrategyWeightBestPointExploitation() {
        return explorationStrategyWeightBestPointExploitation;
    }

    @Override
    public double getMaxBestNeighbourhood() {
        return maxBestNeighbourhood;
    }

    @Override
    public double getToBoundaryMaxFraction() {
        return toBoundaryMaxFraction;
    }

    @Override
    public double getBetweenOptimaMaxFraction() {
        return betweenOptimaMaxFraction;
    }

    @Override
    public int getSquareInitializerDelay(int dimension) {
        if (quadraticInitilizerDelay.equals(QUADRATIC_INITILIZER_DIMENSION_DELAY))
            return dimension;
        if (quadraticInitilizerDelay.equals(QUADRATIC_INITILIZER_INFINITY_DELAY))
            return Integer.MAX_VALUE;
        if (quadraticInitilizerDelay.equals(QUADRATIC_INITILIZER_NO_DELAY))
            return 0;
        throw new IllegalArgumentException("Unspecified " + quadraticInitilizerDelay + " initializer delay");
    }

    @Override
    public int getSamplingAveragingTime() {
        return samplingAveragingTime;
    }

    @Override
    public double getExplorationStrategyWeightDifferencePointExploration() {
        return explorationStrategyWeightDifferncePointExploration;
    }

    @Override
    public int getRestartTime() {
        return improvementRestart;
    }

    @Override
    public int getAdaptationHistoryLength() {
        return gapsoAdaptationHistoryLength;
    }

    @Override
    public boolean resetAdaptationWeightAtRestart() {
        return resetAdaptationAtReset;
    }

    @Override
    public double getInitialExplorationAreaRatio() {
        return initialExplorationAreaRatio;
    }

    @Override
    public double getExplorationStrategyWeightCompleteBoundsReset() {
        return explorationStrategyWeightCompleteBoundsReset;
    }

    @Override
    public boolean adaptInitializationStrategy() {
        return shouldInitializationStrategyBeAdapted;
    }

    @Override
    public boolean useFunctionCacheToCheckForDuplicates() {
        return useCacheForDuplicates;
    }

    @Override
    public int getMaxSamplesCount() {
        return samplesMemorySize;
    }

    @Override
    public String[] getInitializersSequence() {
        return initializersSequence;
    }

    @Override
    public double getExplorationStrategyWeightRoulette() {
        return explorationStrategyWeightRoulette;
    }

    @Override
    public double getExplorationStrategyWeightValueBasedHeuristic() {
        return explorationStrategyWeightValueBasedHeuristic;
    }

    @Override
    public double getExplorationStrategyWeightModelBasedHeuristic() {
        return explorationStrategyWeightModelBasedHeuristic;
    }

    @Override
    public double getExplorationStrategyWeightEASwapping() {
        return explorationStrategyWeightEASwapping;
    }


    @Override
    public boolean isEnableBehaviorsMixing() {
        return enableBehaviorsMixing;
    }

    @Override
    public String toString() {
        return  (this.adaptInitializationStrategy() ? "A" : "") +
                (this.getExplorationStrategyWeightBestPointExploitation() >= 0 ? "Bi" : "") +
                (this.getExplorationStrategyWeightBestPointExploration() >= 0 ? "Bo" : "") +
                (this.getExplorationStrategyWeightDifferencePointExploration() >= 0 ? "D" : "") +
                (this.getExplorationStrategyWeightRandomPointExploration() >= 0 ? "Rp" : "") +
                (this.getExplorationStrategyWeightRoulette() >= 0 ? "Rt" : "") +
                (this.getExplorationStrategyWeightCompleteBoundsReset() >= 0 ? "Rr" : "") +
                (this.getExplorationStrategyWeightModelBasedHeuristic() >= 0 ? "M" : "") +
                (this.getExplorationStrategyWeightValueBasedHeuristic() >= 0 ? "V" : "") +
                (this.getExplorationStrategyWeightEASwapping() >= 0 ? "Ea" : "");
    }
}

package pl.edu.pw.mini.optimization.core;

import pl.edu.pw.mini.optimization.core.behaviour.InitializerBehaviourWrapper;
import pl.edu.pw.mini.optimization.core.initialization.BaseLocationInitializer;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;
import pl.edu.pw.mini.optimization.utils.Generator;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Particle {
    private final SamplesGatherer _function;
    private final double[] globalBest;
    private List<RTree> tabooIndexes;
    private final List<Particle> _particles;
    private final int idx;
    private double[] x;
    private double[] xBest;
    private double[] v;
    private double y;
    private double yBest;
    private Behaviour lastBehaviour;

    public Particle(SamplesGatherer function, List<Particle> particles, BaseLocationInitializer locationInitializer, double[] bestSoFar) {
        this.globalBest = bestSoFar;
        this._function = function;
        this.tabooIndexes = tabooIndexes;
        int dim = _function.getDimension();
        this._particles = particles;
        this._particles.add(this);
        this.idx = particles.indexOf(this);
        xBest = new double[dim];
        yBest = Double.MAX_VALUE;
        lastBehaviour = new InitializerBehaviourWrapper(locationInitializer, 0.0);
        lastBehaviour.registerIteration(1);
        x = locationInitializer.getNext();
        evaluate();
    }

    public void initializeVelocity(List<Particle> particles) {
        v = new double[_function.getDimension()];
        double[] partnersX = particles.get(Generator.RANDOM.nextInt(particles.size())).getX();
        for (int i = 0; i < x.length; ++i) {
            v[i] = (partnersX[i] - x[i]) / 2.0;
        }
    }

    private double evaluate() {
        y = _function.getValue(x, yBest);
        if (lastBehaviour != null) {
            lastBehaviour.registerCurrentValue(this, y);
            Loggers.SAMPLES_LOGGER.registerSample(new SingleSample(x,y),lastBehaviour);
        }
        if (y < yBest) {
            xBest = Arrays.copyOf(x, x.length);
            yBest = y;
        }
        return y;
    }

    public void sampleNext(Behaviour behaviour) {
        Loggers.MOVES_LOGGER.registerNewMove();
        lastBehaviour = behaviour;
        v = behaviour.computeVelocity(this);
        boolean xAccepted = false;
        while (!xAccepted) {
            xAccepted = true;
            double[] tempX = Arrays.copyOf(x, x.length);
            for (int i = 0; i < v.length; ++i) {
                tempX[i] += v[i];
                if (tempX[i] < _function.getBounds().getLower()[i] || tempX[i] > _function.getBounds().getUpper()[i]) {
                    xAccepted = false;
                }
            }
            if (!xAccepted) {
                for (int i = 0; i < v.length; ++i) {
                    //TODO can use distance from bounds here
                    v[i] /= 2.0;
                }
            } else {
                x = Arrays.copyOf(tempX, tempX.length);
            }
        }
        Loggers.MOVES_LOGGER.registerEffectSample(x, lastBehaviour);
        evaluate();
    }

    public double[] getV() {
        return v;
    }

    public double[] getXBest() {
        return xBest;
    }

    public double[] getX() {
        return x;
    }

    public double getYBest() {
        return yBest;
    }

    public List<Particle> getParticles() {
        return _particles;
    }

    public SamplesGatherer getFunction() {
        return _function;
    }

    public void resetBest() {
        yBest = Double.MAX_VALUE;
        //TODO: lastBehaviour = null; - should be considered how to do this?
    }

    public void recomputeBest() {
        double newBestValue = _function.getValue(xBest);
        if (newBestValue < yBest) {
            yBest = newBestValue;
        }
    }

    public double[] getGlobalBestSoFar() {
        return globalBest;
    }
}

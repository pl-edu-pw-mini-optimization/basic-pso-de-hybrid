package pl.edu.pw.mini.optimization.utils;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation.Evaluation;
import pl.edu.pw.mini.optimization.core.sampling.rtree.evaluation.SamplesEvaluator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Generator {
    public static final RandomGenerator RANDOM = new JDKRandomGenerator();

    public static double[] generateRandomWithinBounds(Bounds bounds, int dimension) {
        double[] location = new double[dimension];
        for (int i = 0; i < dimension; ++i) {

            location[i] = Generator.RANDOM.nextDouble() * (bounds.getUpper()[i] -
                    bounds.getLower()[i])
                    + bounds.getLower()[i];
        }
        return location;
    }

    public static double[] generateRandomWithinSimpleBounds(double minBound, double maxBound, int dimension) {
        double[] location = new double[dimension];
        for (int i = 0; i < dimension; ++i) {

            location[i] = Generator.RANDOM.nextDouble() * (maxBound -
                    minBound)
                    + minBound;
        }
        return location;
    }

    public static double[] generateRandomWithUnsampledBias(SamplesEvaluator se, int tests, int dim, Bounds bounds) {
        double largestAverageDistance = Double.NEGATIVE_INFINITY;
        double[] bestX = null;
        for (int tries = 0; tries < tests; ++tries) {
            double[] x = Generator.generateRandomWithinBounds(bounds,
                    dim);
            Evaluation e = se.getEvaluation(x);
            if (e.getContainmentRatio() <= 1e-8) {
                bestX = x;
                break;
            }
            double evaluatedAverageDistance = e.getContainmentMeanDistance();
            if (evaluatedAverageDistance > largestAverageDistance) {
                largestAverageDistance = evaluatedAverageDistance;
                bestX = x;
            }
        }
        return bestX;
    }

    public static double[] generateNewDESample(double[] localXBest, double[] current, double[] diffVector1, double[] diffVector2, double scale, double crossProb, Bounds bounds) {
        double[] newX = new double[current.length];
        boolean xAccepted = false;
        while (!xAccepted) {
            xAccepted = true;
            for (int i = 0; i < newX.length; ++i) {
                newX[i] =
                        ((RANDOM.nextDouble() > crossProb) ?
                                (current[i]) :
                                (localXBest[i] + scale * (diffVector1[i] - diffVector2[i])));
                if (newX[i] < bounds.getLower()[i] || newX[i] > bounds.getUpper()[i]) {
                    xAccepted = false;
                    break;
                }
            }
            if (!xAccepted) {
                scale /= 2.0;
                newX = new double[current.length];
            }
        }

        return newX;
    }

    public static double[] generateNewEASample(List<Sample> samples, double mutationFactor, double crossProb, Bounds bounds) {
        EnumeratedDistribution<Sample> samplesDistribution = generateSamplesSampler(samples);

        Sample firstSample = samplesDistribution.sample();
        double[] newX = generateEASampleFromSample(mutationFactor, crossProb, bounds, samplesDistribution, firstSample);

        return newX;
    }

    public static double[] generateNewEASample(Sample seedSample, List<Sample> samples, double mutationFactor, double crossProb, Bounds bounds) {
        EnumeratedDistribution<Sample> samplesDistribution = generateSamplesSampler(samples);

        double[] newX = generateEASampleFromSample(mutationFactor, crossProb, bounds, samplesDistribution, seedSample);

        return newX;
    }

    private static double getWorstValue(List<Sample> samples) {
        double localYWorst = samples.get(0).getValue();
        for (Sample sample : samples) {
            if (sample.getValue() > localYWorst) {
                localYWorst = sample.getValue();
            }
        }
        return localYWorst;
    }

    private static double[] generateEASampleFromSample(double mutationFactor, double crossProb, Bounds bounds, EnumeratedDistribution<Sample> samplesDistribution, Sample firstSample) {
        double[] newX = new double[firstSample.getX().length];

        if (RANDOM.nextDouble() <= crossProb) {
            Sample secondSample = firstSample;
            while (secondSample.equals(firstSample)) {
                secondSample = samplesDistribution.sample();
            }
            if (!Arrays.equals(firstSample.getX(), secondSample.getX())) {
                double scale = RANDOM.nextDouble();
                for (int i = 0; i < newX.length; ++i) {
                    newX[i] = firstSample.getX()[i] + scale * (secondSample.getX()[i] - firstSample.getX()[i]);
                }
            } else {
                newX = mutateSample(firstSample, mutationFactor, bounds);
            }
        } else {
            newX = mutateSample(firstSample, mutationFactor, bounds);
        }
        return newX;
    }

    private static double[] mutateSample(Sample firstSample, double mutationFactor, Bounds bounds) {
        double[] newX = new double[firstSample.getX().length];
        boolean xAccepted = false;
        while (!xAccepted) {
            xAccepted = true;
            for (int i = 0; i < newX.length; ++i) {
                newX[i] = firstSample.getX()[i] +
                        RANDOM.nextGaussian() * mutationFactor * (bounds.getUpper()[i] - bounds.getLower()[i]) / 10.0;
                if (newX[i] < bounds.getLower()[i] || newX[i] > bounds.getUpper()[i]) {
                    xAccepted = false;
                    break;
                }
            }
            if (!xAccepted) {
                newX = new double[firstSample.getX().length];
            }
        }
        return newX;
    }

    private static EnumeratedDistribution<Sample> generateSamplesSampler(List<Sample> samples) {
        double largeMValue = getWorstValue(samples) + 1;

        EnumeratedDistribution<Sample> samplesDistribution;
        List<Pair<Sample,Double>> samplesRoullette = new ArrayList<>();
        for (Sample sample: samples) {
            samplesRoullette.add(new Pair<>(sample, largeMValue - sample.getValue()));
        }
        samplesDistribution = new EnumeratedDistribution<>(Generator.RANDOM, samplesRoullette);
        return samplesDistribution;
    }

}

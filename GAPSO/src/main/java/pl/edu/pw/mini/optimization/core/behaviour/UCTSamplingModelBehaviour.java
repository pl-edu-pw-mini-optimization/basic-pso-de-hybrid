package pl.edu.pw.mini.optimization.core.behaviour;

import org.apache.commons.math3.linear.SingularMatrixException;
import pl.edu.pw.mini.optimization.core.Particle;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.sampling.Sample;
import pl.edu.pw.mini.optimization.core.sampling.lm.SquareModel;
import pl.edu.pw.mini.optimization.utils.Generator;

import java.util.List;

public class UCTSamplingModelBehaviour extends UCTSamplingBehaviour {

    public UCTSamplingModelBehaviour(double weigth, double cFactor) {
        super(weigth, cFactor);
    }

    @Override
    public double[] computeVelocity(Particle particle) {
        particle.resetBest();

        int dim = particle.getFunction().getDimension();
        double[][] promisingRectangleBounds = particle.getFunction().getPromisingRectangleBounds(cFactor);
        double[] newX  = Generator.generateRandomWithinBounds(new Bounds(promisingRectangleBounds), dim);
        List<Sample> data = particle.getFunction().getNearestSamples(2 * dim + 1,
                newX);
        if (data.size() < 2 * dim + 1) {
            return super.computeVelocity(particle);
        } else {
            double[][] functionBounds = getBoundsForResult(promisingRectangleBounds, particle.getFunction().getBounds().toArray());
            SquareModel sm = new SquareModel(dim, data);

            double[] newVelocity = null;
            try {
                newVelocity = sm.getBoundedParabolaPeak(new Bounds(functionBounds));

            } catch (SingularMatrixException ex) {
                return super.computeVelocity(particle);
            }
            for (int i = 0; i < dim; i++) {
                newVelocity[i] -= particle.getX()[i];
            }
            return newVelocity;
        }
    }

    protected double[][] getBoundsForResult(double[][] bounds, double[][] functionBounds) {
        return bounds;
    }


}

package pl.edu.pw.mini.optimization.core.sampling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SingleSample extends Sample {
    protected double[] x;
    protected double value;

    public SingleSample(double[] x, double value) {
        this.x = Arrays.copyOf(x, x.length);
        this.value = value;
    }

    @Override
    public double[] getX() {
        return x;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public double getRadius() {
        return 0;
    }

    @Override
    public long getWeight() {
        return 1;
    }

    @Override
    public List<DistancedSample> getKOrderedSamples(double[] point, int k) {
        List<DistancedSample> distancedSamples = new ArrayList<>();
        if (k > 0) {
            distancedSamples.add(new DistancedSample(this, point));
        }
        return distancedSamples;
    }
}

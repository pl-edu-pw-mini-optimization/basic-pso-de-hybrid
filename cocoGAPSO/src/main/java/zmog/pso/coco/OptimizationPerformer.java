package zmog.pso.coco;

import pl.edu.pw.mini.optimization.OptimizationAlgorithm;
import pl.edu.pw.mini.optimization.core.GPSOConfigurator;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.initialization.PreviousOptimaAwareLocationInitializer;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;
import pl.edu.pw.mini.optimization.utils.Loggers;

import java.util.List;

public class OptimizationPerformer {
    private GPSOConfigurator configurator;
    private CocoProblem problem;
    private int dimension;
    private PreviousOptimaAwareLocationInitializer locationInitializer;
    private List<RTree> tabooIndexes;
    private List<Behaviour> behaviours;
    private OptimizationAlgorithm optimizationAlgorithm;
    private long evaluationsDone;
    private long evaluationsRemaining;
    private double[] optimum;
    private double bestValue;
    private boolean shouldBeRestarted;

    public OptimizationPerformer(GPSOConfigurator configurator, CocoProblem PROBLEM, int dimension, PreviousOptimaAwareLocationInitializer locationInitializer, List<RTree> tabooIndexes, List<Behaviour> behaviours, OptimizationAlgorithm optimizationAlgorithm, long evaluationsDone, long evaluationsRemaining, double[] optimum, double bestValue, boolean shouldBeRestarted) {
        this.configurator = configurator;
        problem = PROBLEM;
        this.dimension = dimension;
        this.locationInitializer = locationInitializer;
        this.tabooIndexes = tabooIndexes;
        this.behaviours = behaviours;
        this.optimizationAlgorithm = optimizationAlgorithm;
        this.evaluationsDone = evaluationsDone;
        this.evaluationsRemaining = evaluationsRemaining;
        this.optimum = optimum;
        this.bestValue = bestValue;
        this.shouldBeRestarted = shouldBeRestarted;
    }

    public long getEvaluationsDone() {
        return evaluationsDone;
    }

    public double[] getOptimum() {
        return optimum;
    }

    public double getBestValue() {
        return bestValue;
    }

    public boolean isShouldBeRestarted() {
        return shouldBeRestarted;
    }

    public OptimizationPerformer invoke() {
        /* Break the loop if the target was hit or there are no more remaining evaluations */
        while (!problem.isFinalTargetHit() && (evaluationsRemaining > 0)) {
            optimum = optimizationAlgorithm.optimize(
                    10);
            bestValue = optimizationAlgorithm.getOptimumValue();
            evaluationsDone = problem.getEvaluations();
            evaluationsRemaining = (long) (dimension * configurator.getBudgetMultiplier()) - evaluationsDone;
            if (optimizationAlgorithm.breakComputations()) {
                break;
            }
            if (optimizationAlgorithm.shouldBeRestarted()) {
                if (configurator.isTabuInitializerToBeUsed()) tabooIndexes.add(optimizationAlgorithm.getSamplesIndex());
                optimizationAlgorithm.clearMemory();
                behaviours.forEach(Behaviour::resetResultsAccumulation);
                SingleSample optimumEstimationSample = new SingleSample(optimum, bestValue);
                Loggers.SUBRUNS_LOGGER.registerEstimatedOptimum(optimumEstimationSample);
                locationInitializer.registerNewOptimumEstimation(optimumEstimationSample);
                shouldBeRestarted = true;
                break;
            }
        }
        return this;
    }
}

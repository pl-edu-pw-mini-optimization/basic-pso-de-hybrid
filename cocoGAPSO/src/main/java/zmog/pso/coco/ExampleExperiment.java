package zmog.pso.coco;

import pl.edu.pw.mini.optimization.OptimizationAlgorithm;
import pl.edu.pw.mini.optimization.core.GPSOConfigurator;
import pl.edu.pw.mini.optimization.core.GeneralizedParticleSwarm;
import pl.edu.pw.mini.optimization.core.GridSearch;
import pl.edu.pw.mini.optimization.core.behaviour.Behaviour;
import pl.edu.pw.mini.optimization.core.functions.Bounds;
import pl.edu.pw.mini.optimization.core.functions.Function;
import pl.edu.pw.mini.optimization.core.functions.SamplesGatherer;
import pl.edu.pw.mini.optimization.core.initialization.*;
import pl.edu.pw.mini.optimization.core.sampling.SingleSample;
import pl.edu.pw.mini.optimization.core.sampling.rtree.RTree;
import pl.edu.pw.mini.optimization.utils.Loggers;
import pl.edu.pw.mini.optimization.utils.PropertiesAccessor;

import java.util.*;

import static pl.edu.pw.mini.optimization.utils.Sanitizer.sanitizeFilename;

/**
 * An example of benchmarking random search on a COCO suite.
 * <p>
 * Set the parameter BUDGET_MULTIPLIER to suit your needs.
 */
public class ExampleExperiment {
    /**
     * The maximal number of independent restarts allowed for an algorithm that
     * restarts itself.
     */
    private static final int INDEPENDENT_RESTARTS = 10000;
    /**
     * The problem to be optimized (needed in order to simplify the interface
     * between the optimization algorithm and the COCO platform).
     */
    private static CocoProblem PROBLEM;


//
//    /**
//     * The random seed. Change if needed.
//     */
//    public static final long RANDOM_SEED = 0xdeadbeef;

    /**
     * The main method initializes the random number generator and calls the
     * example experiment on the bi-objective suite.
     */
    public static void main(String[] args) {
        /* Change the log level to "warning" to get less output */
        CocoJNIReflectionWrapper.cocoSetLogLevel("info");

        /* Start the actual experiments on a test suite and use a matching logger, for
         * example one of the following:
         *
         *   bbob                 24 unconstrained noiseless single-objective functions
         *   bbob-biobj           55 unconstrained noiseless bi-objective functions
         *   bbob-biobj-ext       92 unconstrained noiseless bi-objective functions
         *   bbob-largescale      24 unconstrained noiseless single-objective functions in large dimension
         *
         * Adapt to your need. Note that the experiment is run according
         * to the settings, defined in exampleExperiment(...) below.
         */
        exampleExperiment("bbob", "bbob");

        System.out.println("Done!");
        System.out.flush();
    }

    /**
     * A simple example of benchmarking random search on a given suite with
     * default instances that can serve also as a timing experiment.
     *
     * @param suiteName    Name of the suite (e.g. "bbob" or "bbob-biobj").
     * @param observerName Name of the observer matching with the chosen
     *                     suite
     *                     (e.g. "bbob-biobj" when using the "bbob-biobj-ext" suite).
     */
    private static void exampleExperiment(String suiteName, String observerName) {

        final GPSOConfigurator configurator = PropertiesAccessor.getGPSOConfigurationInstance();
        final BBOBExperimentConfigurator bbobConfigurator = new PropertiesBBOBExperimentConfigurator();
        int[] behaviourPoll = getBehaviourPool(configurator);
        int dimension;

        printoutInitialStatus(behaviourPoll);

        try {
            /* Set some options for the observer. See documentation for other options. */
            Benchmark benchmark = configureCOCOBenchmark(suiteName, observerName, configurator, bbobConfigurator, behaviourPoll);

            /* Initialize timing */
            Timing timing = new Timing();
            /* Iterate over all problems in the suite */
            while ((PROBLEM = benchmark.getNextProblem()) != null) {
                if (functionNotOnList(bbobConfigurator, PROBLEM))
                    continue;
                if (dimensionNotOnList(bbobConfigurator, PROBLEM))
                    continue;
                System.out.println("new problem " + PROBLEM.getName());
                dimension = PROBLEM.getDimension();
                SamplesGatherer f = wrapProblemInSamplesGatherer(PROBLEM, configurator.getModelEstimationProbability());

                PreviousOptimaAwareLocationInitializer locationInitializer =
                        new NonoverlappingRegionsExploringLocationInitializer(f);

                /* Run the algorithm at least once */
                List<RTree> tabooIndexes = new ArrayList<>();
                List<Behaviour> behaviours = Behaviour.getBehaviours(behaviourPoll);
                for (int run = 1; run <= 1 + INDEPENDENT_RESTARTS; run++) {
                    Loggers.SWARM_LOGGER.registerRestart();
                    if (configurator.resetAdaptationWeightAtRestart()) {
                        behaviours = Behaviour.getBehaviours(behaviourPoll);
                    }
                    final int POP_SIZE = configurator.getPopulationSizeAdjuster().getSize(
                            dimension,
                            (int)Math.round(Math.log10(configurator.getBudgetMultiplier())),
                            behaviours.size());

                    Loggers.registerRuns(
                            Behaviour.getId(behaviourPoll) + "_"
                                    + PropertiesAccessor.getInstance().getVersion(),
                            PROBLEM.getId(),
                            PROBLEM.getDimension()
                            );
                    locationInitializer.resetState();
                    OptimizationAlgorithm optimizationAlgorithm = chooseOptimizationAlgorithm(configurator, bbobConfigurator, PROBLEM, f, locationInitializer, behaviours, POP_SIZE);
                    long evaluationsDone = PROBLEM.getEvaluations();
                    long evaluationsRemaining = (long) (dimension * configurator.getBudgetMultiplier()) - evaluationsDone;

                    double[] optimum = null;
                    double bestValue = Double.POSITIVE_INFINITY;
                    boolean shouldBeRestarted = false;

                    OptimizationPerformer optimizationPerformer = new OptimizationPerformer(configurator, PROBLEM, dimension, locationInitializer, tabooIndexes, behaviours, optimizationAlgorithm, evaluationsDone, evaluationsRemaining, optimum, bestValue, shouldBeRestarted).invoke();
                    evaluationsDone = optimizationPerformer.getEvaluationsDone();
                    optimum = optimizationPerformer.getOptimum();
                    bestValue = optimizationPerformer.getBestValue();
                    shouldBeRestarted = optimizationPerformer.isShouldBeRestarted();
                    if (shouldBeRestarted)
                        continue;
                    if (optimum != null) {
                        SingleSample optimumEstimationSample = new SingleSample(optimum, bestValue);
                        Loggers.OPTIMA_LOGGER.registerOptimum(optimumEstimationSample);
                        Loggers.SUBRUNS_LOGGER.registerEstimatedOptimum(optimumEstimationSample);
                    }
                    tabooIndexes.forEach(RTree::clearIndex);
                    optimizationAlgorithm.clearMemory();

                    /* Break the loop if the algorithm performed no evaluations or an unexpected thing happened */
                    if (PROBLEM.getEvaluations() == evaluationsDone) {
                        System.out.println("WARNING: Budget has not been exhausted (" + evaluationsDone + "/"
                                + dimension * configurator.getBudgetMultiplier() + " evaluations done)!\n");
                        break;
                    } else if (PROBLEM.getEvaluations() < evaluationsDone)
                        System.out.println("ERROR: Something unexpected happened - function evaluations were decreased!");
                }

                /* Keep track of time */
                timing.timeProblem(PROBLEM);
            }

            /* Output the timing data */
            timing.output();

            benchmark.finalizeBenchmark();
            Loggers.complete();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static OptimizationAlgorithm chooseOptimizationAlgorithm(GPSOConfigurator configurator, BBOBExperimentConfigurator bbobConfigurator, CocoProblem PROBLEM, SamplesGatherer f, PreviousOptimaAwareLocationInitializer locationInitializer, List<Behaviour> behaviours, int POP_SIZE) {
        OptimizationAlgorithm optimizationAlgorithm;
        if (!bbobConfigurator.isFunctionMappingExperiment()) {
            optimizationAlgorithm = new GeneralizedParticleSwarm(
                    f,
                    POP_SIZE,
                    configurator.getSwarmRestartManager(PROBLEM.getDimension()),
                    locationInitializer,
                    behaviours);
        } else {
            optimizationAlgorithm = new GridSearch(
                    f,
                    1000);
        }
        return optimizationAlgorithm;
    }

    private static Benchmark configureCOCOBenchmark(String suiteName, String observerName, GPSOConfigurator configurator, BBOBExperimentConfigurator bbobConfigurator, int[] behaviourPoll) throws Exception {
        final String observerOptions = buildObserverOptions(suiteName, configurator, bbobConfigurator, behaviourPoll);

        /* Initialize the suite and observer.
        * For more details on how to change the default options, see
        * http://numbbo.github.io/coco-doc/C/#suite-parameters and
        * http://numbbo.github.io/coco-doc/C/#observer-parameters. */
        Suite suite = new Suite(suiteName, "", "");
        Observer observer = new Observer(observerName, observerOptions);
        return new Benchmark(suite, observer);
    }

    private static void printoutInitialStatus(int[] behaviourPoll) {
        System.out.println("Running the example experiment... (might take time, be patient)");
        System.out.println("Algorithm " + Behaviour.toString(behaviourPoll));
        System.out.flush();
    }

    private static int[] getBehaviourPool(GPSOConfigurator configurator) {
        int[] behaviourPoll;
        try {
            behaviourPoll = configurator.getAlgorithmsInitialPollList();
        } catch (NumberFormatException ex) {
            behaviourPoll = Behaviour.setupFromDictionary(configurator.getAlgorithmsDictionaryPollList());
        }
        return behaviourPoll;
    }

    private static String buildObserverOptions(String suiteName, GPSOConfigurator configurator, BBOBExperimentConfigurator bbobConfigurator, int[] behaviourPoll) {
        return "result_folder: " +
        getResultsFolderName(behaviourPoll, bbobConfigurator, configurator) +
        " algorithm_name: " +
        getAlgorithmName(behaviourPoll, bbobConfigurator, configurator) +
        " algorithm_info: " +
        getAlgorithmInfo(suiteName, behaviourPoll, configurator);
    }

    private static boolean dimensionNotOnList(BBOBExperimentConfigurator bbobConfigurator, CocoProblem PROBLEM) {
        int dimension = PROBLEM.getDimension();
        return Arrays.stream(bbobConfigurator.getDimensionsList()).noneMatch(dm -> dm == dimension);
    }

    private static boolean functionNotOnList(BBOBExperimentConfigurator bbobConfigurator, CocoProblem PROBLEM) {
        return Arrays.stream(bbobConfigurator.getFunctionsList())
                .noneMatch(fName -> PROBLEM.getName().contains(fName + " "))
                && Arrays.stream(bbobConfigurator.getFunctionsList())
                .noneMatch("all"::equals);
    }

    private static SamplesGatherer wrapProblemInSamplesGatherer(CocoProblem PROBLEM, double modelEstimationProbability) {
        return new SamplesGatherer(new Function() {

            @Override
            public double getValue(double[] x) {
                return (PROBLEM.evaluateFunction(x))[0];
            }

            @Override
            public Bounds getBounds() {
                return new Bounds(
                        PROBLEM.getSmallestValuesOfInterest(),
                        PROBLEM.getLargestValuesOfInterest()
                );
            }

            @Override
            public int getDimension() {
                return PROBLEM.getDimension();
            }

            @Override
            public String getName() {
                return PROBLEM.getName();
            }
        }, modelEstimationProbability);
    }

    private static String getAlgorithmInfo(
            String suiteName,
            int[] behaviourPoll,
            GPSOConfigurator configurator) {
        return "GAPSO_" +
                sanitizeFilename(Behaviour.getId(behaviourPoll)) + "_" +
                configurator.getPopulationSizeAdjuster().toString() + "_" +
                PropertiesAccessor.getInstance().getVersion() + "_" +
                Math.round(Math.log10(configurator.getBudgetMultiplier())) + "_on_" + suiteName +
                (configurator.isTabuInitializerToBeUsed() ? "-tabu-init" : "-no-tabu-init") +
                (configurator.getModelEstimationProbability() > 0 ? "-estimate-" + configurator.getModelEstimationProbability() : "-cache-only") +
                configurator.getSwarmRestartManager(1).toString();
    }

    private static String getAlgorithmName(
            int[] behaviourPoll,
            BBOBExperimentConfigurator bbobConfigurator,
            GPSOConfigurator configurator) {
        return bbobConfigurator.getExperimentName() + "_" +
                configurator.toString() +
                "_" +
                PropertiesAccessor.getInstance().getVersion() + "_" +
                sanitizeFilename(Behaviour.getId(behaviourPoll));
    }

    private static String getResultsFolderName(
            int[] behaviourPoll,
            BBOBExperimentConfigurator bbobConfigurator,
            GPSOConfigurator configurator) {
        return bbobConfigurator.getExperimentName() + "_" +
                PropertiesAccessor.getInstance().getVersion() + "_" +
                configurator.toString() +
                "_" +
                sanitizeFilename(Behaviour.getId(behaviourPoll));
    }

}

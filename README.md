# Generalized Adapative Particle Swarm Optimization (GAPSO)

## General features

 * PSO behaviour
 * DE behaviour
 * Linear model for square function based on R-tree indexed samples
 * Behaviour adaptation by roulette rule applied on normalized behaviour success
 * JADE-like population size
 * JADE-like restarts (with additional observation specialized for step functions)

Settings for algorithm in `algorithm.properties` which should be placed next to application `JAR` file.

```properties
gapso.behaviors=PSO-L-2007:1000,DE:1000,LM:1,PM:1       #an alternative definition - using the short names from factories
gapso.behaviors.mixing=true                             #are behaviours to be mixed after each iteration (not necessarily adapted)
gapso.budget=1000                                       #function evaluations budget multiplier (10000 for proper experiments)
gapso.population.type=JADE                              #population size would be dimension times multiplier
gapso.population.multiplier=10                          #population size multiplier
gapso.reset.adaptation.at.reset=true                    #should behaviour adaptation data be reset at every reset of algorithm due to stagnation
gapso.history.length=10                                 #the length of behaviour history in iterations
gapso.restarts.stagnation=40                            #number of iterations before reset
gapso.restarts.collapse=1e-4                            #swarm diameter or best value difference spread
gapso.restarts.collapse.type=LINEAR                     #(or VARIANCE)
gapso.initializer.quadratic.delay.type=NO               #how many times quadratic initializer will be used (NO - every possible time, DIM - every DIM, INF - only once per large restart)
#gapso.initializer.composite.sequence=square,linear     #initializers sequence (new since 1.22.0), possible values: square,linear,boundary,ea,de
gapso.sampling.use.cache=true                           #should samples index be checked for duplicates
gapso.samples.memory=20000                              #max size of samples index (there were performance problems when index has been allowed to grow infinitely)
                                                        #initializer strategies section begin
                                                        #prior to 1.20.8 those parameters should have been >= 0.0
                                                        #since to 1.20.8 values < 0 mean that a given strategy will not be available at all
gapso.exploration.weight.random.point.exploration=1.0   #re-initializer properties (odds of selecting uniformly at random local optimum point as search origin)
gapso.exploration.weight.best.point.exploration=1.0     #re-initializer properties (odds of selecting best local optimum point as search origin)
gapso.exploration.weight.best.point.exploitation=1.0    #re-initializer properties (odds of selecting best local optimum point as search origin for exploitation)
gapso.exploration.weight.difference.point.exploration=1 #re-initializer properties (odds of selecting a DE-like movement search for optimum)
                                                        #new properties since 1.20.8
gapso.exploration.weight.complete.bounds.reset=1.0      #re-initializer properties (odds of selecting the whole function area)
gapso.exploration.adapt.exploration.strategy=false      #re-initializer properties (should the odds be adapted on the basis of their performance)
                                                        #new properties since 1.23.0
gapso.exploration.weight.roulette=1.0                   #re-initializer properties (odds of selecting roullete random local optimum point as search origin)
                                                        #initializer strategies section end
gapso.exploration.max.best.neighbourhood=0.15           #re-initializer properties (area scaling when exploiting)
gapso.exploration.to.boundary.max.fraction=1.0          #re-initializer properties (area scaling between optimum and boundary)
gapso.exploration.between.optima.max.fraction=1.0       #re-initializer properties (area scaling between optima)
```

Settings for COCO BBOB experiment should be placed in `bbob.properties` next to the application JAR

```
bbob.function=f24 instance 5                            #function names separated by commas, possibly with instance numbers, "all" for all functions
bbob.dimensions=20                                      #list of dimensions (proper experiments will have 2,3,5,10,20,40)
experiment.name=default                                 #some informative short string to be used as the most visible label part
bbob.map.function=false                                 #smart GAPSO optimization is run (if `true` GridSearch is done)
```

## 1.27.5

 * Created logger of samples used for next move
 * Logger properties taken out into a separate settings file
   with `.default.properties`/`.properties` pattern

## 1.27.4

 * Reorganized boundaries into Bounds object
 * Refactored array copying
 * Refactored best solution update

## 1.27.3

 * GridSearch implementation of OptimizationAlgorithm

## 1.27.2

 * Generalized GAPSO class into `OptimizationAlgorithm` interface

## 1.27.1

 * Settings for BBOB removed from GAPSO

## 1.27.0

 * Trying more stubbornly to get DE and EA switching regions in those reinitializers
 * Removed exploitation of the best optimum before moving to exploration scheme

## 1.26.0-initialization-polynomial

 * Added polynomial behaviour with hardcoded configuration

## 1.26.0

 * Trying more stubbornly to get DE and EA switching regions in those reinitializers
 * Removed exploitation of the best optimum before moving to exploration scheme
 * Added behaviour setup from dictionary

## 1.25.3

 * Added polynomial behaviour

## 1.25.2

 * Added SPSO-2011 behaviour

## 1.25.1

 * Global best made available in each particle

## 1.25.0

 * Added global PSO behaviour

## 1.24.3

 * Replacing old insignificantly different in location and worse optima in restart handler

## 1.24.2

 * Improved history managment when no history should be stored

## 1.24.1

 * Added mixing option to behaviour management

## 1.24.0

 * Added optima logger

## 1.23.0

 * Added roulette sampling of already discovered optima
 * Default max neighboorhood changed to 0.15
 * Dropped -GECCO-BBOB-2019 from version - as we are way past sending anythong to that workshop
   (although COCO 2.3 still is a relevant part of the project)

## 1.22.1-GECCO-BBOB-2019

 * Added ability to configure initializers sequence (`gapso.initializer.composite.sequence`)
 * Returned DE re-initilization to use predefined optimum size instead of minOptimumDistance

## 1.21.0-GECCO-BBOB-2019

 * Added ability to configure cache size (`gapso.samples.memory`)

## 1.20.8-GECCO-BBOB-2019

 * Parametrizing re-initilizer adaptation
 * Parametrizing total restart initial weight
 * Setting up re-initializer possible behaviours (with less then 0 to switch off)
 * Setting up area ratio in exploration through settings

## 1.20.7-GECCO-BBOB-2019

 * Removed total restart option in re-initilizer

## 1.20.6-GECCO-BBOB-2019

 * Less repetitive DE sampling in re-initilizer
 * Adaptation and total restart option in re-initilizer

## 1.20.5-GECCO-BBOB-2019

 * Smaller initial default area ratio for exploration
 * Learning about optima distances in order to adjust search area

## 1.20.4-GECCO-BBOB-2019-EA

 * Added EA behaviour

## 1.20.4-GECCO-BBOB-2019

 * Resetting exploitation when new optimum is out of search bounds
 * Retaining behaviour poll between restarts
 * Clearing behaviour successes history between restarts
 * Debugged bounds in regions to search

## 1.20.3-GECCO-BBOB-2019

 * Created regions object for handling exploration logic (regions are now shrinking after one exploration)

## 1.20.2-GECCO-BBOB-2019

 * Larger tolerance in exploitation

## 1.20.1-GECCO-BBOB-2019

 * Splitting regions after each search exploited something

## 1.20.0-GECCO-BBOB-2019

 * Foundations for more systematic reinitialization implemented

## 1.19.21-GECCO-BBOB-2019

 * Changed DE initialization to EA initialization in DefaultComplex initializer

## 1.19.20-GECCO-BBOB-2019

 * Changed DE exploration in order to test less times the same optima - still needed identification of the possible optimum size (plan is to do it by clustering and analyzing std-dev of samples in single cluster)

## 1.19.19-GECCO-BBOB-2019

 * Returned no averaging to standard sampling, but more samples in linear models

## 1.19.18-GECCO-BBOB-2019

 * Experiments with averaging function value (both in samplers and LM)

## 1.19.17-GECCO-BBOB-2019

 * Square function initializer fires every `D` tries (where `D` is function dimensions) for `gapso.initializer.quadratic.delay.type=DIM`
 * Square function initializer fires only once per large restart  for `gapso.initializer.quadratic.delay.type=INF`

## 1.19.16-GECCO-BBOB-2019

 * Initialization using DE/best/1/bin-like sampling

## 1.19.15-GECCO-BBOB-2019

 * Parameters taken out into property file and their default values stored in default configuration object

## 1.19.14-GECCO-BBOB-2019

 * Preparation for taking parameters out of initializer into properties

## 1.19.13-GECCO-BBOB-2019

 * Exploration strategies: random bounds within bounds

## 1.19.12-GECCO-BBOB-2019

 * Exploration strategies: refactored code of boundaries into objects
 * Exploration strategies: alternative exploration scheme
   (a) random and closest neighbourhood
   (b) best and closest neighbourhood
   (c) exploit best (as before)

## 1.19.11-GECCO-BBOB-2019

 * Exploration strategies: reduced probability of outside of some optimum

## 1.19.10-GECCO-BBOB-2019

 * Exploration strategies: enhanced probability of exploring outside some optimum
 * Exploration strategies: added probability of utilizing current best estimation
 and small size search space

## 1.19.9-GECCO-BBOB-2019

 * Exploration strategies: Single optimum as basis for exploration, two random, one best and one random

## 1.19.8-GECCO-BBOB-2019

 * Unique optima

## 1.19.7-GECCO-BBOB-2019

 * Using different optima as origin points
 * Using different multiple optima

## 1.19.6-GECCO-BBOB-2019

 * Stopping exploitation if improvement insignificant

## 1.19.5-GECCO-BBOB-2019

 * Added function wide adaptation in initialization reset

## 1.19.4-GECCO-BBOB-2019

 * Debugged exploiting
 * Constant shrinking in exploiting - reseting area after changing optimization focus

## 1.19.3-GECCO-BBOB-2019

 * Properly staying on the same optimum (actually it migh be worth
 to explore also previous chaotic option, where reference optimum changed
 every time it stuck)
 * Also definition of stuck should be explored, as previously it was going aout of region
   without improvement and now its having no chances to improve

## 1.19.2-GECCO-BBOB-2019

 * Second attempt at exploration: random single iteration on the basis of first
   somewhat exploited local optimum - still short memory and unclear inactivity

## 1.19.1-GECCO-BBOB-2019

 * First attempt at exploration: not very good - short memory and chaotic

## 1.19.0-GECCO-BBOB-2019

 * Possibility of changing bounds for initializer
 * Utilizing previous optimum estimation in initializer

## 1.18.3-GECCO-BBOB-2019

 * Fixed boundary vertex initialization (index updated after first poll)
 * Fixed linear solver (not taking into account double checking same place)

## 1.18.2-GECCO-BBOB-2019

 * Parabola peak selected every time its possible in intialization
 * Initialization refactored into a more proper set of objects instead of parabola function
 * Taboo initilization is dropped from functionality for now (and maybe forever)

## 1.18.1-GECCO-BBOB-2019

 * Solution updates in every iteration - in case something has been reset

## 1.18.0-GECCO-BBOB-2019

 * More randomized algorithm start (random cube vertex)
 * Still needed some rational decision about restricted search space
 * Working on logging algorithm run
 * Extending logging with null loggers

## 1.17.0-GECCO-BBOB-2019

 * Added maxBound - minBound linear way of computing swarm dispersion
 * In experiments reset epsilon set to larger value, after observing logs
 for higher dimensions
 * Also, possibly swarm size in high-dimensions (10+) should not be multiplied by 15